# Website

This website is built using [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

## Installation

```console
yarn install
```

## Local Development

```console
yarn start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.

## Build

```console
yarn build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.

## Deployment

```console
GIT_USER=<Your GitHub username> USE_SSH=true yarn deploy
```

If you are using GitHub pages for hosting, this command is a convenient way to build the website and push to the `gh-pages` branch.



#TODO Artigos para pdf
1. Revisar **TODOS** os artigos para verificar possíveis notas de rodapé/fim
1. Fazer os cabeçalhos das primeiras páginas de contexto
1. Fazer página de apresentação dos grupos de pesquisas (Página em Branco)
1. Arrumar contagem de páginas para começar junto com o conteúdo
1. Página de fechamento




# Como criar um e-book:

https://rachelandrew.co.uk/archives/2014/01/07/html-epub-mobi-pdf-wtf-creating-an-ebook/

# Questões de diagramação

1. Espaçamento entre cabeçalho e texto --> OK
1. Inserir equipe editorial de forma rápida (inserir design e diagramação) --OK
1. Título mais parecido com o modelo e tirar ´:´ sobresalentes --> OK
1. Inserir logo da unesp na última página --> OK
1. Ajustar título no footer --> OK
1. Ajustar no markdown:
  1. Ajustar notas de fim ver: https://novo.labriunesp.org/cadernos/labri-n0003 --> OK
  1. Vínculo Institucional dos Autores (colocar depois das referencias)
  1. Transformar o author:title em texto
  1. Revisar todos os arquivos markdown e trocar o strong para h3


1. Vários problemas no arquivo n 4
1. Escrever o texto sobre os Cadernos LabRI
1. Inserir a série na capa e na referencia

#16.03

1.Criar template markdown para que os próximos trabalhos escritos não tenham problemas com o processo automática

1. Verificar citação de texto (mais de 3 linhas) --> OK
1. Verifica como o código fica em pdf --> OK
1. Destaque - span --> OK

#22.03

1. Fazer sumário automático
  1. Realizar o parseamento da - pagina
  1. Adicionar id para todo h1, h2, h3 e h4
  1. Criar um sumário com os links e os IDs
1.Criar template markdown para que os próximos trabalhos escritos não tenham problemas com o processo automática


#19.04

- Dar um jeito de tirar o [TOC] no texto final
  - Possibilidade: adicionar uma class ou id e colocar invisible no css. Testar se afetao pipeline md to pdf
- Colocar icones de download do pdf e epub -- ok
- Verificar o que está faltando no site

# Coisas a fazer no site:
 - Centralizar o subtítulo na home
 - Ex membros quebou
 - tag das publicações não está centralizada
 - Página das publicações icone errado
 - Tempo de leitura não está centralizado



 # 17.05

 - Resolver questões dos projetos -- ok
 - Resolver questão do icone do pdf na página dos pdf -- okk
 - Fazer das notícias um blog autônomo
 - implementar a página do PODRI


## MD to pdf
- Gerar os PDFs dos arquivos novos
- Documenar a toda a parte do md to pdf
- Ajusar o texto da contracapa