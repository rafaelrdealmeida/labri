/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React from 'react';
import Layout from '@theme/Layout';
import BlogPostItem from '@theme/BlogPostItem';
import BlogPostPaginator from '@theme/BlogPostPaginator';
import BlogSidebar from '@theme/BlogSidebar';
import {ThemeClassNames} from '@docusaurus/theme-common';
import TOC from '@theme/TOC';
import styles from './styles.module.css';

const MONTHS = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez',
];

function BlogPostPage(props) {
  const {content: BlogPostContents, sidebar} = props;
  const {frontMatter, metadata} = BlogPostContents;
  const {title, description, nextItem, prevItem, editUrl, date, readingTime} = metadata;
  const {hide_table_of_contents: hideTableOfContents, tipo_publicacao, author, subtitulo, tags, download_pdf} = frontMatter;
  const match = date.substring(0, 10).split('-');
  const year = match[0];
  const month = MONTHS[parseInt(match[1], 10) - 1];
  const day = parseInt(match[2], 10);
  return (
    <Layout 
    title={title} 
    description={description}
    wrapperClassName={ThemeClassNames.wrapper.blogPages}
    pageClassName={ThemeClassNames.page.blogPostPage}>
      {BlogPostContents && (
        <div className="container margin-vert--lg">
          <div className="row">
            <div className="col col--2">  
              <BlogSidebar sidebar={sidebar} />
            </div>
            <div className="col col--8">
                <div className={styles.download_top}>
                  <div className={styles.download_pdf}>
                    <a href={frontMatter.download_pdf} target="_blank" type="application/pdf" rel="noopener noreferrer">
                      <img src="/img/download_pdf.svg" alt="Download PDF" ></img>
                      <img src="/img/download_epub.svg" alt="Download EPUB" ></img>
                      <p className={styles.img__description}>Baixe o arquivo em PDF</p>
                    </a>
                  </div>
                </div>
              <div className={styles.tag}>
                <center><a class={styles.tags} href="./tags">
                {tags}
                </a></center>
              </div>
              <div className={styles.premiere}>
                <center><strong className={styles.publi}> {tipo_publicacao}</strong></center>
                <div className={styles.tempo}>
                <center><time dateTime={date}>
                      {day}/{month}/{year} · Tempo de leitura: {Math.ceil(readingTime)} min
                  </time></center>
                </div>
                <h1 className={styles.titulo}> {title} {subtitulo}</h1>                
                <h2> Por {author} </h2>
              </div>
                <BlogPostContents />
              <div>
                {editUrl && (
                  <a href={editUrl} target="_blank" rel="noreferrer noopener">
                    <svg
                      fill="currentColor"
                      height="1.2em"
                      width="1.2em"
                      preserveAspectRatio="xMidYMid meet"
                      viewBox="0 0 40 40"
                      style={{
                        marginRight: '0.3rem',
                        verticalAlign: 'sub',
                      }}>
                      <g>
                        <path d="m34.5 11.7l-3 3.1-6.3-6.3 3.1-3q0.5-0.5 1.2-0.5t1.1 0.5l3.9 3.9q0.5 0.4 0.5 1.1t-0.5 1.2z m-29.5 17.1l18.4-18.5 6.3 6.3-18.4 18.4h-6.3v-6.2z" />
                      </g>
                    </svg>
                    Edit this page
                  </a>
                )}
              </div>
              {(nextItem || prevItem) && (
                <div className="margin-vert--xl">
                  <BlogPostPaginator nextItem={nextItem} prevItem={prevItem} />
                </div>
              )}
            </div>
            {!hideTableOfContents && BlogPostContents.toc && (
              <div className="col col--2">
                <div className={styles.download_bottom}>
                      <div className={styles.download_pdf}>
                        <a href={frontMatter.download_pdf} target="_blank" type="application/pdf" rel="noopener noreferrer">
                      <img src="/img/download_pdf.svg" alt="Download PDF" ></img>
                      <img src="/img/download_epub.svg" alt="Download EPUB" ></img>

                      <p className={styles.img__description}>Baixe o arquivo em PDF</p>
                    </a>
                  </div>
                </div>
                <TOC toc={BlogPostContents.toc} />
              </div>
            )}
          </div>
        </div>
      )}
    </Layout>
  );
}

export default BlogPostPage;
