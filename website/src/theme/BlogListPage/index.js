/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React from 'react';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import BlogPostItem from '@theme/BlogPostItem';
import BlogListPaginator from '@theme/BlogListPaginator';
import BlogSidebar from '@theme/BlogSidebar';
import styles from './styles.module.css';
import clsx from 'clsx';

function BlogListPage(props) {
  const {metadata, items, sidebar} = props;
  const {
    siteConfig: {title: siteTitle},
  } = useDocusaurusContext();
  const {blogDescription, blogTitle, permalink} = metadata;
  const isBlogOnlyMode = permalink === '/';
  const title = isBlogOnlyMode ? siteTitle : blogTitle;
  return (
    <Layout title={title} description={blogDescription}>
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className={styles.faixa}>
          <h1 className="hero__title">Publicações</h1>
          <h2 className="hero__subtitle">Cadernos LabRI/Unesp</h2>
        </div>
      </header>
      <section>
      <div className={styles.container}>
        <div className={clsx("info-revista", styles.informacoes)}>
          <div className={styles.grid}>
            <div className={styles.apresentacao}>
              <div className = "card">
                <div className="card-content">
                  <div class="card__header">
                    <h3>Apresentação</h3>
                </div>
                  <div className="card__body">
                      <p>
                      Os Cadernos LabRI são uma publicação circunscrita a divulgar os trabalhos de grupos e indivíduos vinculados ao LabRI/UNESP. Os principais objetivos desta publicação são
                      <p>1. Fornecer um espaço aos grupos para divulgação de seus trabalhos.</p>
                      <p>2. Auxiliar uma melhor comunicação dos projetos e dos produtos derivados</p>
                      <p>3. Estímulo à utilização de tecnologias digitais no cotidiano acadêmico</p>
                      <p>Devido aos aspectos apontados acima, os Cadernos LabRI não estão abertos a submissão de trabalhos externos para a publicação. Apesar disso, eventualmente, convidados também poderão ter seus trabalhos divulgados nos Cadernos LabRI.</p>
                      </p>
                  </div>
                </div>
                <div className="card__footer">
                  <a href="../docs/cadernos/intro"><button class="button button--secondary button--block">Saiba Mais</button></a>
                </div>
              </div>
            </div>
            <div className={clsx(styles.expediente)}>
              <div className = "card">
                <div className="card-content">
                  <div class="card__header">
                    <h3>Expediente</h3>
                  </div>
                    <div className="card__body">
                      <p>
                      O editor principal é o docente que  está responsável pela coordenação do LabRI/UNESP. 
                      
                      <p>Porém, as séries temáticas e de grupos de pesquisa terão como editor responsável o coordenador da respectiva série temática ou grupo de pesquisa.</p>

                      <p>- Editor principal: Marcelo Passini Mariano</p>
                      <p>- Assistentes editoriais: Júlia dos Santos Silveira, Pedro Campagna, Rafael de Almeida</p> 
                      <br/><br/><br/>


                      </p>
                    </div>
                  </div>
                  <div className="card__footer">
                    <a href="../docs/cadernos/expediente"><button class="button button--secondary button--block">Saiba Mais</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.temas}>
              <div className = "card">
                <div className="card-content">
                  <div class="card__header">
                    <h3>Séries LabRI</h3>
                  </div>
                  <div className="card__body">
                    <a class="button button--secondary" href="./tags/covid-19">
                      COVID-19
                    </a>
                    <a class="button button--secondary" href="./tags/cadernos">
                      CADERNOS
                    </a>
                  </div>
                </div>
                <div className="card__footer">
                  <a href="../docs/cadernos/series-labri"><button class="button button--secondary button--block">Saiba Mais</button></a>
                </div>
              </div>
            </div>
        </div>

        </section>
        <section>
        <div className={styles.conteudo}>
          <div className="row">
            <main className={styles.blogpost}>
              {items.map(({content: BlogPostContent}) => (
                <BlogPostItem
                  key={BlogPostContent.metadata.permalink}
                  frontMatter={BlogPostContent.frontMatter}
                  metadata={BlogPostContent.metadata}
                  truncated={BlogPostContent.metadata.truncated}>
                  <BlogPostContent />
                </BlogPostItem>
              ))}
            </main>
            <div className="col col--12">
            <BlogListPaginator metadata={metadata} />
            </div>
          </div>
        </div>

        </section>
    </Layout>
  );
}

export default BlogListPage;
