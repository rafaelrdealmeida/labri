/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React from 'react';
import clsx from 'clsx';
import { MDXProvider } from '@mdx-js/react';
import Head from '@docusaurus/Head';
import Link from '@docusaurus/Link';
import MDXComponents from '@theme/MDXComponents';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';
const MONTHS = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez',
];

function BlogPostItem(props) {
    const {
        children,
        frontMatter,
        metadata,
        truncated,
        isBlogPostPage = false,
    } = props;
    const subtitulo = frontMatter.subtitulo;
    const descricao = frontMatter.descricao;
    const tipo_publicacao = frontMatter.tipo_publicacao;
    const { date, permalink, tags, readingTime } = metadata;
    const { author, title, image, keywords } = frontMatter;
    const authorURL = frontMatter.author_url || frontMatter.authorURL;
    const authorTitle = frontMatter.author_title || frontMatter.authorTitle;
    const image_url = frontMatter.image_url;
    const authorImageURL =
        frontMatter.author_image_url || frontMatter.authorImageURL;
    const imageUrl = useBaseUrl(image, {
        absolute: true,
    });

    const renderPostHeader = () => {
        const TitleHeading = isBlogPostPage ? 'h1' : 'h2';
        const match = date.substring(0, 10).split('-');
        const year = match[0];
        const month = MONTHS[parseInt(match[1], 10) - 1];
        const day = parseInt(match[2], 10);
        return (
            <div class="container">
                <div class="row">
                    <div class="col col--12">
                        <section>
                        <div className={styles.tag.tag}>
                            <div className={styles.grid}>
                                <div className={styles.tags}>
                                    {tags.map(({ label, permalink: tagPermalink }) => (
                                        <button className={styles.button, styles.tag}>
                                        <Link
                                            key={tagPermalink}
                                            to={tagPermalink}>
                                            {label}
                                        </Link>
                                        </button>
                                        ))}
                                </div>
                            </div>
                        </div>
                                <figure className={styles.effect_steve}>
                                    <img src={image_url} alt={title} />
                                    <figcaption>
                                        <h3><div>{tipo_publicacao}</div>
                                            <time dateTime={date}>
                                                {day}/{month}/{year}
                                                {readingTime && <> · Tempo de leitura: {Math.ceil(readingTime)} min</>}
                                            </time></h3>

                                        <h2>{title}:<span> {subtitulo} </span>
                                        <span><h6><i><b>{author}</b></i></h6></span></h2>


                                        {isBlogPostPage ? title : <Link to={permalink}>{title}</Link>}
                                        <p> {descricao} </p>
                                    </figcaption>
                                </figure>
                        </section>
                    </div>
                </div>
            </div>
        );
    };

    return (
        <>
            <article>
                <div className={styles.grid}>
                    {renderPostHeader()}
                </div>
            </article>

        </>
    );
}

export default BlogPostItem;
