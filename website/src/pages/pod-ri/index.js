import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "../styles.module.css";
import stylesPod from "./styles.pod-ri.module.css";
import LogoPodRI from "@site/static/img/projetos/extensao/pod-ri/pod-ri.svg";
import useBaseUrl from "@docusaurus/useBaseUrl";

const intro = [
  {
    projeto: "pod-ri - labri-unesp",
    text: "Seu podcast sobre as Relações Internacionais e o mundo",
    imgIntro: "/img/projetos/extensao/pod-ri/pod-ri.svg",
  },
];

const sobre = [
  {
    projeto: "pod-ri - labri-unesp",
    description:
      "O Pod-RI é um projeto feito por estudantes, no formato de podcast, iniciado como uma ideia entre amigos. Gravamos episódios com diversas temáticas das Relações Internacionais, assim como conversas com especialistas e profissionais. Além disso, produzimos conteúdos para redes sociais, onde entramos em contato mais direto com a comunidade de Relações Internacionais do Brasil.",
    imgSobre: "./img/projetos/extensao/pod-ri/sobre.png",
  },
];

const eppod = [
  {
    projeto: "pod-ri - labri-unesp",
    nome: "#3.12: Trabalhando com Assistência de Refugiados com Miguel Pachioni",
    description:
      "Bem vindos a mais um episódio do Pod-RI! Neste episódio conversamos com Miguel Pachioni sobre sua experiência trabalhando na ACNUR e sobre o importante tema da assistência de refugiados.",
    imgEps: "./img/projetos/extensao/pod-ri/EPs-pod.jpg",
    info: "Todos os episódios do Pod-RI e plataformas em que estão disponíveis encontram-se aqui:",
  },
];

const membro = [
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Ailton Salvadori",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-ailton.png",
    rede1: "https://www.instagram.com/ailtonsalva/",
    imgRede1: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    rede2: "https://www.facebook.com/ailton.salvadori",
    imgRede2: "./img/projetos/extensao/pod-ri/fb-logo.svg",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Bianca Cintra da Costa Antunes",
    rede1: "https://www.facebook.com/bianca.antunes.758",
    imgRede1: "./img/projetos/extensao/pod-ri/fb-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-bianca.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Bruno Cesar David Ruy",
    rede1: "https://www.instagram.com/brunoruyyy/",
    imgRede1: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    rede2: "https://www.facebook.com/bruno.ruy.99",
    imgRede2: "./img/projetos/extensao/pod-ri/fb-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-bruno.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Danielle Elis Alves Valdivia",
    rede1: "https://www.linkedin.com/in/daniellevaldiviaunesp/",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.facebook.com/danielle.a.valdivia/",
    imgRede2: "./img/projetos/extensao/pod-ri/fb-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-danielle.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Enzo Mendes Golfetti",
    rede1: "https://www.linkedin.com/in/enzogolfetti/",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-enzo.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Gustavo Pasqueta",
    rede1: "https://www.linkedin.com/in/gustavo-pasqueta/",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.instagram.com/gustapasqueta/",
    imgRede2: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-gustavo.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Leonardo Pagano Landucci",
    rede1: "http://lattes.cnpq.br/1338768819301424",
    imgRede1: "./img/projetos/extensao/pod-ri/lattes-logo.png",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-leonardo.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Samuel Davis Domingues Lima",
    rede1: "https://www.linkedin.com/in/samuel-davis-domingues-lima-61843b1b3",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.instagram.com/samueldavisd/",
    imgRede2: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-samuel.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Sofia Navarro Aguiar",
    rede1: "http://lattes.cnpq.br/8344699371908069",
    imgRede1: "./img/projetos/extensao/pod-ri/lattes-logo.png",
    rede2: "https://www.linkedin.com/in/sofia-navarro-aguiar-23a435a3/",
    imgRede2: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-sofia.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Stephany Anicezio de Souza Primo",
    rede1: "https://www.instagram.com/stephyanicezio/?hl=pt-br",
    imgRede1: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    rede2: "https://www.linkedin.com/in/stephanyanicezio",
    imgRede2: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-stephany.png",
  },
  {
    projeto: "pod-ri - labri-unesp",
    nome: "Victoria dos Anjos Gois",
    rede1: "https://www.linkedin.com/in/victoria-dos-anjos-gois-079563204/",
    imgRede1: "./img/projetos/extensao/pod-ri/linkedin-logo.svg",
    rede2: "https://www.instagram.com/torigois/",
    imgRede2: "./img/projetos/extensao/pod-ri/ig-logo.svg",
    imgMembro: "./img/projetos/extensao/pod-ri/pod-victoria.png",
  },
];

const redessociais = [
  {
    projeto: "pod-ri - labri-unesp",
    imgRedes: "./img/projetos/extensao/pod-ri/redes-sociais.svg",
    description: "ACOMPANHE O Pod-RI NAS REDES SOCIAIS",
    info: "Siga nossas redes sociais para ficar por dentro de conteúdos exclusivos e saber antecipadamente sobre o lançamento de novos episódios.",
  },
];

function Intro({ text, projeto, imgIntro }) {
  return (
    <div className={clsx("hero hero--primary", stylesPod.heroBannerPod)}>
      <div className={stylesPod.LogoPod}>
        <img src={imgIntro} alt={projeto} />
      </div>
      <div className={styles.hero__subtitle}>
        <p>{text}</p>
      </div>
    </div>
  );
}

function Sobre({
  description,
  projeto,
  imgSobre,
  imageUrl,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col col--6">
            <img src={imgSobre} alt={projeto}></img>
          </div>
          <div className={clsx(stylesPod.pSobrePod, "col col--6")}>
            <p>{description}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

function EpPod({ description, imageUrl, imgEps, EpsPod, info, nome }) {
  const ImagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.pEpPod)}>
      <h3>Episódios Disponíveis</h3>
      <div className="container">
        <div className="row">
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <img src={imgEps} alt={EpsPod}></img>
          </div>
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <h3>{nome}</h3>
            <p>{description}</p>
            <h3>
              <a href="https://anchor.fm/pod-ri/episodes/Pod-RI-3-12---Trabalhando-com-Assistncia-de-Refugiados-com-Miguel-Pachioni-e12qg3i">
                Acesse Agora
              </a>
            </h3>
          </div>
          <div className={clsx(stylesPod.pEpPod, "col col--4")}>
            <p>{info}</p>
            <h3>
              <a href="https://linktr.ee/podri">Linktr.ee</a>
            </h3>
          </div>
        </div>
      </div>
    </div>
  );
}

function Membro({
  imageUrl,
  nome,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
  imgMembro,
  membro,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(stylesPod.pEquipe2, "col col--6")}>
      <h3>{nome}</h3>
      <div className={clsx(stylesPod.imagemEquipe)}>
        <img src={imgMembro} alt={membro} />
      </div>
      <div className={stylesPod.RedesLogo}>
        <a href={rede1}>
          <img src={imgRede1} alt={rede1} />
        </a>
        <a href={rede2}>
          <img src={imgRede2} alt={rede2} />
        </a>
      </div>
    </div>
  );
}

function RedesSociais({ imageUrl, redes, info, imgRedes, description }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className="container">
      <div className="row">
        <div className={clsx(stylesPod.RedesSociais, "col col--12")}>
          <img src={imgRedes} alt={redes} />
        </div>
        <div className={(stylesPod.Sociais, "col col--12")}>
          <h2>{description}</h2>
          <div className={stylesPod.pSociais}><h4>{info}</h4></div>
            <a className="button button--secondary" href="https://www.instagram.com/podr_i/">Instagram</a>
            <a className="button button--secondary" href="https://www.facebook.com/podcastRI/">Facebook</a>

        </div>
      </div>
    </div>
  );
}

function SobrePod() {
  return (
    <Layout title="Sobre Pod-RI">
      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(stylesPod.Sobre, styles.container)}>
            <div className="row">
              {sobre.map((props, idx) => (
                <Sobre key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Sobre, styles.container)}>
            <div className="row">
              {eppod.map((props, idx) => (
                <EpPod key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.Membro, styles.container)}>
            <div className="row">
              {membro.map((props, idx) => (
                <Membro key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(stylesPod.RedesSociais, styles.container)}>
            <div className="row">
              {redessociais.map((props, idx) => (
                <RedesSociais key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default SobrePod;
