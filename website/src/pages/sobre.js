import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

const membros = [
  {
    nome: "Pedro Henrique Campagna Moura da Silva",
    cargo: "estagiário",
    rede1: "https://gitlab.com/pedrohcmds",
    imgRede1: "./img/social/gitlab.png",
    rede2: "https://www.linkedin.com/in/pedro-h-205ab0b4/",
    imgRede2: "./img/social/linkedin.png",
    rede3: "http://lattes.cnpq.br/1387918525684397",
    imgRede3: "./img/social/lattes.png",
    imageUrl: "img/equipe/pedrohcmds.jpg",
    descricao: (
      <>
        Graduando do curso de Relações Internacionais pela Faculdade Estadual
        Paulista Julio Mesquita FIlho (UNESP) Campus de Franca. Técnico em
        Informática formado pelo Instituto Federal de Ciência e Tecnologia do
        Mato Grosso do Sul (IFMS) Campus Campo Grande. Participante do QUANTA
        2014 - INTERNATIONAL COMPETITION FOR SCIENCE, MATHEMATICS, MENTAL
        ABILITY AND ELECTRONICS em Lucknow, Índia; Finalista da FEBRACE, 2015;
        Finalista da Fecintec 2014; Finalista da FETEC 2014; Finalista da
        FEBRACE 2015; Vencedor do prêmio ABRIC de iniciação científica.
      </>
    ),
  },
  {
    nome: "Rafael de Almeida",
    cargo: "Colaborador",
    rede1: "www.linkedin.com/akgjajk",
    rede1: "https://twitter.com/rafardealmeida",
    imgRede1: "./img/social/twitter.png",
    rede2: "http://lattes.cnpq.br/5174307461578307",
    imgRede2: "./img/social/lattes.png",
    rede3:
      "https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/",
    imgRede3: "./img/social/linkedin.png",
    imageUrl: "img/equipe/rafael.png",
    descricao: (
      <>
        Mestre em Relações Internacionais pela Universidade Federal de
        Uberlândia (UFU). Possui graduação em Relações Internacionais pela
        Universidade Estadual Paulista "Julio de Mesquita Filho", Campus Franca
        (2012). Membro do Laboratório de Novas Tecnologias de Pesquisa em
        Relações Internacionais (LANTRI/UNESP), que integra a Rede de Pesquisa
        em Política Externa e Regionalismo (REPRI). Entre 2008 e 2011 foi
        redator-colaborador do Observatório de Política Externa Brasileira
        (OPEB) vinculado ao Grupo de Estudos de Defesa e Segurança Internacional
        (GEDES/UNESP). Foi assistente de pesquisa no Centro de Estudos de
        Cultura Contemporânea (CEDEC). Na graduação foi bolsista PIBIC/CNPq e,
        posteriormente, bolsista FAPESP. No mestrado foi bolsista FAPEMIG.
      </>
    ),
  },
  {
    nome: "Marcelo Passini Mariano",
    cargo: "Diretor do LabRI",
    rede1: "www.linkedin.com/akgjajk",
    rede1: "https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ",
    imgRede1: "./img/social/google.png",
    rede2: "https://orcid.org/0000-0002-1159-2537",
    imgRede2: "./img/social/orcid.png",
    rede3: "http://lattes.cnpq.br/3505849964932313",
    imgRede3: "./img/social/lattes.png",
    imageUrl: "img/equipe/marcelo.jpg",
    descricao: (
      <>
        Possui graduação em Ciências Sociais pela Universidade de São Paulo
        (1992), mestrado em Ciência Política pela Universidade de São Paulo
        (1998) e doutorado em Sociologia pela Universidade Estadual Paulista
        (2007). Professor Assistente Doutor do curso de Relações Internacionais
        da Faculdade de Ciências Humanas e Sociais da Universidade Estadual
        Paulista - UNESP/Campus de Franca. Professor do Programa de
        Pós-Graduação San Tiago Dantas (UNESP/UNICAMP/PUC-SP). Coordenador do
        Laboratório de Novas Tecnologias de Pesquisa em Relações Internacionais
        (LANTRI /FCHS /UNESP), que integra a Rede de Pesquisa em Política
        Externa e Regionalismo (REPRI). É pesquisador do Centro de Estudos de
        Cultura Contemporânea (CEDEC). Tem experiência na área de Relações
        Internacionais com pesquisas nos seguintes temas: processos de
        integração regional (Mercosul, Alca, Relação Mercosul União Européia e
        integração na América Latina), política externa brasileira, controle
        democrático de política externa, processos de tomada de decisão e gestão
        de relações internacionais de governos não-centrais (paradiplomacia),
        técnicas de pesquisa em ciências humanas e o uso das tecnologias de
        informação e comunicação.
      </>
    ),
  },
  {
    nome: "Júlia Silveira",
    cargo: "Estagiária",
    rede1: "https://gitlab.com/rikamishiro",
    imgRede1: "./img/social/gitlab.png",
    rede2: "https://www.linkedin.com/in/j%C3%BAlia-silveira-32325b19b/",
    imgRede2: "./img/social/linkedin.png",
    rede3: "Lattes",
    imgRede3: "./img/social/lattes.png",
    imageUrl: "img/equipe/julia.jpg",
    descricao: (
      <>
        Graduanda em Relações Internacionais pela Faculdade Estadual Paulista
        Julio Mesquita Filho (UNESP) Campus de Franca. Membro do Grupo de
        Pesquisa e Estudos da Ásia (GPEA) da Faculdade Estadual Paulista Julio
        Mesquita Filho (UNESP) Campus de Franca.
      </>
    ),
  },
];

const exMembros = [
  {
    cargo: "Estagiário Remunerado",
    nome: "Ivan Beretta",
    email: ".gh.pinto@unesp.br",
    linkedin: "https://www.linkedin.com/in/berettaivan/",
    imageUrl: "./img/equipe/ivan.jpg",
    periodo: "09/04/2018 - 15/12/2019",
  },
  {
    cargo: "Estagiário Voluntário",
    nome: "Guilherme Henrique",
    email: ".ferracinigabriel@gmail.com",
    linkedin: "",
    imageUrl: "./img/equipe/guilherme.jpg",
    periodo: "09/04/2018 - 15/12/2019",
  },
  {
    cargo: "Estagiário Remunerado / Voluntário",
    nome: "Cassiano Baldin",
    email: "cassianobaldin@hotmail.com",
    linkedin: "https://www.linkedin.com/in/cassiano-baldin/",
    imageUrl: "./img/equipe/cassiano.jpg",
    periodo: "02/04/2018 a 15/12/2019 - 28/09/2016 a 16/12/2016",
  },
  {
    cargo: "Estagiário Remunerado",
    nome: "Virgínia Maria Correa",
    email: "vi.mcorrea@hotmail.com",
    linkedin: "https://www.linkedin.com/in/virg%C3%ADniacorr%C3%AAa/",
    imageUrl: "./img/equipe/virginia.jpg",
    periodo: "01/09/2015 a 31/12/2016",
  },
  {
    cargo: "Estagiário Remunerado",
    nome: "Andrey Ide",
    email: "ide.andrey@gmail.com",
    linkedin: "https://www.linkedin.com/in/andreyide/",
    imageUrl: "./img/equipe/andrey.jpg",
    periodo: "01/04/2014 a 31/12/2014",
  },
  {
    cargo: "Estagiário Voluntário",
    nome: "Jaqueline Trevisan Pigatto",
    email: "jaquetpigatto@gmail.com ",
    linkedin: "https://www.linkedin.com/in/jaqueline-trevisan-pigatto/",
    imageUrl: "./img/equipe/jaqueline.jpg",
    periodo: "01/08/2017 a 07/12/2017",
  },
  {
    cargo: "Estagiário Voluntário",
    nome: "Norberto Vanderlei Simões Filho",
    email: "norberto.filho@live.com",
    linkedin: "",
    imageUrl: "./img/equipe/norberto.jpg",
    periodo: "01/08/2017 a 07/12/2017",
  },
];

const sobre = {
  quem_somos: (
    <>
      <p>
        O Laboratório de Relações Internacionais da Universidade Estadual
        Paulista (LabRI/UNESP), criado em 2011, faz parte da estrutura do
        departamento de Relações Internacionais (DERI) da UNESP, campus Franca.
      </p>
      <p>
        Podemos pensar o LabRI/UNESP como um Hub Acadêmico. O termo Hub é
        comumente utilizado na área de rede de computadores para se referir a um
        equipamento que interliga diferentes computadores, possibilitando com
        que os mesmos troquem informações.
      </p>
      <p>
        Nos últimos anos, esse termo passou a ser utilizado por outras áreas
        além da computação para se referir a um local físico ou virtual onde
        indivíduos e grupos podem compartilhar experiências e usufruir de um
        espaço de infraestrutura comum. Termos como Hub digital ou Hub de
        inovação tem sido utilizados para se referir a iniciativas voltadas a
        estimular e fornecer apoio físico ou virtual a pequenos empreendimentos
        e a startups.
      </p>
      <p>
        O LabRI/UNESP não é um grupo de pesquisa. A partir da breve descrição
        indicada acima, pode-se considerá-lo um Hub acadêmico, pois fornece um
        espaço e infraestrutura física e virtual para que os docentes, discentes
        e grupos de pesquisa a ele vinculados possam desenvolver as suas
        atividades de pesquisa e ensino de extensão, especialmente se tais
        atividades demandam a utilização de tecnologias digitais e o suporte a
        partir de uma infraestrutura computacional.
      </p>
      <p>
        Projetos e iniciativas de docentes e discentes vinculados ao curso de
        Relações Internacionais da UNESP/Franca e ao Programa de Pós-Graduação
        em Relações Internacionais San Tiago Dantas (UNESP, UNICAMP, PUC-SP) são
        desenvolvidos no espaço físico e virtual do LabRI/UNESP.
      </p>
    </>
  ),

  objetivos: (
    <>
      O Laboratório de Relações Internacionais da Universidade Estadual Paulista
      (LabRI/UNESP), criado em 2011, faz parte da estrutura do departamento de
      Relações Internacionais (DERI) da UNESP, campus Franca. O principal
      objetivo que o DERI atribui ao LabRI/UNESP é fornecer apoio, por meio de
      tecnologias digitais, às atividades de ensino, pesquisa e extensão dos
      docentes, discentes e grupos de pesquisa vinculados ao curso de Relações
      Internacionais. Deste modo, o LabRI/UNESP é um espaço voltado a disseminar
      e prover auxílio na utilização de novas tecnologias da informação e
      comunicação. Dentre as atividades desenvolvidas no LabRI se destacam:
      <ul>
        {" "}
        1. Manutenção da infraestrutura computacional disponível no LabRI/UNESP;{" "}
      </ul>
      <ul> 2. Digitalização e pós-processamento de documentação textual </ul>
      <ul>
        {" "}
        3. Implementação de software e serviços de código aberto utilizados para
        fins acadêmicos;{" "}
      </ul>
      <ul>
        {" "}
        4. Auxílio na gestão de dados gerados e coletados pelos docentes,
        discentes e grupos de pesquisa;{" "}
      </ul>
      <ul>
        {" "}
        5. Apoio para a utilização das bases de dados disponíveis no LabRI;{" "}
      </ul>
      <ul> 6. Apoio ao trabalho experimental. </ul>
    </>
  ),

  historico: (
    <>
      <p>
        O Laboratório de Relações Internacionais (LabRI) foi criado em 2011 e
        faz parte da estrutura do departamento de Relações Internacionais (DERI)
        da Unesp, campus Franca.
      </p>
      <p>
        Para iniciar seus trabalhos, em 2012, o LabRI/DERI adquiriu um conjunto
        de 10 computadores de mesa e a diretoria da Unesp, campus Franca,
        concedeu uma sala dentro da Diretoria Técnica de Informática (DTI) para
        que o LabRI/DERI pudesse utilizar os equipamentos, foi feito upgrade de
        10 computadores e foi mantido 10 computadores que já estavam instalados
        no espaço do laboratório.
      </p>
      <p>
        No primeiro semestre de 2014, o LabRI/DERI conseguiu junto a diretoria
        da Unesp, campus Franca, uma bolsa de estágio. Desde então o LabRI/DERI
        tem o suporte de um estagiário remunerado.
      </p>
      <p>
        A partir de 2016, o LabRI/UNESP passou a utilizar o sistema Proxmox para
        virtualizar a infraestrutura computacional disponível, buscando utilizar
        e ampliar o suporte tecnológico aos discentes e docentes.
      </p>
      <p>
        A partir de 2017, o LabRI começou a receber estagiários voluntários.
      </p>
      <p>
        A partir de 2020, passou a incentivar o maior uso de linguagem de
        programação no cotidiano de suas atividades. Além disso, iniciou um
        processo de reestruturação das bases de dados nas quais auxilia a
        manutenção e expansão.
      </p>
    </>
  ),

  suporte: (
    <>
      <p>
        Como já apontado acima, o LabRI é um espaço que visa incentivar e dar
        suporte às atividades que demandam a utilização de tecnologias digitais.
        Devido a isso, podemos destacar duas principais frentes de apoio que o
        LabRI oferece:
      </p>
      <p>
        <h4>1. Infraestrutura computacional</h4>
      </p>
      <p>
        Desde 2016 temos um cluster computacional, baseado no Proxmox - sistema
        open source de virtualização, no espaço do LabRI/UNESP com o objetivo de
        fornecer um apoio computacional mais adequado às atividades de pesquisa.
        A partir do Proxmox provemos:
      </p>
      <p>
        Alguns serviços de apoio a pesquisa com destaque a indexação integral de
        documentos, feita a partir do software livre Recoll, e a realização de
        Reconhecimento Óptico de Caracteres (OCR) automático em documentos de
        imagem e PDFs não pesquisáveis;
      </p>
      <p>
        Armazenamento das bases de dados. Grande parte destas bases são
        consultadas através da indexação realizada no Recoll e estão disponíveis
        para os pesquisadores via acesso remoto (x2go e/ou Chrome Remote
        Desktop).
      </p>
      <p>
        Acesso a um desktop virtual completo através da virtualização de
        sistemas operacional também conhecida como Virtual Desktop
        Infrastructure (VDI) que a utilização conjunta do Proxmox e de software
        de acesso remoto, como o x2go e o Chrome Remote Desktop, proporcionam.
        Os estudantes e professores com recursos computacionais pessoais não
        adequados podem se beneficiar bastante deste recurso.
      </p>
      <p>
        As máquinas em que o Proxmox está instalado se transformam em estações
        multifuncionais de trabalho fornecendo suporte virtual e real a uma gama
        de diversas atividades tanto presenciais como remotas.
      </p>
      <p>
        OBSERVAÇÃO: caso você necessite de informações mais detalhadas sobre os
        equipamentos que compõem a infraestrutura do LabRI/UNESP, entre em
        contato por email.
      </p>
      <h4>2. Projetos integrados e bases de dados</h4>
      <p>
        Uma importante iniciativa que o LabRI/UNESP tem promovido é transformar
        algumas das bases de dados em “Projetos de Dados” para estruturarmos
        melhor a gestão e expansão das mesmas. Esta iniciativa tem aperfeiçoado
        nossa estratégia de coleta, tratamento, armazenamento e disponibilização
        de dados. Além disso, reflete um acúmulo de anos de trabalho voltado
        para a sistematização de dados relevantes para as pesquisas de Relações
        Internacionais.
      </p>
      <p>
        Deste modo, passamos a estruturar essas bases de dados em um projeto
        integrado, conhecido também como projeto guarda-chuva, que abarca
        subprojetos responsáveis na gestão das várias bases de dados
        disponíveis. Para mais detalhes, <a href="./projetos">clique aqui</a>.
      </p>
    </>
  ),

  grupos_pesquisa: (
    <>Conheça mais sobre os grupos de pesquisa que são parceiros do LabRI</>
  ),
};

function Membro({
  imageUrl,
  nome,
  cargo,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
  rede3,
  imgRede3,
  descricao,
}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className="avatar">
      <div className={styles.membro}>
        <div className={styles.foto_perfil}>
          {imgUrl && <img className="avatar__photo" src={imgUrl} alt={nome} />}
        </div>
        <div className="avatar__intro">
          <h4 className="avatar__name">{nome}</h4>
          <small className="avatar__subtitle">{cargo}</small>
        </div>
        <div className="membro_info">
          <p>{descricao}</p>
          <a href={rede1}>
            <img
              className={styles.loginho}
              src={useBaseUrl(imgRede1)}
              alt={rede1}
            />
          </a>
          <a href={rede2}>
            <img
              className={styles.loginho}
              src={useBaseUrl(imgRede2)}
              alt={rede2}
            />
          </a>
          <a href={rede3}>
            <img
              className={styles.loginho}
              src={useBaseUrl(imgRede3)}
              alt={rede3}
            />
          </a>
        </div>
      </div>
    </div>
  );
}

function ExMembro({ imageUrl, nome, cargo, email, linkedin, periodo }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.exMembro}>
      <div className={styles.exMembroFoto}>
        {imgUrl && <img className="avatar__photo" src={imgUrl} alt={nome} />}
      </div>
      <div className="avatar__intro">
        <h4 className="avatar__name">{nome}</h4>
        <small className="avatar__subtitle">{cargo}</small>
        <small className="avatar__subtitle">{periodo}</small>
      </div>
      <div className={clsx(styles.redes_sociais, "membro_info")}>
        <a href="mailto:{email}">
          <img
            className={styles.loginho}
            src={useBaseUrl("./img/social/email.png")}
            alt={email}
          />
        </a>
        <a href={linkedin}>
          <img
            className={styles.loginho}
            src={useBaseUrl("./img/social/linkedin.png")}
            alt={linkedin}
          />
        </a>
      </div>
    </div>
  );
}

function SobreLabri() {
  return (
    <Layout title="SobreLabRI">
      <main className={styles.main}>
        <header className={clsx("hero hero--primary", styles.heroBanner)}>
          <div className={styles.container}>
            <h1 className="hero__title">Sobre o LabRI</h1>
            <h2 className="hero__subtitle">Conheça melhor o LabRI</h2>
          </div>
        </header>
        <section className={styles.content}>
          <div className={styles.container}>
            <div className={styles.sobre}>
              <div className={styles.sobre_box}>
                <h3>Quem Somos</h3>
                <p>{sobre.quem_somos}</p>
              </div>
              <div className={styles.sobre_box}>
                <h3>Objetivos</h3>
                <p>{sobre.objetivos}</p>
              </div>
              <div className={styles.sobre_box}>
                <h3>Histórico</h3>
                <p>{sobre.historico}</p>
              </div>
              <div className={styles.sobre_box}>
                <h3>Suporte aos docentes, discentes e demais colaboradores</h3>
                <p>{sobre.suporte}</p>
              </div>
              <div className={styles.sobre_box}>
                <h3>Grupos de Pesquisa</h3>
                <p>{sobre.grupos_pesquisa}</p>
                <a class="button button--secondary" href="grupos-pesquisa">
                  Saiba Mais
                </a>
              </div>
            </div>
            <section id="equipe">
              <div className={styles.equipe}>
                <h3> Equipe </h3>
                {membros && membros.length > 0 && (
                  <section className={styles.membros}>
                    {membros.map((props, idx) => (
                      <Membro key={idx} {...props} />
                    ))}
                  </section>
                )}
              </div>
            </section>
          </div>
          <section id="ex-membros">
            <div className={styles.equipe}>
              <h3> Ex-Membros da Equipe </h3>
              {exMembros && exMembros.length > 0 && (
                <div className={styles.membros}>
                  {exMembros.map((props, idx) => (
                    <ExMembro key={idx} {...props} />
                  ))}
                </div>
              )}
            </div>
          </section>
        </section>
      </main>
    </Layout>
  );
}

export default SobreLabri;
