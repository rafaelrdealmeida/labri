import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";
import Slider from "react-slick";
import 'slick-carousel/slick/slick.css';

const sobre = [
  {
    projeto: "equipe - labri-unesp",
    title: "Quem Sou",
    imgFoto: "/img/membros-pag/pag-fabio.svg",
    foto: "foto do membro da equipe",
    text: (
      <>
      <b>Fábio de Oliveira Paron. 21 anos. Graduando em Relações Internacionais - 2º ano. Estagiário pelo LabRI/UNESP desde jun/2021. </b>
      </>
    )
  }
]  

const estudos = [
  {
    projeto: "equipe - labri-unesp",
    title: "Formação Acadêmica",
    text1: (
      <>
      <ul><b>Graduando em Relações Internacionais</b></ul>
      {/* <ul><b>Outros Títulos Acadêmicos</b></ul>
      <ul><b>Outros feitos acadêmicos:</b></ul>
        <ul><ul><li><b>Curso 1 (opcional)</b></li></ul></ul>
        <ul><ul><li><b>Curso 2 (opcional)</b></li></ul></ul> */}
      <ul><b>Línguas:</b></ul>
        <ul><ul><li><b>Português</b>: nativo</li></ul></ul>
        <ul><ul><li><b>Inglês:</b> avançado</li></ul></ul>
      </>
    ),
    imgFoto: "/img/membros-pag/Logo_Unesp.svg",
    text2: (
      <>
      <ul><b>Área de maior interesse: </b> <i>Desenvolvimento Back End</i></ul>
      <ul><b>Principais conhecimentos de programação:</b></ul>
      <ul><li><i>Python</i></li></ul>
      <ul><li><i>Markdown</i></li></ul>
      </>
    )
  }
]

const conhecimentos = [
  {
    projeto: "equipe - labri-unesp",
    title: (
     <> <center>Conhecimentos e Interesses</center>
     </>
    ),
    subtitle: (
      <>
      <center>Estuda programação como parte da rotina do estágio; graduando no segundo ano de Relações Internacionais pela UNESP - Franca</center>
      </>
    )
  }
]

const interesses = [
  {
    projeto: "equipe- labri-unesp",
    imgFoto: "/img/membros-pag/portfolio-09.svg",
    text: (
      <>
      <ul><h2>Lógica da programação</h2></ul>
      <ul>Lógica da programação aplicada em tarefas simples e cotidianas que auxiliam o LabRI/UNESP</ul>
      </>
    ),
    text2: (
      <>
      <ul><h2>Linguagem Python</h2></ul>
      <ul>Breves conhecimentos a respeito da linguagem Python</ul>
      </>
    ),
    imgFoto2: "/img/membros-pag/python.svg"
  },
  {
  //   projeto: "equipe- labri-unesp",
  //   imgFoto: "/img/membros-pag/portfolio-10.svg",
  //   text: (
  //     <>
  //     <ul><h2>SQL</h2></ul>
  //     <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
  //     </>
  //   ),
  //   text2: (
  //     <>
  //     <ul><h2>Adobe Photoshop</h2></ul>
  //     <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
  //     </>
  //   ),
  //   imgFoto2: "/img/membros-pag/portfolio-12.svg"
  // },
  // {
  //   projeto: "equipe- labri-unesp",
  //   imgFoto: "/img/membros-pag/portfolio-09.svg",
  //   text: (
  //     <>
  //     <ul><h2>CSS3 E HTML5</h2></ul>
  //     <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
  //     </>
  //   ),
  //   text2: (
  //     <>
  //     <ul><h2>JavaScript</h2></ul>
  //     <ul>Breve introdução do conhecimento e da experiência da pessoa com relação aos conhecimentos apontados acima (Ex: explicar os principais conhecimentos de Python e há quanto tempo utiliza essa linguagem)</ul>
  //     </>
  //   ),
  //   imgFoto2: "/img/membros-pag/javascript.svg"
  },
]

const footer = [
  {
    projeto: "equipe - labri-unesp",
    text: (
      <>
      <ul><b>ONDE ME ENCONTRAR</b></ul>
      </>
    ),
    imgLogo: "/img/membros-pag/portfolio-04.svg",
    imgLogo2: "/img/membros-pag/portfolio-02.svg",
    link2: "https://gitlab.com/f.paron",
    imgLogo3: "/img/membros-pag/portfolio-03.svg",
    imgFoto: "/img/membros-pag/portfolio-01.svg",
    title: "ENTRE EM CONTATO",
    text2: "f.paron@unesp.br ou fabioparon@hotmail.com"
  }
]


function Footer({text, logo, imgLogo, logo2, imgLogo2, logo3, imgLogo3, foto, imgFoto, text2, title}){
  return(
    <div>
      <div className={clsx(styles.fundo3, "container")}>
        <div className="row">
          <div className="col col--4">
            <h2 className={clsx(styles.tFooter)}><b>{text}</b></h2>
            <img className={clsx(styles.Logos)} src={imgLogo} alt={logo} />
            <img className={clsx(styles.Logos2)} src={imgLogo2} alt={logo2} />
            <img className={clsx(styles.Logos2)} src={imgLogo3} alt={logo3} />
          </div>
          <div className="col col--4">
            <img src={imgFoto} alt={foto} />
          </div>
          <div className="col col--4">
            <h2 className={clsx(styles.tFooter)}><b>{title}</b></h2>
            <p className={clsx(styles.pFooter)}>{text2}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

function Interesses({foto, imgFoto, foto2, imgFoto2, text, text2}){
  return(
    <div>
      <div className={clsx(styles.fundo, "container")}>
        <div className="row">
          <div className="col col--4">
            <img src={imgFoto} alt={foto} />
          </div>
          <div className="col col--8">
            <p className={clsx(styles.tInteresses)}>{text}</p>
          </div>
            <div className="col col--8">
              <p className={clsx(styles.tInteresses)}>{text2}</p>
            </div>
            <div className="col col--4">
              <img src={imgFoto2} alt={foto2} />
            </div>
        </div>
      </div>
    </div>
  )
}

function Conhecimentos({title, subtitle}){
  return(
    <div>
      <div className={clsx(styles.fundo)}>
      <div className={clsx(styles.titleConhecimentos)}>
            {title}
      </div>
      <div className={clsx(styles.subConhecimentos)}>
            {subtitle}
        </div>
    </div>
    </div>
  )
}

function Estudos({title, foto, imgFoto, text1, text2}){
  return(
    <div>
      <div className={clsx(styles.fundo2, "container")}>
        <div className="row">
          <div className="col col--6">
            <h2 className={clsx(styles.tEstudos)}>{title}</h2>
            <p className={clsx(styles.pEstudos)}>{text1}</p>
          </div>
          <div className="col col--6">
            <img src={imgFoto} alt={foto} />
            <p className={clsx(styles.pEstudos)}>{text2}</p>
          </div>
        </div>
      </div>
    </div>
  )
}

function Sobre({foto, imgFoto, title, text, imageUrl}){
  const ImagemInfo = useBaseUrl(imageUrl);
  return(
    <div>
    <div className={clsx(styles.fundo, "container")}>
      <div className="row">
        <div className="col col--6">
          <img src={imgFoto} alt={foto}></img>
        </div>
        <div className="col col--6">
          <h2 className={clsx(styles.tSobreEquipe)}><b>{title}</b></h2>
          <p className={clsx(styles.pSobreEquipe)}>{text}</p>
        </div>
      </div>
    </div>
    </div>
  )
}

function Equipe(){
    return(
        <Layout title="Fábio Paron">
        <header className={clsx('hero hero--primary', styles.heroBanner)}>
          <div className={styles.container}>
            <h1 className={clsx(styles.hero__title)}>Fábio Paron</h1>
            <h2 className={clsx(styles.hero__subtitle)}>Estagiário</h2>
          </div>
        </header>
        <main className={clsx(styles.pagCor)}>
          <section className="content">
          <div className="container">
            <div className="row">
              {sobre.map((props, idx) => (
                <Sobre key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className="row">
              {estudos.map((props, idx) => (
                <Estudos key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className="row">
              {conhecimentos.map((props, idx) => (
                <Conhecimentos key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className="row">
              {interesses.map((props, idx) => (
                <Interesses key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className="container">
            <div className="row">
              {footer.map((props, idx) => (
                <Footer key={idx} {...props} />
              ))}
            </div>
          </div>
          </section>
        </main>

        </Layout>
    )
}

export default Equipe;
