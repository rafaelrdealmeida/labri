import React from 'react';
import Layout from '@theme/Layout';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';

function Slick() {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    };
  
  
    return (
    <Layout title="Hello">
        <head>
           {/* <link
                rel="stylesheet"
                type="text/css"
                charset="UTF-8"
                href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
                /> */}
                <link
                rel="stylesheet"
                type="text/css"
                href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
                />
       </head> 
        <Slider {...settings}>
            <div>
                <h3><img src="https://i.imgur.com/J47EtrJ.jpg"/> </h3>
            </div>
            <div>
                <h3><img src="https://i.imgur.com/jpI9oMY.png"/> </h3>
            </div>
             <div>
                <h3><img src="https://i.imgur.com/4imPA0A.jpg"/> </h3>
            </div>
        </Slider>
      
    </Layout>
  );
}

export default Slick;