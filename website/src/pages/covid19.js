import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

function Noticias() {
  return (
    <Layout title="Noticias">
      <main className={styles.main}>
        <h1 className={styles.heading}>Olá Mundo</h1>
        <article className={styles.contents}>
          Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum
        </article>
        <section class={styles.content}>
          <h1>Notícias</h1>
        </section>
      </main>
    </Layout>
  );
}

export default Noticias;
