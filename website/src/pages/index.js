import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

const docs = [
  {
    title: "Tecnologias Digitais",
    text: (
      <>
        Neste espaço reunimos materiais sobre ferramentas tecnológicas feitos
        pela equipe do LabRI/UNESP ou por terceiros que visam auxiliar
      </>
    ),
    link: "/docs/projetos/ensino/tec-digitais/intro",
    imageUrl: "./img/home_tec.svg",
  },
  {
    title: "Oficinas e Cursos",
    text: <>Conheça as oficinas e os cursos produzidos pelo LabRI/UNESP</>,
    link: "/docs/projetos/ensino/intro",
    imageUrl: "./img/home_cursos.svg",
  },
];

const equipe = [
  {
    nome: "Pedro Henrique Campagna",
    cargo: "Estagiário",
    rede1: "https://gitlab.com/pedrohcmds",
    imgRede1: "./img/social/gitlab.png",
    rede2: "https://www.linkedin.com/in/pedrohcmds/",
    imgRede2: "./img/social/linkedin.png",
    rede3: "http://lattes.cnpq.br/1387918525684397",
    imgRede3: "./img/social/lattes.png",
    imageUrl: "./img/equipe/pedrohcmds.jpg",
  },
  {
    nome: "Rafael de Almeida",
    cargo: "Colaborador",
    rede1: "https://twitter.com/rafardealmeida",
    imgRede1: "./img/social/twitter.png",
    rede2: "http://lattes.cnpq.br/5174307461578307",
    imgRede2: "./img/social/lattes.png",
    rede3:
      "https://www.linkedin.com/in/rafael-augusto-ribeiro-de-almeida-083792137/",
    imgRede3: "./img/social/linkedin.png",
    imageUrl: "./img/equipe/rafael.png",
  },
  {
    nome: "Julia Silveira",
    cargo: "estagiária",
    rede1: "https://www.linkedin.com/in/júlia-silveira-32325b19b/",
    imgRede1: "./img/social/linkedin.png",
    rede2: "http://lattes.cnpq.br/2622160206382987",
    imgRede2: "./img/social/lattes.png",
    rede3: "https://gitlab.com/rikamishiro",
    imgRede3: "./img/social/gitlab.png",
    imageUrl: "./img/equipe/julia.jpg",
  },
  {
    nome: "Marcelo Mariano",
    cargo: "Coordenador",
    rede1: "https://scholar.google.com.br/citations?hl=pt-BR&user=UFsVIxQAAAAJ",
    imgRede1: "./img/social/google.png",
    rede2: "https://orcid.org/0000-0002-1159-2537",
    imgRede2: "./img/social/orcid.png",
    rede3: "http://lattes.cnpq.br/3505849964932313",
    imgRede3: "./img/social/lattes.png",
    imageUrl: "./img/equipe/marcelo.jpg",
  },
];

const infos = [
  {
    title: "Bem vindo ao LabRI",
    description: (
      <>
     O Laboratório de Relações Internacionais (LabRI/UNESP) é um espaço voltado a apoiar o desenvolvimento de atividades de pesquisa, ensino e extensão que demandem a utilização de tecnologias digitais.  
      </>
    ),
    link: "./sobre",
    imageUrl: "./img/home_quem_somos.svg",
  },
  {
    title: "Projetos",
    description: (
      <>
        Em conjunto com os grupos e redes de pesquisa vinculados ao LabRI/UNESP,
        desenvolvemos projetos tecnológicos voltados a fornecer um pelo suporte
        às atividades de ensino, pesquisa e extensão.
      </>
    ),
    link: "./projetos",
    imageUrl: "./img/home_projetos.svg",
  },
  {
    title: "Oportunidade de Estágio",
    description: (
      <>
        Atualmente temos uma vaga de estágio remunerado preenchida. Há a
        possibilidade de qualquer estudante se inscrever no estágio voluntário
        (não remunerado) e fazer parte da Equipe do LabRI/UNESP!
      </>
    ),
    link: "./docs/geral/info/estagio",
    imageUrl: "./img/home_estagio.svg",
  },
];

function Info({ title, description, link, imageUrl }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.info, "card col col--4")}>
      <div className="card-content">
        <div class="card__header">
          <h3>{title}</h3>
          {imagemInfo && (
            <div className="text__center">
              <img
                className={styles.featureImage}
                src={useBaseUrl(imageUrl)}
                alt={title}
              />
            </div>
          )}
        </div>
        <div className="card__body">
          <p>{description}</p>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>
            Saiba Mais
          </a>
        </div>
      </div>
    </div>
  );
}

function Doc({ title, text, link, imageUrl }) {
  const imagem = useBaseUrl(imageUrl);
  return (
    <div className={clsx("col col--6", styles.doc)}>
      <h3>{title}</h3>
      {imagem && (
        <div className={styles.ImagemDoc}>
          <img src={useBaseUrl(imageUrl)} alt={title} />
        </div>
      )}
      <p>{text}</p>
      <a className="button button--secondary" href={link}>
        Saiba Mais
      </a>
    </div>
  );
}

function Equipe({
  nome,
  cargo,
  rede1,
  imgRede1,
  rede2,
  imgRede2,
  rede3,
  imgRede3,
  imageUrl,
}) {
  const img = useBaseUrl(imageUrl);
  return (
    <div className={clsx("card-demo", "col", "col--4")}>
      <div className={styles.equipe}>
        <div className="card__header">
          {imageUrl && (
            <div className={styles.imagem_equipe}>
              <a href="./sobre#equipe">
                <img
                  className={styles.imagem_equipe}
                  src={imageUrl}
                  alt={nome}
                />{" "}
              </a>
            </div>
          )}
        </div>
        <div className="card__body">
          <h4>{nome}</h4>
          <h6>{cargo}</h6>
        </div>
        <div className={clsx("card__footer", styles.redes_sociais)}>
          <a href={rede1}>
            <img src={useBaseUrl(imgRede1)} alt={rede1} />
          </a>
          <a href={rede2}>
            <img src={useBaseUrl(imgRede2)} alt={rede2} />
          </a>
          <a href={rede3}>
            <img src={useBaseUrl(imgRede3)} alt={rede3} />
          </a>
        </div>
      </div>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Description will go into a meta tag in <head />"
    >
      <head>
        <link
          href="https://fonts.googleapis.com/css?family=Montserrat"
          rel="stylesheet"
        ></link>
      </head>
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(styles.sobreEstagioProjetos, styles.container)}>
            <div className="row">
              {infos.map((props, idx) => (
                <Info key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.noticiasRevista, styles.container)}>
            <div className="row">
              <div className="card-demo col col--6">
                <div className="card-content">
                  <div className="card__header">
                    <h3>Notícias</h3>
                    <div className={styles.noticiasImg}>
                      <img
                        src={useBaseUrl("./img/home_noticias.svg")}
                        alt="Noticias"
                      />
                    </div>
                  </div>
                  <div className="card__body">
                    <p>
                      Fique por dentro das atividades realizadas no LabRI/UNESP
                    </p>
                  </div>
                </div>
                <div className="card__footer">
                  <a className="button button--secondary" href="./noticias">
                    Ler Mais
                  </a>
                </div>
              </div>
              <div className="card-demo col col--6">
                <div className="card-content">
                  <div className="card__header">
                    <h3>Publicações</h3>
                    <div className={styles.noticiasImg}>
                      <img
                        src={useBaseUrl("./img/home_revista.svg")}
                        alt="Revista"
                      />
                    </div>
                  </div>
                  <div className="card__body">
                    <p>
                      Neste espaço divulgamos os textos produzidos pela equipe
                      do LabRI/UNESP e por participantes de seus projetos. Além
                      disso, este é um espaço em que os grupos e redes de
                      pesquisa vinculados ao LabRI/UNESP pode utilizar para
                      publicar seus trabalhos
                    </p>
                  </div>
                </div>
                <div className="card__footer">
                  <a className="button button--secondary" href="./cadernos">
                    Ler Mais
                  </a>
                </div>
              </div>
            </div>
          </div>

          <div className={clsx(styles.docsLabri, styles.container)}>
            <div className="row">
              {docs.map((props, idx) => (
                <Doc key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.equipe, styles.container)}>
            <h3>Equipe</h3>
            <div className="row">
              {equipe.map((props, idx) => (
                <Equipe key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Home;
