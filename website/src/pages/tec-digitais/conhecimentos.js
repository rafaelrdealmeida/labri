import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const tutoriais = [
    {
        projeto: "conhecimentos - labri-unesp",
        embed1: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/u4P0LOofEFs" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        ),
        text1: (
            <>
            <h3>Bits, bytes, kilos...</h3>
            <p>Tutorial sobre as principais unidades de medidas utilizadas na computação</p>
            </>
        ),
        link1: "https://youtu.be/u4P0LOofEFs",
        imgFoto1: "/img/tec-digitais/t1-conhecimentos.svg",
        embed2: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/X2adch0_WBY" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        ),
        text2: (
            <>
            <h3>Velocidade de transferência de dados: unidades medidas</h3>
            <p>Tutorial sobre a influência das unidades de medidas na transferência de dados</p>
            </>
        ),
        link2: "https://youtu.be/X2adch0_WBY",
        imgFoto2: "/img/tec-digitais/t2-conhecimentos.svg"
    },
    {
        projeto: "conhecimentos - labri-unesp",
        embed1: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/OK2u0WEKJCg" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        ),
        text1: (
            <>
            <h3>Dados pessoais: cópias de segurança</h3>
            <p>Tutorial sobre como proteger seus dados</p>
            </>
        ),
        link1: "https://youtu.be/OK2u0WEKJCg",
        imgFoto1: "/img/tec-digitais/t3-conhecimentos.svg",
        embed2: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/KJ7whRUOvNQ" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        ),
        text2: (
            <>
            <h3>Imagens e vídeos: pixel, resolução e proporção de imagens</h3>
            <p>Tutorial sobre como manusear imagens e vídeos</p>
            </>
        ),
        link2: "https://youtu.be/KJ7whRUOvNQ",
        imgFoto2: "/img/tec-digitais/t4-conhecimentos.svg"
    },
]

const footer = [
    {
        projeto: "conhecimentos - labri-unesp",
        link: "/tec-digitais"
    }
]

function Footer({link}){
    return(
        <div className={clsx(styles.containerT)}>
            <h2 className={clsx(styles.highlightC)}><a href={link}>Voltar para a página principal</a></h2>
        </div>
    )
}

function Tutoriais({embed1, embed2, text1, imgFoto1, text2, imgFoto2, link1, link2}){
    return(
        <div className={clsx(styles.containerT)}>
            <div className="row">
                <div className="col col--6">
                    <p className={clsx(styles.pSobre)}>{embed1}</p>
                </div>
                <div className="col col--6">
                <p className={clsx(styles.pSobre)}>{text1}</p>
                <a className="button button--secondary" href={link1}>Acesse Aqui</a>
                </div>
                <div className="col col--6">
                    <p className={clsx(styles.pSobre)}>{text2}</p>
                    <a className="button button--secondary" href={link2}>Acesse Aqui</a>
                </div>
                <div className="col col--6">
                <p className={clsx(styles.pSobre)}>{embed2}</p>
                </div>
            </div>
        </div>
    )
}

function Conhecimentos(){
    return(
        <Layout title="Conhecimentos Básicos">
            <header className={clsx(styles.heroBanner, "hero hero--primary")}>
                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.hero__title)}>
                        Conhecimentos Básicos
                    </div>
                </div>
            </header>
            <main>
        <section className={styles.content}>
            <div className={clsx(styles.container)}>
                <h1 className={clsx(styles.highlightH)}>TUTORIAIS</h1>
            </div>

            <div className={clsx(styles.container)}>
                <div className="row">
                {tutoriais.map((props, idx) => (
                 <Tutoriais key={idx} {...props} />
                ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
                <div className="row">
                {footer.map((props, idx) => (
                 <Footer key={idx} {...props} />
                ))}
            </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default Conhecimentos;