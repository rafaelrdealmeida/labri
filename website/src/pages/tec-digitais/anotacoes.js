import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";



const sobre = [
    {
        projeto: "anotacoes - labri-unesp",
        title1: "Sugestões",
        text1: (
            <>
            <a href="https://youtu.be/gn9Vth3r5h4">Documentação acadêmica: explicação sobre os apontamentos e o uso de softwares e serviços</a>
            </>
        ),
        title2: "Mapas Mentais",
        imgFoto1: "/img/tec-digitais/t1-anotacoes.svg",
        text2: (
            <>
            <li><b>Freeplane:</b> Software de Mapa Mental baseado no Freemind</li>
            <ul><ul><li><i>Sistema:</i> Linux / Windows / Mac OS</li></ul></ul>
            <ul><ul><li><i>Custo:</i> Gratuito</li></ul></ul>
            <ul><ul><li><a href="https://www.freeplane.org/wiki/index.php/Home">Site Oficial</a></li></ul></ul>
            <li><b>MindMup:</b> Mapa Mental online</li>
            <ul><ul><li><i>Sistema:</i> Linux / Windows / Mac OS</li></ul></ul>
            <ul><ul><li><i>Custo:</i> Gratuito e Planos Pagos</li></ul></ul>
            <ul><ul><li><i>Exporta e importa arquivos .mm (Freemind)</i></li></ul></ul>
            <ul><ul><li><i>Salva arquivos no Google Drive</i></li></ul></ul>
            <ul><ul><li><a href="https://www.mindmup.com/">Site Oficial</a></li></ul></ul>
            </>
        ),
        title3: "Exemplos de anotações realizadas",
        imgFoto2: "/img/tec-digitais/t2-anotacoes.svg",
        embed1: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/TtW0m1Ie5ls" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        ),
        text3: (
            <>
            <ul><ul><li><b>Observação:</b> Anotação feita por uma estudante em arquivo de texto (PDF pesquisável) combinando marcas coloridas, elementos gráficos e apontamentos próprios nas últimas páginas do arquivo. As cores utilizadas seguem um padrão que foi definido pela estudante e atende suas necessidades.</li></ul></ul>
            <ul><ul><li><b>Software:</b> Leitor de PDF como o Adobe Reader, PDFxchange, Okular, etc.</li></ul></ul>
            </>
        )
    }
]

const footer = [
    {
        projeto: "conhecimentos - labri-unesp",
        link: "/tec-digitais"
    }
]

function Footer({link}){
    return(
        <div className={clsx(styles.containerT)}>
            <h2 className={clsx(styles.highlightC)}><a href={link}>Voltar para a página principal</a></h2>
        </div>
    )
}

function Sobre({title1, title2, title3, text1, text2, text3, imgFoto1, foto1, embed1}){
    return(
        <div className={clsx(styles.containerT)}>
            <div className="row">
                <div className="col col--10">
                    <h2 className={clsx(styles.highlightF)}>{title1}</h2>
                    <p className={styles.pSobre2}>{text1}</p>
                </div>
                <div className="col col--4">
                    <img className={clsx(styles.Sobre)} src={imgFoto1} alt={foto1} />
                </div>
                <div className="col col--8">
                    <h2 className={clsx(styles.highlightD)}>{title2}</h2>
                    <p className={styles.pSobre}>{text2}</p>
                </div>
                <div className="col col--6">
                    <h2 className={clsx(styles.highlightE)}>{title3}</h2>
                    <p className={styles.pSobre}>{text3}</p>
                </div>
                <div className="col col--6">
                    <p className={clsx(styles.pSobre)}>{embed1}</p>
                </div>
            </div>
        </div>
    )
}

function Anotacoes(){
    return(
        <Layout title="Tutoriais sobre Anotações">
            <header className={clsx(styles.heroBanner, "hero hero--primary")}>
                <div className={clsx(styles.container)}>
                    <h1 className={clsx(styles.hero__title)}>
                        Tutoriais sobre Anotações
                    </h1>
                </div>
            </header>
            <main>
        <section className={styles.content}>
            <div className={clsx(styles.container)}>
                <div className="row">
                {sobre.map((props, idx) => (
                 <Sobre key={idx} {...props} />
                ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
                <div className="row">
                {footer.map((props, idx) => (
                 <Footer key={idx} {...props} />
                ))}
            </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default Anotacoes;