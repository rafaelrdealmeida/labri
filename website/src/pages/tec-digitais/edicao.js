import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";


const tutorial = [
    {
        projeto: "edicao - labri-unesp",
        title: "Editores de texto online",
        text: (
            <>
            <li><b>Documentos Google</b></li>
            <ul><ul><li><i>Custo:</i> Gratuito e Planos Pagos</li></ul></ul>
            <ul><ul><li><a href="https://docs.google.com/document/u/0/">Site Oficial</a></li></ul></ul>
            <ul><ul><li><i>Vídeo:</i> <a href="https://youtu.be/0DeJgsdZtZA">Documentos Google - Como formatar e inserir sumário automático</a> <i>(Duração: 1m38s)</i></li></ul></ul>
            </>
        ),
        imgFoto: "/img/tec-digitais/t1-edicoes.svg"
    }
]

const footer = [
    {
        projeto: "conhecimentos - labri-unesp",
        link: "/tec-digitais"
    }
]

function Footer({link}){
    return(
        <div className={clsx(styles.containerT)}>
            <h2 className={clsx(styles.highlightC)}><a href={link}>Voltar para a página principal</a></h2>
        </div>
    )
}

function Tutorial({title, text, imgFoto, foto}){
    return(
        <div className={clsx(styles.containerT)}>
            <div className="row">
                <div className="col col--6">
                    <h2 className={clsx(styles.highlightG)}>{title}</h2>
                    <p className={clsx(styles.pSobre)}>{text}</p>
                </div>
                <div className="col col--4">
                    <img className={clsx(styles.Sobre)} src={imgFoto} alt={foto} />
                </div>
            </div>
        </div>
    )
}

function Edicao(){
    return(
        <Layout title="Tecnologias para Edição de Textos">
            <header className={clsx(styles.heroBanner, "hero hero--primary")}>
                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.hero__title)}>
                        Tutoriais sobre Edição de Textos
                    </div>
                </div>
            </header>
            <main>
        <section className={styles.content}>
            <div className={clsx(styles.container)}>
                <div className="row">
                {tutorial.map((props, idx) => (
                 <Tutorial key={idx} {...props} />
                ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
                <div className="row">
                {footer.map((props, idx) => (
                 <Footer key={idx} {...props} />
                ))}
            </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default Edicao;