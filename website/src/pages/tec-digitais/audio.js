import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const tutorial = [
    {
        projeto: "audio - labri-unesp",
        title: "Transcrição Instantânea",
        text: (
            <>
            <li><b>Custo:</b> gratuito</li>
            <li><b>Sistema:</b> Android</li>
            <li><a href="https://play.google.com/store/apps/details?id=com.google.audio.hearing.visualization.accessibility.scribe&hl=pt_BR">Site Oficial</a></li>
            </>
        ),
        embed: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/OOdVvRTKo5M" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        )
    }
]

const footer = [
    {
        projeto: "conhecimentos - labri-unesp",
        link: "/tec-digitais"
    }
]

function Footer({link}){
    return(
        <div className={clsx(styles.containerT)}>
            <h2 className={clsx(styles.highlightC)}><a href={link}>Voltar para a página principal</a></h2>
        </div>
    )
}

function Tutorial({title, text, embed}){
    return(
        <div className={clsx(styles.container)}>
            <h2><mark>{title}</mark></h2>
            <div className="row">
                <div className="col col--6">
                    <p className={clsx(styles.pSobre)}>{text}</p>
                </div>
                <div className="col col--6">
                    <div className={clsx(styles.pSobre)}>{embed}</div>
                </div>

            </div>
        </div>
    )
}

function Audio(){
    return(
        <Layout title="Áudio">
            <header className={clsx(styles.heroBanner, "hero hero--primary")}>
                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.hero__title)}>
                        Áudio
                    </div>
                </div>
            </header>
            <main>
        <section className={styles.content}>
            <div className={clsx(styles.container)}>
                <div className="row">
                {tutorial.map((props, idx) => (
                 <Tutorial key={idx} {...props} />
                ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
                <div className="row">
                {footer.map((props, idx) => (
                 <Footer key={idx} {...props} />
                ))}
            </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default Audio;