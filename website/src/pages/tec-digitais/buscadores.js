import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const tutoriais = [
    {
        projeto: "buscadores - labri-unesp",
        title: "Operadores Booleanos",
        text: (
            <>
            <p className={clsx(styles.pSobre)}>Vídeo sobre o que são operadores booleanos e como usar nos sites de bases de dados acadêmicos e nos buscadores como Google, Bing, etc.</p>
            </>
        ),
        embed: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/X6qkLPLz4IE" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        )
    },
    {
        projeto: "buscadores - labri-unesp",
        title: "Procura no Google Acadêmico com Operadores Booleanos",
        text: (
            <>
            <p className={clsx(styles.pSobre)}> Exemplo de procura utilizando os operadores AND e OR.</p>
            </>
        ),
        embed: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/ASxJZK_m4Mk" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        )
    },
    {
        projeto: "buscadores - labri-unesp",
        title: "Pesquisa Programável Google",
        text: (
            <>
            <p className={clsx(styles.pSobre)}> Vídeo sobre o serviço do Google que permite configurar o buscador para procurar em sites escolhidos por você. Pode ser muito útil para o estudo e a pesquisa acadêmica.</p>
            </>
        ),
        embed: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/3SwwHiwCDlo" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        )
    },
    {
        projeto: "buscadores - labri-unesp",
        title: "Pesquisa em Bases de Dados Acadêmicas",
        text: (
            <>
            <p className={clsx(styles.pSobre)}>Vídeo sobre algumas das principais bases de dados de artigos científicos disponíveis na internet.</p>
            </>
        ),
        embed: (
            <>
            <iframe width="500" height="250" src="https://youtu.be/wjcYz3cctvI" frameborder="0" allowfullscreen autostart="false"></iframe>
            </>
        )
    }
]

const footer = [
    {
        projeto: "conhecimentos - labri-unesp",
        link: "/tec-digitais"
    }
]

function Footer({link}){
    return(
        <div className={clsx(styles.containerT)}>
            <h2 className={clsx(styles.highlightC)}><a href={link}>Voltar para a página principal</a></h2>
        </div>
    )
}

function Tutoriais({title, text, embed}){
    return(
        <div className={clsx(styles.container)}>
            <h2><mark>{title}</mark></h2>
            <div className="row">
                <div className="col col--6">
                    <p className={clsx(styles.pSobre)}>{text}</p>
                </div>
                <div className="col col--6">
                    <p className={clsx(styles.pSobre)}>{embed}</p>
                </div>
            </div>
        </div>
    )
}

function Buscadores(){
    return(
        <Layout title="Pesquisa em Buscadores">
            <header className={clsx(styles.heroBanner, "hero hero--primary")}>
                <div className={clsx(styles.container)}>
                    <div className={clsx(styles.hero__title)}>Pesquisa em Buscadores</div>
                </div>
            </header>
            <main>
        <section className={styles.content}>
            <div className={clsx(styles.container)}>
                <div className="row">
                {tutoriais.map((props, idx) => (
                 <Tutoriais key={idx} {...props} />
                ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
                <div className="row">
                {footer.map((props, idx) => (
                 <Footer key={idx} {...props} />
                ))}
            </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default Buscadores;