import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";


const sobre = [
    {
        projeto: "tec - labri-unesp",
        text: (
            <>
            <h2 className={clsx(styles.highlightB)}>Tutoriais: Tecnologias Digitais</h2>
            <p>Acesse também: <a href="https://sites.google.com/view/tics-estudo-pesquisa/in%C3%ADcio?authuser=0#h.45hkck3ggb48">Pesquisa em Bases Acadêmicas da área de Relações Internacionais</a></p>
            </>
        ),
        imgMenu: "/img/tec-digitais/exemplo.svg"
    }
]

const menu = [
    {
        projeto: "tec - labri-unesp",
        title: "Conhecimentos Básicos",
        link: "/tec-digitais/conhecimentos",
        imgFoto: "/img/tec-digitais/conhecimentos.svg"
    },
    {
        projeto: "tec - labri-unesp",
        title: "Tecnologias para Anotações",
        link: "/tec-digitais/anotacoes",
        imgFoto: "/img/tec-digitais/anotacoes.svg"
    },
    {
        projeto: "tec - labri-unesp",
        title: "Tecnologias para Edição de Textos",
        link: "/tec-digitais/edicao",
        imgFoto: "/img/tec-digitais/edicao.svg"
    },
    {
        projeto: "tec - labri-unesp",
        title: "Pesquisa em Buscadores",
        link: "/tec-digitais/buscadores",
        imgFoto: "/img/tec-digitais/buscadores2.svg"
    },
    {
        projeto: "tec - labri-unesp",
        title: "Audio",
        link: "/tec-digitais/audio",
        imgFoto: "/img/tec-digitais/audio.svg"
    },
]

function Menu({title, link, foto, imgFoto}){
    return(
        <div className={clsx(styles.Info, "card col col--6")}>
            <div className="card-content">
                <div className="card__header">
                    <div className={clsx(styles.Info, "text__center")}>
                        <img src={imgFoto} alt={foto} />
                    </div>
                </div>
                <div className="card__body">
                    <h3>{title}</h3>
                </div>
                <div className="card__footer">
                    <a className="button button--secondary" href={link}>Acesse Aqui</a>
                </div>
            </div>
        </div>
    )
}

function Sobre({menu, imgMenu, title, text}){
    return(
        <div>
            <div className={clsx(styles.containerM)}>
                <div className="row">
                    <div className="col col--4">
                        <img className={clsx(styles.Sobre)} src={imgMenu} alt={menu} />
                    </div>
                    <div className="col col--6">
                        <p className={clsx(styles.pSobre)}>{text}</p>
                    </div>
                    <div className="col col--10">
                        <h1 className={clsx(styles.highlight)}>MENU</h1>
                    </div>
                </div>
            </div>
        </div>
    )
}

function TecDigitais(){
    return(
        <Layout title="Tecnologias Digitais">
            <header className={clsx(styles.heroBanner, "hero hero--primary")}>
                <div className={clsx(styles.container)}>
                    <h1 className={clsx(styles.hero__title)}>
                        Tecnologias Digitais para o Estudo e a Pesquisa
                    </h1>
                </div>
            </header>
            <main>
        <section className={styles.content}>
            <div className={clsx(styles.container)}>
                <div className="row">
                {sobre.map((props, idx) => (
                 <Sobre key={idx} {...props} />
                ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
                <div className="row">
                {menu.map((props, idx) => (
                 <Menu key={idx} {...props} />
                ))}
            </div>
          </div>
          </section>
          </main>
        </Layout>
    )
}

export default TecDigitais