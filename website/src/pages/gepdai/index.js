import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const sobre = [
    {
        projeto: "gepdai - labri-unesp",
        title: "Sobre o GEPDAI",
        text: "O Grupo de Estudo em Política e Direito Ambiental Internacional (GEPDAI) se dedica a analisar e compreender os temas da Política e do Direito Ambiental em uma perspectiva das Relações Internacionais. O GEPDAI promove reuniões quinzenais que trazem as discussões de maneira horizontal entre os membros e docentes participantes resultando em reflexões e produções a respeito da dicotomia Sociedade - Meio Ambiente, tema de alta relevância no contexto global atual.",
        imgSobre: "/img/gepdai/sobre.svg"
    }
]

const atividades = [
    {
        projeto: "gepdai - labri-unesp",
        title: "Atividades",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam maximus ultricies dignissim. Aliquam auctor pulvinar urna, luctus imperdiet ante volutpat vitae. Sed pulvinar sapien sapien, vel feugiat nibh mattis congue. Nunc vel enim suscipit eros finibus semper. Maecenas hendrerit est eu urna efficitur, id imperdiet nisl dignissim. Proin malesuada vel lectus sed dapibus. Praesent tristique dolor et leo accumsan, at luctus sem consectetur.",
        imgAtiv: "/img/gepdai/outro.svg"
    }
]

const equipe = [
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do Membro",
        imgIcon: "/img/gepdai/icon.png",
        imgRede1: "/img/social/linkedin.svg",
        link1: "/gepdai",
        imgRede2: "/img/social/facebook.svg",
        link2: "/gepdai"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do Membro",
        imgIcon: "/img/gepdai/icon2.png",
        imgRede1: "/img/social/linkedin.svg",
        link1: "/gepdai",
        imgRede2: "/img/social/facebook.svg",
        link2: "/gepdai"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do Membro",
        imgIcon: "/img/gepdai/icon3.png",
        imgRede1: "/img/social/linkedin.svg",
        link1: "/gepdai",
        imgRede2: "/img/social/facebook.svg",
        link2: "/gepdai"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do Membro",
        imgIcon: "/img/gepdai/icon3.png",
        imgRede1: "/img/social/linkedin.svg",
        link1: "/gepdai",
        imgRede2: "/img/social/facebook.svg",
        link2: "/gepdai"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do Membro",
        imgIcon: "/img/gepdai/icon.png",
        imgRede1: "/img/social/linkedin.svg",
        link1: "/gepdai",
        imgRede2: "/img/social/facebook.svg",
        link2: "/gepdai"
    },
    {
        projeto: "gepdai - labri-unesp",
        nome: "Nome do Membro",
        imgIcon: "/img/gepdai/icon2.png",
        imgRede1: "/img/social/linkedin.svg",
        link1: "/gepdai",
        imgRede2: "/img/social/facebook.svg",
        link2: "/gepdai"
    },
]

const historia = [
    {
        projeto: "gepdai - labri-unesp",
        title: "História do GEPDAI",
        text: "O grupo surge em [ano de surgimento] com a proposta de [objetivo do grupo]. compreendendo que [contextualização que o  grupo se insere] com isso se busca [aspirações]. Através de uma visão crítica, o grupo faz uso de aparato teórico de livros e textos de autores da área, como também a análise documental a cerca de casos pontuais.",
        imgHist: "/img/gepdai/outro.svg"
    }
]

function Equipe({nome, icon, imgIcon, rede1, imgRede1, rede2, imgRede2, link1, link2}){
    return(
    <div className={clsx(styles.Info, "card col col--4")}>
    <div className="card-content">
        <div className="card__header">
            <div className={clsx(styles.Info, "text__center")}>
                <img src={imgIcon} alt={icon} />
            </div>
        </div>
        <div className="card__body">
            <h2 className={clsx(styles.h2Gepdai)}>{nome}</h2>
        </div>
        <div className={clsx(styles.Icons, "card__footer")}>
            <img src={imgRede1} alt={rede1} href={link1} />
            <img src={imgRede2} alt={rede2} href={link2} />
        </div>
    </div>
</div>
    )
}

function Historia({title, text, hist, imgHist}){
    return(
        <div>
            <div className={clsx(styles.containerS)}>
                <div className="row">
                    <div className="col col--4">
                        <img className={clsx(styles.Atividades)} src={imgHist} alt={hist} />
                    </div>
                    <div className="col col--6">
                        <h2 className={clsx(styles.h2Gepdai)}>{title}</h2>
                        <p className={clsx(styles.pSobre)}>{text}</p>                    </div>
                    <div className="col col--10">
                        <h3>Equipe</h3>
                    </div>
                </div>
            </div>
        </div>
    )
}

function Atividades({title, text, ativ, imgAtiv}){
    return(
        <div>
            <div className={clsx(styles.containerS)}>
                <div className="row">
                    <div className="col col--6">
                        <h2 className={clsx(styles.h2Gepdai)}>{title}</h2>
                        <p className={clsx(styles.pSobre)}>{text}</p>
                    </div>
                    <div className="col col--4">
                        <img className={clsx(styles.Atividades)} src={imgAtiv} alt={ativ} />
                    </div>
                </div>
            </div>
        </div>
    )
}

function Sobre({sobre, imgSobre, title, text}){
    return(
        <div>
            <div className={clsx(styles.containerS)}>
                <div className="row">
                    <div className="col col--4">
                        <img className={clsx(styles.Sobre)} src={imgSobre} alt={sobre} />
                    </div>
                    <div className="col col--6">
                        <h2 className={clsx(styles.h2Gepdai)}>{title}</h2>
                        <p className={clsx(styles.pSobre)}>{text}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

function Gepdai(){
    return(
        <Layout title="GEPDAI">
            <header className={clsx(styles.heroBanner, "hero hero--primary")}>
                <div className={clsx(styles.container)}>
                <h1 className={clsx(styles.hero__title)}>GEPDAI</h1>
                <h2 className={clsx(styles.hero__subtitle)}>Grupo de Estudos em Política e Direito Ambiental Internacional</h2>
                </div>
            </header>
            <main>
        <section className={styles.content}>
            <div className={clsx(styles.container)}>
                <div className="row">
                {sobre.map((props, idx) => (
                 <Sobre key={idx} {...props} />
                ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {atividades.map((props, idx) => (
                <Atividades key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {historia.map((props, idx) => (
                <Historia key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {equipe.map((props, idx) => (
                <Equipe key={idx} {...props} />
              ))}
            </div>
          </div>
            </section>
            </main>
        </Layout>
    )
}

export default Gepdai;