import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const tipos = [
    {
        projeto: "estagio labri-unesp",
        title: "Estágio Voluntário",
        imgFoto: "/img/estagio/estagio2.svg",
        link: "/estagio/estagio-voluntario"
    },
    {
        projeto: "estagio labri-unesp",
        title: "Estágio Remunerado",
        imgFoto: "/img/estagio/estagio1.svg",
        link: "/estagio/estagio-remunerado"
    }
]

const sobre = [
    {
        projeto: "estagio labri-unesp",
        title: "Sobre o LabRI",
        text: (
            <>
            <ul>O Laboratório de Relações Internacionais da Universidade Estadual Paulista (LabRI/UNESP), criado em 2011, faz parte da estrutura do departamento de Relações Internacionais (DERI) da UNESP, campus Franca. O principal objetivo que o DERI atribui ao LabRI/UNESP é fornecer apoio, por meio de tecnologias digitais, às atividades de ensino, pesquisa e extensão dos docentes, discentes e grupos de pesquisa vinculados ao curso de Relações Internacionais.</ul>
            <ul>O LabRI/UNESP não é um grupo de pesquisa. Pode-se considerá-lo um Hub acadêmico, pois fornece um espaço e infraestrutura física e virtual para que os docentes, discentes e grupos de pesquisa a ele vinculados possam desenvolver as suas atividades de pesquisa e ensino de extensão, especialmente se tais atividades demandam a utilização de tecnologias digitais e o suporte a partir de uma infraestrutura computacional. Para mais detalhes sobre o LabRI/UNESP, <a href="https://labriunesp.org/sobre">clique aqui</a></ul>
            </>
        )
    }
]

function Sobre({title, text}){
    return(
        <div>
            <h2>{title}</h2>
            <p className={clsx(styles.pSobre)}>{text}</p>
        </div>
    )
}

function Tipos({title, foto, imgFoto, link}){
    return(
        <div className={clsx(styles.Info, "card col col--6")}>
            <div className="card-content">
                <div className="card__header">
                    <div className={clsx(styles.Info, "text__center")}>
                        <img src={imgFoto} alt={foto} />
                    </div>
                </div>
                <div className="card__body">
                    <h3>{title}</h3>
                </div>
                <div className="card__footer">
                    <a className="button button--secondary" href={link}>Acesse Aqui</a>
                </div>
            </div>
        </div>
    )
}

function Estagio(){
    return(
        <Layout title="Oportunidade de Estágio">
        <header className={clsx("hero hero--primary", styles.heroBanner)}>
            <div className={clsx(styles.container)}>
                <h1 className={clsx(styles.hero__title)}>Oportunidades de Estágio</h1>
            </div>
        </header>
        <main>
        <section className={styles.content}>
        <div className={clsx(styles.container)}>
            <div className="row">
              {sobre.map((props, idx) => (
                <Sobre key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {tipos.map((props, idx) => (
                <Tipos key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
        </main>
        </Layout>
    )
}

export default Estagio