import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const sobre = [
    {
        projeto: "remunerado - labri-unesp",
        title: "Sobre o Estágio Remunerado",
        description: (
            <>
            Atualmente, o LabRI/UNESP possui apenas <b>uma (1) vaga de estágio remunerado</b>, porém as demandas e possibilidades de atuação no laboratório são uma boa oportunidade para que os estudantes possam desenvolver habilidades importantes para sua inserção profissional. Caso a chamada para o estágio remunerado não esteja aberta, acesse a página de <b><a href="https://labriunesp.org/estagio/estagio-voluntario">Estágio Voluntário</a></b> para mais informações de como atuar voluntariamente no LabRI.
            </>
        )
    }
]

const chamadas = [
    {
        projeto: "remunerado - labri-unesp",
        imgLogo: "/img/estagio/remunerado.svg",
        title: "Chamadas",
        text: "O LabRI não possui chamadas abertas atualmente."
    }
]

const infos = [
    {
        projeto: "remunerado - labri-unesp",
        title: "Quem pode participar?",
        text: "O estágio no LabRI/UNESP dá prioridade para os discentes vinculados ao Departamento de Relações Internacionais (graduandos e pós-graduandos), sendo assim apenas (por enquanto) estudantes do curso de Relações Internacionais da UNESP/Franca podem aplicar para este processo seletivo.",
        title2: "Pré-Requisitos",
        text2: (
            <>
            Consideramos que o estágio, por ser um início de inserção profissional de graduandos,
não deve exigir previamente conhecimentos especializados. Apesar disso, o estudante deve estar disposto a aprender aspectos importantes para o desenvolvimento das atividades e tarefas do LabRI/UNESP.
            <ul><li>Aprender fundamentos de programação (Python)</li></ul>
            <ul><li>Automatização e monitoramento de processos</li></ul>
            <ul><li>Documentar os processos desenvolvidos</li></ul>
            <ul><li>Acompanhar no acompanhamento e experimentação de novas tecnologias digitais</li></ul>
            </>
        ),
    },
]

function Chamadas({logo, imgLogo, title, text}){
    return(
        <div className={clsx(styles.container)}>
            <div className="row">
                <div className={clsx(styles.Info, "col col--6")}>
                    <img src={imgLogo} alt={logo} />
                </div>
                <div className={clsx(styles.Info, "col col--4")}>
                    <div className="card-content">
                        <h3>{title}</h3>
                    </div>
                    <div className="card__body">
                    <p className={clsx(styles.pChamadas)}>{text}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

function Infos({title, text, title2, text2}){
    return(
        <div className={clsx(styles.SobreI)}>
            <div className="row">
            <div className="col col--6">
                <div className="card-content">
                    <h3>{title}</h3>
                </div>
                <div className="card__body">
                    <p className={clsx(styles.pParticipar)}>{text}</p>
                </div>
            </div>
            <div className="col col--6">
                <div className="card-content">
                    <h3>{title2}</h3>
                </div>
                <div className="card__body">
                    <p className={clsx(styles.pParticipar)}>{text2}</p>
                </div>
            </div>
            </div>
        </div>
    )
}

function Sobre({title, description}){
    return(
        <div className={clsx(styles.Participar)}> 
            <h2>{title}</h2>
            <p className={clsx(styles.pSobre)}>{description}</p>
        </div>
    )
}

function Remunerado(){
    return(
        <Layout title="Estágio Remunerado">
        <header className={clsx("hero hero--primary", styles.heroBanner)}>
            <div className={clsx(styles.container)}>
                <h1 className={clsx(styles.hero__title)}>Estágio Remunerado</h1>
            </div>
        </header>
        <main>
            <section className={clsx(styles.content)}>
                <div className={clsx(styles.container)}>
                    <div className="row">
                        {sobre.map((props, idx) => (
                        <Sobre key={idx} {...props} />
                        ))}                        
                    </div>
                </div>

                <div className={clsx(styles.container)}>
                    <div className="row">
                        {chamadas.map((props, idx) => (
                        <Chamadas key={idx} {...props} />
                        ))}                        
                    </div>
                </div>

                <div className={clsx(styles.container)}>
                    <div className="row">
                        {infos.map((props, idx) => (
                        <Infos key={idx} {...props} />
                        ))}                        
                    </div>
                </div>
            </section>
        </main>
        </Layout>
    )
}

export default Remunerado;