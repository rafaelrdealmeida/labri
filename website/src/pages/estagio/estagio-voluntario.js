import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const tipos = [
    {
        projeto: "voluntario labri-unesp",
        title: (
            <>
            Modalidade de Estágio Voluntário 01: <u>Atividades Gerais</u>
            </>
            ),
        imgLogo: "/img/estagio/modulo1.svg",
        text: (
            <>
            Nesta modalidade de estágio o estudante, ao invés de auxiliar o desenvolvimento de algum projeto específico, irá ajudar nas atividades gerais do LabRI. Dentre estas atividades giram em torno dos oitos pontos indicados na “Apresentação Geral”
            <li><b>Carga horária:</b> 12 horas semanais de atividades remotas ou presenciais na sala do LabRI</li>
            <li>A atribuição das tarefas do estágio e o gerenciamento das atividades será realizado através no gerenciador de projetos/tarefas do LabRI (RedMine)</li>
            <li>Caso o estudante participe do próximo processo seletivo do estágio remunerado terá um bônus de 25% na nota final.</li>
            </>
        )
    },
    {
        projeto: "voluntario labri-unesp",
        title: (
            <>
            Modalidade de Estágio Voluntário 02: <u>Projetos Específicos</u>
            </>
            ),
        imgLogo: "/img/estagio/estagio2.svg",
        text: (
            <>
            Nesta modalidade de estágio o estudante irá auxiliar o desenvolvimento de algum dos projetos indicados nesta chamada (ver abaixo).
            <li><b>Carga horária:</b> 6 horas semanais de atividades remotas ou presenciais na sala do LabRI</li>
            <li>Plano de Atividades (RedMine)</li>
            <li>Caso o estudante participe do próximo processo seletivo do estágio remunerado terá um bônus de 15% na nota final.</li>
            </>
        )
    },
    {
        projeto: "voluntario labri-unesp",
        title: (
            <>
            Modalidade de Estágio Voluntário 03: <u>Submissão de Trabalho</u>
            </>
        ),
        imgLogo: "/img/estagio/modulo3.svg",
        text: (
            <>
            Nesta modalidade o estudante deverá apresentar um projeto significativo tanto para si quanto para o LabRI, de maneira a executar o plano proposto a partir do uso do espaço físico e dos equipamentos do laboratório.
            <li><b>Carga horária:</b> 6 horas semanais de atividades remotas ou presenciais na sala do LabRI</li>
            <li>Plano de Atividades, contendo descrições sobre:</li>
            <ul><ul><li>Qual o objetivo do projeto;</li></ul></ul>
            <ul><ul><li>Qual a justificativa geral do projeto;</li></ul></ul>
            <ul><ul><li>Porque o projeto precisa/deve ser realizado pelo LabRI;</li></ul></ul>
            <ul><ul><li>Como o projeto prepara o estudante para o ambiente de trabalho;</li></ul></ul>
            <ul><ul><li>Quais serão as atividades envolvidas;</li></ul></ul>
            <ul><ul><li>Quais serão as tecnologias envolvidas;</li></ul></ul>
            <ul><ul><li>Qual o tempo necessário para a execução do projeto;</li></ul></ul>
            <ul><ul><li>Quais os resultados esperados.</li></ul></ul>
            </>
        )
    }
]

const sobre = [
    {
        projeto: "voluntario labri-unesp",
        title: "Sobre o Estágio Voluntário",
        description: (
            <>
            <li>Auxílio nas atividades gerais do LabRI/UNESP (descrever as atividades envolvidas)</li>
            <li>Caso a pessoa escolha desenvolver atividades gerais, significa que não atuará em um projeto específico, mas sim ajudará nas tarefas cotidianas do LabRI/UNESP. Tais tarefas se relacionam a:</li>
            <ul><ul><li>Auxílio aos projetos de dados, ensino, extensão e sistema em andamento;</li></ul></ul>
            <ul><ul><li>Manutenção e expansão das bases de dados existentes;</li></ul></ul>
            <ul><ul><li>Manutenção dos sites e das redes sociais;</li></ul></ul>
            <ul><ul><li>Apoio às demandas cotidianas do LabRI/UNESP </li></ul></ul>
            <ul><ul><li>Auxílio em projetos específicos do LabRI/UNESP (descrever as atividades envolvidas)</li></ul></ul>
            <li>A atuação no estágio voluntário (ao menos 90 dias) representará <b>20% de pontuação</b> no próximo processo seletivo de estágio remunerado</li>
            </>
        )
    }
]

const participar = [
    {
        projeto: "voluntario - labri-unesp",
        title1: "Quem pode participar?",
        description1: "O estágio no LabRI/UNESP dá prioridade para os discentes vinculados ao Departamento de Relações Internacionais (graduandos e pós-graduandos), sendo assim apenas (por enquanto) estudantes do curso de Relações Internacionais da UNESP/Franca podem aplicar para este processo seletivo.",
        title2: "Processo Seletivo",
        description2: (
            <>
            <ul><li>Em desenvolvimento.</li></ul>
            </>
        )
    },
]

const participar2 = [
    {
        projeto: "voluntario - labri-unesp",
        title1: "Pré-Requisitos",
        description1: (
            <>
            Consideramos que o estágio, por ser um início de inserção profissional de graduandos,
não deve exigir previamente conhecimentos especializados. Apesar disso, o estudante deve estar disposto a aprender aspectos importantes para o desenvolvimento das atividades e tarefas do LabRI/UNESP.
            <ul><li>Aprender fundamentos de programação (Python)</li></ul>
            <ul><li>Automatização e monitoramento de processos</li></ul>
            <ul><li>Documentar os processos desenvolvidos</li></ul>
            <ul><li>Acompanhar no acompanhamento e experimentação de novas tecnologias digitais</li></ul>
            </>
        ),
        title2: "Tempo mínimo de duração",
        description2: "O candidato deve levar em conta o tempo de atuação no ambiente do LabRI/UNESP. A duração do período de estágio é de no mínimo 3(três) meses, sendo que é possível estagiar até 24 meses. A carga horária é flexível, partindo de 10(dez) horas semanais, até 20(vinte) horas semanais, havendo também um meio-termo de 15(quinze) horas."
    }
]

function Participar2({title1, description1, title2, description2}){
    return(
        <div className={clsx(styles.Participar)}>
            <div className="row">
                <div className="col col--6">
                    <h3>{title1}</h3>
                    <p className={clsx(styles.pParticipar)}>{description1}</p>
                </div>
                <div className="col col--6">
                    <h3>{title2}</h3>
                    <p className={clsx(styles.pParticipar)}>{description2}</p>
                </div>
            </div>
        </div>
    )
}

function Participar({title1, description1, title2, description2}){
    return(
        <div className={clsx(styles.Sobre)}>
            <h2>PARTICIPAR DO ESTÁGIO VOLUNTÁRIO</h2>
            <div className="row">
                <div className="col col--6">
                    <h3>{title1}</h3>
                    <p className={clsx(styles.pParticipar)}>{description1}</p>
                </div>
                <div className="col col--6">
                    <h3>{title2}</h3>
                    <p className={clsx(styles.pParticipar)}>{description2}</p>
                </div>
            </div>
        </div>
    )
}

function Sobre({title, description}){
    return(
        <div className={clsx(styles.Sobre)}> 
            <h2>{title}</h2>
            <p className={clsx(styles.pSobre)}>{description}</p>
        </div>
    )
}

function Tipos({ title, subtitle, text, logo, imgLogo }) {
  return (
    <div className={clsx(styles.Info, "card col col--12")}>
      <div className="card-content">
        <h2>{title}</h2>
      </div>
      <div className="card__body">
        <div className={clsx(styles.Info, "text__center")}>
          <img src={imgLogo} alt={logo} />
        </div>
      </div>
      <div className="card__footer">
          <p className={clsx(styles.pSobre)}>{text}</p>
      </div>
    </div>
  );
}

function Voluntario() {
  return (
    <Layout title="Estágio Voluntário">
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className={clsx(styles.container)}>
          <h1 className={clsx(styles.hero__title)}>Estágio Voluntário</h1>
        </div>
      </header>
      <main>
        <section className={styles.content}>
        <div className={clsx(styles.container)}>
            <div className="row">
              {sobre.map((props, idx) => (
                <Sobre key={idx} {...props} />
              ))}
            </div>
          </div>

        <div className={clsx(styles.container)}>
            <div className="row">
              {tipos.map((props, idx) => (
                <Tipos key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.containerP)}>
            <div className="row">
              {participar.map((props, idx) => (
                <Participar key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.containerP)}>
            <div className="row">
              {participar2.map((props, idx) => (
                <Participar2 key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
        </main>
    </Layout>
  );
}

export default Voluntario;
