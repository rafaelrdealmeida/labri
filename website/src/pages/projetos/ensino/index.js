import React from 'react';
import Layout from '@theme/Layout';
import styles from './styles.module.css';
import stylesEnsino from './styles.ensino.module.css';
import clsx from 'clsx';
import useBaseUrl from '@docusaurus/useBaseUrl';


const projetos = [
  {
    title: 'Trilha de Dados',
    imageUrl: './img/projetos/ensino/trilha-dados/trilha-dados.svg',
    description: (
      <>
         O projeto Trilha de Dados visa reunir informações e material didático  sobre questões relativas à ciência de dados nas 
         Relações Internacionais. Assuntos como coleta e análise de dados; fundamentos de algoritmos e programação; entre outros 
         estão entre os tópicos que serão abordados aqui.

      </>
    ),
    link: '/docs/projetos/ensino/trilha-dados/intro',
    tipo: 'educacao',
  },
  {
    title: 'Tecnologias Digitais',
    imageUrl: '/img/projetos/ensino/tec-digitais/logo-tec-digitais.svg',
    description: (
      <>
      Nesse espaço você encontrará tutoriais entre outros materiais didáticos que tem por objetivo disseminar a utilização de tecnologias digitais.

      </>
    ),
    link: '/docs/projetos/ensino/tec-digitais/intro',
    tipo: 'educacao',
  },
  {
    title: 'Processar imagens que contém texto',
    imageUrl: './img/projetos/ensino/ensino/arquivos.svg',
    description: (
      <>
        Você tem um arquivo, tirou foto ou digitalizou um texto, mas não conhece buscar palavras chaves nele?
        Este arquivo é uma imagem (png, jpeg ou tiff) ou um pdf?
        Neste tutorial indicamos como processar imagens textuais para gerar um pdf pesquisável e, deste modo, conseguir buscar as palavras chaves contidas dentro deste arquivo.
      </>
    ),
    link: '/projetos/ensino/pos-processamento',
    tipo: 'educacao',
  },
  {
    title: 'Acesso Remoto',
    imageUrl: './img/projetos/ensino/ensino/acesso-remoto.svg',
    description: (
      <>
        Aqui você encontrará informações para acesso a estação remoto de trabalho
      </>
    ),
    link: '/projetos/ensino/acesso-remoto',
    tipo: 'educacao',
  },
  {
    title: 'Recoll',
    imageUrl: './img/projetos/ensino/ensino/recoll.svg',
    description: (
      <>
        Aqui você encontrará como utilizar os recursos do aplicativo Recoll.
      </>
    ),
    link: '/projetos/ensino/recoll',
    tipo: 'educacao',
  },
  {
    title: 'Filezilla',
    imageUrl: './img/projetos/ensino/ensino/filezilla.svg',
    description: (
      <>
        Aqui você aprenderá como fazer a transferência de arquivos entre dois computadores utilizando o Filezilla.
      </>
    ),
    link: '/projetos/ensino/filezilla',
    tipo: 'educacao',
  },
  {
    title: 'Material Bibliográfico',
    imageUrl: './img/projetos/ensino/ensino/biblio-acad.svg',
    description: (
      <>
        Aqui você encontrará indicações de material bibliográfico relacionado às Relações Internacionais e/ou tecnologias digitais.
      </>
    ),
    link: '/docs/projetos/ensino/material-bibliografico/intro',
    tipo: 'educacao',
  },

];

function Projeto({imageUrl, title, description, pessoas, instituicoesEnvolvidas, financiamento, link, objetivo}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>

        <h3>{title}</h3>
        <p> <b></b> {description}</p>
        <a className="button button--secondary" href={link}>Saiba Mais</a>

      </div>

    </div>
  );
}

function Projetos_ensino() {
  return (
    <Layout title="Projeto De Ensino">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className="hero__title">Projetos</h1>
          <h2 className="hero__subtitle">Conheça e Contribua com os projetos do LabRI</h2>
        </div>
      </header>
      <div className={clsx(styles.apresentacao, styles.container)}>
        <h2> Apresentação </h2>
        <p>
          Abaixo estarão expostos os projetos nos quais o LabRI participa, assim como informações essenciais sobre esses projeto.
        </p>
        <p>Os projetos do LabRI são divididos conforme seus objetivos. são eles: </p>
        <div className="row tipos_projeto">
          <div className="col col--4">
            <h3><a href="/projetos/dados/">Projetos de Dados</a></h3>
          </div>
          <div className="col col--4">
            <h3><a href="/projetos/sistemas/">Projetos de Sistemas</a></h3>
          </div>
          <div className="col col--4">
            <h3><a href="/projetos/extensao/">Projetos de Extensão</a></h3>
          </div>
        </div>
      </div>

      <main className={styles.main}>
      <section className={styles.content}>
        {projetos && projetos.length > 0 && (
          <section className={styles.projetos}>
            <div className="container">
              {projetos.map((props, idx) => (
                <Projeto key={idx} {...props} />
              ))}
            </div>
          </section>
        )}
      </section>
    </main>
    </Layout>
  );
}

export default Projetos_ensino;
