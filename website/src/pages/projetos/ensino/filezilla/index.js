import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesFilezilla from "./styles.filezilla.module.css";
import styles from "../styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";

const intro = [
  {
    projeto: "filezilla - labri-unesp",
    title: "Filezilla: Transferência de Arquivos",
  },
];

const tutorial = [
  {
    projeto: "filezilla - labri-unesp",
    title: "O que é Filezilla?",
    text: "É um aplicativo de código aberto disponível para MAC OS, Windows e Linux. Sua função é realizar a transferência de arquivos entre dois computadores via acesso remoto.",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/intro.png",
  },
];

const info = [
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 1",
    subtitulo: "Abrir o aplicativo",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo1.png",
    description:
      "Abra o Menu de aplicações do computador e vá em Internet e procure por pelo aplicativo FileZilla",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 2",
    subtitulo: "Abrir o aplicativo",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo2.png",
    description:
      "Clique no ícone no canto superior esquerda da tela, logo abaixo de Arquivo.",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 3",
    subtitulo: "Novo site",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo3.png",
    description: "Após a janela abrir clique em Novo Site",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 4",
    subtitulo: "Renomear",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo4.png",
    description:
      "Para nomear o atalho Novo Site, clique em Renomear e coloque um nome de sua preferência para este acesso sFTP.",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 5",
    subtitulo: "Chave de Host",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo6.png",
    description:
      "Quando aparecer uma mensagem de Chave Host Desconhecida,  selecione a caixa de confiar nesse host e depois clique em OK",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 6",
    subtitulo: "Após o login",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo7.png",
    description:
      "a disposição do Filezilla é a seguinte: 1) Lado esquerdo: é o computador o qual você realmente está. 2) Lado direito: é o computador o qual você entrou remotamente. ",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 7",
    subtitulo: "Navegação",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo8.png",
    description:
      "A navegação pelas pastas se dá de duas maneiras, isto para ambos os lados: 1) Selecionar um Endereço de uma pasta e colar no Endereço local; 2) Navegar pelas pastas como mostra o próprio Filezilla",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 8",
    subtitulo: "Transferência",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo9.png",
    description:
      "Para transferir um arquivo ou uma pasta entre primeiramente no local onde se encontra o arquivo ou pasta, em uma das máquinas. Depois abra o local para onde o arquivo será transferido na outra máquina. Volte ao local onde se encontra o arquivo a ser transferido, selecione, segure-o e arraste para a outra máquina",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 9",
    subtitulo: "Verificação",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo10.png",
    description:
      "A transferência bem sucedida deve se parecer com a imagem acima",
  },
  {
    projeto: "filezilla - labri-unesp",
    title: "PASSO 10",
    subtitulo: "Desconectar",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/passo11.png",
    description:
      "Para desconectar da máquina, clique no ícone como mostrado na imagem acima",
  },
];

const final = [
  {
    projeto: "filezilla - labri-unesp",
    title: "Configurar Geral",
    imgLogo: "/img/projetos/ensino/ensino/filezilla/final.png",
  },
];

function Intro({ title, projeto }) {
  return (
    <div className={clsx("hero hero--primary", stylesFilezilla.heroBannerPod)}>
      <div className={stylesFilezilla.Intro}>
        <h1>{title}</h1>
      </div>
    </div>
  );
}

function Tutorial({ title, text, logo, imgLogo, imageUrl }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div>
      <div className={clsx(stylesFilezilla.Tutorial)}>
        <div className={clsx(stylesFilezilla.textIntro)}>
          Caso você tenha dúvidas ou dificuldades para seguir esse{" "}
          <i>tutorial</i> envie um e-mail para:{" "}
          <u>
            <i>unesplabri@gmail.com</i>
          </u>
          Caso não consigamos resolver as dificuldades por e-mail podemos marcar
          uma videoconferência para realizar a configuração em conjunto com
          você.
        </div>
        <h2>{title}</h2>
        <div className="card__body">
          <img src={imgLogo} alt={logo} />
          <h4>{text}</h4>
        </div>
        <div className={clsx(stylesFilezilla.textIntro)}>
          <h2>PASSO A PASSO</h2>
        </div>
      </div>
    </div>
  );
}

function Info({ title, description, logo, imgLogo, imageUrl, subtitulo }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.Info, "card col col--6")}>
      <div className="card-content">
        <div className="card__header">
          <h2>{title}</h2>
          <h4>
            <i>{subtitulo}</i>
          </h4>
        </div>
        <div className={clsx(stylesFilezilla.Info, "text__center")}>
          <img src={imgLogo} alt={logo} />
        </div>
        <div className="card__body">
          <h6>{description}</h6>
        </div>
      </div>
    </div>
  );
}

function Final({ title, logo, imgLogo, imageUrl }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div>
      <div className={clsx(stylesFilezilla.Final)}>
        <div className={clsx(stylesFilezilla.textFinal)}>
          <h2>{title}</h2>
          <h6>
            <p>1 - Host: digite o endereço da máquina que será acessada</p>
            <p>
              2 - Porta: digite o número de acesso à máquina (normalmente é a
              porta 22)
            </p>
            <p>
              3 - Protocolo: Selecione a opção SFTP - SSH File Transfer Protocol
            </p>
            <p>4 - Tipo de logon: selecione a opção:</p>
            <li>
              Normal: para acesso de modo automático, armazena o login de
              usuário e a senha;
            </li>
            <li>
              Pedir a senha: grava o login de usuário, porém precisa digitar a
              senha a todo acesso;
            </li>
            <li>
              Interativo: grava o login de usuário, porém precisa digitar a
              senha a todo acesso (recomendado);
            </li>
            <p>
              5 - Usuário: digite o seu usuário de login da máquina a ser
              acessada
            </p>
            <p>
              6 - Senha: digite a senha do seu usuário de login da máquina a ser
              acessada
            </p>
            <p>
              7 - Clique em Conectar para acessar imediatamente a máquina
              remotamente ou clique em OK para acessar posteriormente o Novo
              Site
            </p>
          </h6>
        </div>
        <div className={clsx(stylesFilezilla.Info)}>
          <img src={imgLogo} alt={logo} />
          <h2>Pronto!</h2>
        </div>
      </div>
    </div>
  );
}

function Filezilla() {
  return (
    <Layout title="Filezilla: transferência de arquivos">
      <header>
        <div>
          {intro.map((props, idx) => (
            <Intro key={idx} {...props} />
          ))}
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(styles.container)}>
            <div className="row">
              {tutorial.map((props, idx) => (
                <Tutorial key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {info.map((props, idx) => (
                <Info key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {final.map((props, idx) => (
                <Final key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Filezilla;
