import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import stylesProcesso from "./styles.processo.module.css";
import styles from "../styles.module.css";
import useBaseUrl from "@docusaurus/useBaseUrl";


const intro = [
    {
        projeto: "processo - labri-unesp",
        title: "Processar Imagens que contém Texto"
    },
];

const sobre = [
    {
        projeto: "processo - labri-unesp",
        imgSobre: "/img/projetos/ensino/ensino/arquivos.svg"
    },
];

const passos = [
    {
        projeto: "processo - labri-unesp",
        title: "PASSO 1",
        subtitulo: "Identificar formato do arquivo",
        imgIcon1: "/img/projetos/ensino/ensino/arquivos.svg",
        text1: (
            <>
            <li><b>Arquivos no formato textual (doc, docx, odt, epub)</b> são nativamente passíveis de busca por palavra chaves (basta você abrir o arquivo, aperta Ctrl+F e digitar a palavra chave que desejar) e indexáveis em software como o Recoll. Sendo assim, não são o foco deste tutorial.</li>
            <li>Em <b>Arquivos no formato de imagem (png, jpeg, tiff entre outros)</b> não é possivel nativamente buscar por palavra chaves e/ou indexar em software como o Recoll. Para saber mais sobre formatos de imagem <a href="https://archive.is/82iiV">clique aqui</a>.</li>
            <li><b>Arquivos no formato PDF</b> dependem da fonte de sua origem para ser ou não nativamente passiveis de busca por palavra chaves e indexáveis em software como o Recoll.</li>
            <ul>
                <li>Se o PDF foi gerado a partir de algum arquivo no formato textual, normalmente, será nativamente passível realizar busca por palavras chaves. Este tipo de PDF é conhecido como "PDF pesquisável"</li>
                <li>Se o PDF foi gerado a partir de algum arquivo no formato de imagem, normalmente não será nativamente passível realizar busca por palavras chaves. Este tipo de PDF é conhecido como "PDF não pesquisável"</li>
            </ul>
            <li>Assim, nosso foco recai sobre os <b>arquivos no formato de imagem e nos PDF não pesquisáveis</b>, pois são estes tipos de arquivo em que não conseguimos nativamente buscar por palavra chaves e/ou indexar em software como o Recoll.</li>
            </>
        ),
        title2: "Qual o formato de arquivo?",
        subtitulo: "Identificar formato",
        imgIcon2: "/img/projetos/ensino/ensino/pos-processamento/passo2.svg",
        text2: (
            <>
            <li>Se o arquivo for um <b>PDF não pesquisável</b> é necessário transforma-lo em algum formato de imagem. De preferência para transforma-lo em TIFF, pois é o formato mais utilizado para este tipo de processamento.</li>
            <li>Se o arquivo já se encontra em <b>formato de imagem</b> siga pra o próximo passo.</li>
            <ul><u><b>ATENÇÃO</b></u>: ao digitalizar páginas de documentos, livros e afins busque definir uma resolução ao menos média (equivalente a 300 dpi) para ter um melhor resultado final ao processar as imagens.</ul>
            <li>Se o arquivo estiver no formato de PDF é necessário transformá-lo em TIFF. Para isso basta seguir o <a href="https://www.youtube.com/watch?v=wmcrUGzGvkQ">vídeo tutorial</a>. Caso o arquivo já se encontre no formato TIFF é seguir para o segundo passo. </li>
            </>
        ),
    },
    {
        projeto: "processo - labri-unesp",
        title: "PASSO 2",
        subtitulo: "Processar Arquivos",
        imgIcon1: "/img/projetos/ensino/ensino/pos-processamento/passo3.svg",
        text1: (
            <>
            <li>Com os arquivos em formato de imagem agora vamos utilizar o software ScanTailor para otimizar as imagens. Este software irá te auxiliar a fixar a orientação, cortar e rotacionar as páginas; selecionar o conteúdo, definir as margens e DPI entre outras otimizações</li>
            <li>Recomendamos definir o DPI em no mínimo 300 DPI. Para saber mais sobre DPI <a href="https://archive.is/ElT6P">clique aqui.</a></li>
            <li>Para mais instruções sofre a utilização do ScanTailor assista o <a href="https://www.youtube.com/watch?v=2a9f9QzasBQ">vídeo tutorial</a>.</li>
            </>
        ),
        title2: "PASSO 3",
        subtitulo2: "Realizar OCR",
        imgIcon2: "/img/projetos/ensino/ensino/pos-processamento/passo4.svg",
        text2: (
            <>
            <li>Com os arquivos tratados e otimizados agora vamos realizar o Reconhecimento Óptico de Caracteres (OCR). Basicamente, o OCR é responsável por identificar os caracteres presentes na imagem e por tornar estes caracteres passiveis de busca por palavras chaves. Para saber mais sobre OCR <a href="https://archive.is/MHccn">clique aqui</a>.</li>
            <li>O <a href="https://tesseract-ocr.github.io/">Tesseract</a> é o software de código aberto mais popular para a realização de OCR. No LabRI/UNESP utilizamos um programa escrito em python chamado  <a href="https://ocrmypdf.readthedocs.io/en/latest/introduction.html">OCRmyPDF</a> para fazer este trabalho. Ele utiliza o Tesseract trazendo algumas facilidade de uso e funcionalidades adicionais.</li>
            <li>O OCRmyPDF vai pegar o arquivos de imagem ou o PDF não pesquisável e realizar o OCR. Quando o processo for finalizado será gerado um PDF pesquisável. Após isso siga para o próximo  passo.</li>
            <li>Se você esta utilizando a estrutura do LabRI/UNESP basta deixar o arquivo na pasta indicada que o OCR será realizado automaticamente.</li>
            <p>Para que o arquivo processado se torne um PDF pesquisável é necessário fazer o Reconhecimento Óptico de Caracteres (OCR) do documento. Basta seguir as instruções do <a href="https://www.youtube.com/watch?v=wmcrUGzGvkQ">vídeo tutorial</a></p>
            </>
        )
    },
    {
        projeto: "processos - labri-unesp",
        title: "PASSO 4",
        subtitulo: "Colocar bookmarks",
        imgIcon1: "/img/projetos/ensino/ensino/pos-processamento/passo5.svg",
        text1: (
            <>
            <li>Se o arquivo que esta sendo processado é um arquivo com muitas páginas e partes (por exemplo, livros, documentos e afins) e será indexado por programas como o Recoll é importante separar este arquivo em partes menores (por exemplo, separar um livro por capítulos)</li>
            <li>Esse processo é importante para que o programa que indexa os arquivos consiga estabelecer mais adequadamente a relevância, de acordo com seu algoritmo, dos arquivos que serão retornados quando é realizado uma busca por palavras chaves.</li>
            <li>A inclusão de marcadores no inicio de cada capitulo, por exemplo, viabiliza este separação.</li>
            <li>Para mais instruções sofre a inclusão dos marcadores basta assistir o <a href="https://www.youtube.com/watch?v=VQ3UQY4qOd0">vídeo tutorial</a>. </li>
            </>
        ),
        title2: "PASSO 5",
        subtitulo: "Cortar o Arquivo",
        imgIcon2: "/img/projetos/ensino/ensino/pos-processamento/passo6.svg",
        text2: (
            <>
            <li>Nós termos fizemos um script em python para realizar o processo de divisão do arquivo a partir dos marcadores presentes no mesmo.</li>
            <li>Também é possivel utilizar programas como o PDFsam para realizar este passo.</li>
            <ul>Para poder cortar o arquivo em capítulos será necessário utilizar o aplicativo <i>PDFsam</i>. Para entender melhor siga as instruções do <a href="https://www.youtube.com/watch?v=WvsRdDKj6_Y">vídeo tutorial.</a></ul>
            </>
        )
    },
    {
        projeto: "processos - labri-unesp",
        title: "PASSO 6",
        subtitulo: "Renomeação",
        imgIcon1: "/img/projetos/ensino/ensino/pos-processamento/passo7.svg",
        text1: (
            <>
            <ul>O último passo é realizar a nomeação dos arquivos em lote. Para isso será utilizado o krename e para entender basta seguir o vídeo tutorial. Além disso, é necessário colocar o nome no Padrão de nomeação.</ul>
            </>
        ),
        title2: "",
        imgIcon2: "/img/projetos/ensino/ensino/arquivos.svg",
        text2: (
            <>
            <h4><ul>Caso você tenha dúvidas ou dificuldades de configurar seu computador ou notebook envie um email para: <u>unesplabri@gmail.com</u>.</ul>
            <ul>Caso não consigamos resolver as dificuldades por email podemos marcar uma videoconferência para realizada a configuração em conjunto com você. </ul></h4>
            </>
        )
    }
]

const final = [
    {
        projeto: "processo - labri-unesp",
        title: "PRONTO!",
        imgLogo: "/img/projetos/ensino/ensino/pos-processamento/final.svg" 
    }
]

function Intro({title}){
    return(
        <div className={clsx("hero hero--primary", stylesProcesso.heroBannerPod)}>
          <div className={stylesProcesso.Intro}>
            <h1>{title}</h1>
          </div>
        </div>
    )
}

function Sobre({sobre, imgSobre, imageUrl}){
    const imagemInfo = useBaseUrl(imageUrl);
    return(
        <div>
            <div className="container">
                <div className="row">
                    <div className={clsx(stylesProcesso.Sobre, "col col--6")}>
                        <img src={imgSobre} alt={sobre} />
                    </div>
                    <div className={clsx(stylesProcesso.pSobre, "col col--6")}>
                        <h4><li>Você tem um arquivo, tirou foto ou digitalizou um texto, mas não conhece buscar palavras chaves nele?</li>
                        <li>Este arquivo é uma imagem (png, jpeg ou tiff) ou um pdf?</li>
                        <li>Neste tutorial indicamos como processar imagens textuais para gerar um pdf pesquisável e, deste modo, conseguir buscar as palavras chaves contidas dentro deste arquivo.</li></h4>
                    </div>
                    <div className={clsx(stylesProcesso.Tutorial)}>
                        <div className={clsx(stylesProcesso.textIntro)}>
                            <h3>O foco deste tutorial é apontar como fazer o processamento de imagens textuais. O objetivo final deste processamento é:</h3>
                            <h4><li>tornar possivel a busca por palavras chaves no arquivo processado;</li></h4>
                            <h4><li>viabilizar a indexação integral deste arquivo através de softwares como o Recoll Desktop Full-Text Search</li></h4>
                        </div>
                     </div>
                </div>
            </div>
        </div>
    )
}

function Passos({title, title2, subtitulo, subtitulo2, icon1, icon2, imgIcon1, imgIcon2, imageUrl, text1, text2}){
    const imagemInfo = useBaseUrl(imageUrl);
    return(
        <div>
            <div className="container">
                <div className="row">
                    <div className={clsx(stylesProcesso.pPassos, "col col--6")}>
                        <h2>{title}</h2>
                        <h4>{subtitulo}</h4>
                        <p>{text1}</p>
                    </div>
                    <div className={clsx(stylesProcesso.pPassos, "col col--6")}>
                        <img src={imgIcon1} alt={icon1} />
                    </div>
                    <div className={clsx(stylesProcesso.pPassos2, "col col--6")}>
                        <img src={imgIcon2} alt={icon2} />
                    </div>
                    <div className={clsx(stylesProcesso.pPassos2, "col col--6")}>
                        <h2>{title2}</h2>
                        <h4>{subtitulo2}</h4>
                        <p>{text2}</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

function Final({title, logo, imgLogo, imageUrl}){
    const imagemInfo = useBaseUrl(imageUrl);
    return (
        <div>
            <div className={clsx(stylesProcesso.Info)}>
                <img src={imgLogo} alt={logo} />
            </div>
            <div className={clsx(stylesProcesso.textInfo)}>
                <h2>{title}</h2>
            </div>
        </div>
    )
}

function Processo(){
    return(
        <Layout title="Processar Imagens  que contém Texto">
            <header>
                <div>
                    {intro.map((props, idx) => (
                        <Intro key={idx} {...props} />
                    ))
                    }
                </div>
            </header>
            <main>
                <section className={styles.content}>
                    <div className={clsx(styles.container)}>
                        <div className="row">
                            {sobre.map((props, idx) => (
                                <Sobre key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className="row">
                            {passos.map((props, idx) => (
                                <Passos key={idx} {...props} />
                            ))}
                        </div>
                    </div>

                    <div className={clsx(styles.container)}>
                        <div className="row">
                            {final.map((props, idx) => (
                                <Final key={idx} {...props} />
                            ))}
                        </div>
                    </div>
                </section>
            </main>
        </Layout>
    )
}

export default Processo; 