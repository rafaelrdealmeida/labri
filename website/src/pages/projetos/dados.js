import React from 'react';
import Layout from '@theme/Layout';
import styles from '../styles.module.css';
import clsx from 'clsx';
import useBaseUrl from '@docusaurus/useBaseUrl';


const projetos = [
  {
    title: 'Hemeroteca de Política Externa Brasileira',
    
    imageUrl: './img/labriunesp-02.svg',
    description: (
      <>
      A Hemeroteca de Política Externa Brasileira contém uma seleção de matérias publicadas, no período de 1972 a 2010, por alguns jornais brasileiros dentre os quais se destacam: O Estado de S. Paulo, Folha de S. Paulo e Gazeta Mercantil. O objetivo desta Hemeroteca é permitir aos pesquisadores interessados o acesso a notícias que foram selecionadas e classificadas ao longo dos anos, de 1972 até 2010, sobre importantes acontecimentos atinentes às relações internacionais do Brasil.
      </>
    ),
    link: '/docs/projetos/dados/hemeroteca-peb/intro',
    tipo: 'dados',
  },
  {
    title: 'NewsCloud',
    imageUrl: './img/projetos/dados/news-cloud/news-cloud.svg',
    description: (
      <>
      Uma importante fonte para pesquisa acadêmica são as notícias veiculadas pelos jornais impressos. Nesses veículos de comunicação, além de informações nacionais e internacionais relevantes, são encontrados opiniões de importantes atores políticos. Porém, o conjunto de informações de cada jornal se encontram em bases de dados distintas sem um mecanismo que viabilize uma busca agregada; a indexação integral dos dados vinculados apresenta limitações que dificultam a pesquisa avançada (utilização de operadores booleanos) através da busca por palavras-chaves, especialmente, quando selecionamos um período temporal longo e abarcamos o grande volume de informação; as informações veiculadas em formato textual não estão estruturadas, isso dificulta o cruzamento de metadados importantes (título, autor, caderno, entre outros). Devido a isso, o objetivo geral desse projeto é coletar, indexar, tratar e estruturar as informações veiculadas por jornais impressos. Mais especificamente, este projeto visa (1) coletar integralmente os jornais impressos selecionados, realizando o devido tratamento das informações veiculadas para uma melhor utilização dos dados para pesquisas acadêmicas; (2) subsidiar pesquisas acadêmicas que utilizam jornais impressos como fontes de informação ou objeto de estudo; (3) fornecer um instrumento básico para análise das informações veiculadas; (4) indicar possibilidades e instrumentos que auxiliem análises mais detalhadas das informações veiculadas.        
      </>
    ),
    link: '/docs/projetos/dados/newscloud/intro',
    tipo: 'dados',
  },
  {
    title: 'DiáriosBR',
    imageUrl: 'img/projetos/dados/diariosbr/diariosbr.svg',
    description: (
      <>
      Os Diários Oficiais reúnem a movimentação legal dos governos federal, estadual e municipal são publicadas, sendo esse fator o que os tornam uma fonte de pesquisa importante para o acompanhamento da destinação de recursos, transferência de cargos e o embasamento legal das atividades da administração pública. Os Diários são uma importante fonte tanto para pesquisas acadêmicas como também para uma melhor participação social no cotidiano da administração pública. Apesar destes dados serem públicos muitas vezes são disponibilizados em formatos, como PDF, que dificultam uma análise de dados mais aprofundada e rápida sendo necessário um grande tempo despendido para o tratamento destes dados para viabilizar uma análise mais acurada.
      </>
    ),
    link: '/docs/projetos/dados/diariosbr/intro',
    tipo: 'dados',
  },
  {
    title: 'Projeto Full Text - Tudo em Texto',
    imageUrl: 'img/projetos/dados/full-text/full-text.svg',
    description: (
      <>
        Em geral, grande parte dos dados disponibilizados publicamente na internet estão em formatos que dificultam uma rápida e adequada utilização. As boas práticas indicadas nas discussões em torno de dados abertos acabam não sendo seguidas. Com isso, é comum vermos dados sendo disponibilizados no formato de imagem, de pdf ou audiovisual sem transcrição  textual. O projeto Full Text visa auxiliar o tratamento desses dados, convertendo e/ou extraindo tais dados para formato textual passível de indexação e/ou melhor estruturação. A realização de pós-processamento e/ou OCR em textos disponibilizados no formato de imagem, a conversão de áudios e vídeos para o formato textual e a extração de conteúdos disponibilizados em pdfs estão entre as atividades realizadas nesse projeto. 
      </>
    ),
    link: '/docs/projetos/dados/full-text/intro',
    tipo: 'dados',
  },
  {
    title: 'Mercodocs',
    imageUrl: 'img/projetos/dados/mercodocs/mercodocs.svg',
    description: (
      <>
      O Mercosul possui uma gama extensa e variada de documentação oficial e pública. Apesar disso, encontramos vários problemas que dificultam a adequada utilização deste material. Dentre destes problemas, podemos destacar: a plataforma que disponibiliza os documentos não indexada integralmente os arquivos; alguns metadados que apresentam inconsistência; muitos documentos estão em formato de imagem (tiff) ou em pdf não pesquisável, sendo necessário a realização de OCR para uma melhor utilização dos arquivos neste estado. A partir deste cenário, o projeto MercoDocs objetiva auxiliar na coleta, tratamento e melhor disponibilização da documentação pública do Mercosul.
      </>
    ),
    link: '/docs/projetos/dados/mercodocs/intro',
    tipo: 'dados',
  },
  {
    title: 'Tweepina',
    imageUrl: 'img/projetos/dados/tweepina/tweepina.svg',
    description: (
      <>
      O Twitter é uma das principais redes sociais da atualidade, sendo muito utilizado por autoridades e instituições públicas que são objetos de estudo de várias pesquisas acadêmicas. Porém, ter acesso a série histórica de tweets dessas autoridades e instituições muitas vezes é difícil. Além disso, muitos tweets acabam sendo deletados, não estando disponíveis em arquivos e/ou repositórios públicos. Devido a isso, o objetivo geral desse projeto é reunir tweets de autoridades e instituições públicas com especial destaque ao Brasil e organismos internacionais. Mais especificamente, este projeto visa (1) auxiliar a construção de uma memória de informações de autoridades e instituições públicas divulgadas através do Twitter; (2) subsidiar pesquisas acadêmicas que utilizam o Twitter como fonte de informação de seus objetos de estudo através da disponibilização das variáveis disponibilizadas pelo Twitter; (3) fornecer um instrumento básico de análise de tweets; (4) indicar possibilidades e instrumentos que auxiliem uma análise mais detalhada dos tweets.
      </>
    ),
    link: '/docs/projetos/dados/tweepina/intro',
    tipo: 'dados',
  },
  {
    title: 'Acervo Redalint',
    imageUrl: 'img/projetos/dados/acervo-redalint/redalint.svg',
    description: (
      <>
       A produção científica de acesso aberto sobre a internacionalização da educação superior na América Latina se encontra dispersa em variados portais, há certa inconsistência nos metadados destas publicações e a indexação integral deste material é rara. A partir deste diagnóstico, o Acervo REDALINT surgiu com o objetivo de reunir em uma plataforma a produção científica sobre esta temática fornecendo metadados consistentes e a indexação integral do conteúdo disponível. 
      </>
    ),
    link: '/docs/projetos/dados/acervo-redalint/intro',
    tipo: 'dados',
  },
  {
    title: 'IRjournalsBR',
    imageUrl: 'img/projetos/dados/irjournalsbr/irjournalsbr.svg',
    description: (
      <>
        As revistas acadêmicas brasileiras de Relações Internacionais seguem as políticas de acesso aberto. Com isso, todo o conhecimento divulgado por estas revistas podem ser acessados gratuitamente por qualquer pessoa interessada e podem ser reutilizados sem prévia autorização dos editores e autores, desde que seja respeitada a licença de uso do Creative Commons adotado pelos respectivos periódicos. Apesar da adoção do acesso aberto ser um aspecto importante para garantir acesso universal ao conhecimento científico, parte dos metadados destas revistas apresentam inconsistências. Ademais, a indexação integral do conteúdo dessas revistas não é disponibilizada publicamente, dificultando a busca por palavras chaves na integralidade dos arquivos. A partir do exposto acima, o projeto IRjornalsBR objetiva formar uma base de dados que aglutine tanto metadados mais consistentes como também forneça uma indexação integral destas revistas.
      </>
    ),
    link: '/docs/projetos/dados/irjournalsbr/intro',
    tipo: 'dados',
  },
  {
    title: 'Gov Latin America',
    imageUrl: 'img/projetos/dados/gov-latam/gov-latam.svg',
    description: (
      <>
       Os dados públicos dos órgãos governamentais latino americanos disponíveis via web com frequência são retirados dos sites oficiais, especialmente, após a passagem de um mandato presidencial para outro. A partir deste diagnóstico, o objetivo do projeto GovLatinAmerica é coletar tais dados para que possam ser utilizados em pesquisas acadêmicas diversas. 
      </>
    ),
    link: '/docs/projetos/dados/gov-latin-america/intro',
    tipo: 'dados',
  },

];

function Projeto({imageUrl, title, description, pessoas, instituicoesEnvolvidas, financiamento, link, objetivo}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>

        <h3>{title}</h3>
        <p>{description}</p>
        {/* <p> <b> Objetivo: </b> {objetivo} </p>
        <p> <b> Descrição: </b> {description}</p>
        <p> <b> Pessoas envolvidas: </b> {pessoas}</p>
        <p> <b> Instituições Envolvidas: </b> {instituicoesEnvolvidas}</p>
        <p> <b> Financiamento: </b>  {financiamento} </p> */}
        <a className="button button--secondary" href={link}>Saiba Mais</a>

      </div>

    </div>
  );
}

function Projetos_dados() {
  return (
    <Layout title="Projetos de Dados">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className="hero__title">Projetos</h1>
          <h2 className="hero__subtitle">Conheça e Contribua com os projetos do LabRI</h2>
        </div>
      </header>
      <div className={clsx(styles.apresentacao, styles.container)}>
        <h2> Apresentação </h2>
        <p>
          Abaixo estarão expostos os projetos nos quais o LabRI participa, assim como informações essenciais sobre esses projeto.
        </p>
        <p>Os projetos do LabRI são divididos conforme seus objetivos. são eles: </p>
        <div className="row tipos_projeto">
          <div className="col col--4">
            <h3><a href="/projetos/sistemas/">Projetos de Sistemas</a></h3>
          </div>
          <div className="col col--4">
            <h3><a href="/projetos/ensino/">Projetos de Ensino</a></h3>
          </div>
          <div className="col col--4">
            <h3><a href="/projetos/extensao/">Projetos de Extensão</a>
          </h3></div>
        </div>
      </div>

      <main className={styles.main}>
      <section className={styles.content}>
        {projetos && projetos.length > 0 && (
          <section className={styles.projetos}>
            <div className="container">
              {projetos.map((props, idx) => (
                <Projeto key={idx} {...props} />
              ))}
            </div>
          </section>
        )}
      </section>
    </main>
    </Layout>
  );
}

export default Projetos_dados;
