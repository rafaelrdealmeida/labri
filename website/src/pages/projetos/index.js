import React from "react";
import Layout from "@theme/Layout";
import styles from "../styles.module.css";
import clsx from "clsx";
import useBaseUrl from "@docusaurus/useBaseUrl";

const projetos = [
  {
    title: "Hemeroteca de Política Externa",
    objetivo:
      "Disponibilizar de forma Gratuita e Digital os documentos da Hemeroteca de Política Externa",
    imageUrl: "img/labriunesp-02.svg",
    description: (
      <>
        Biblioteca Digital de Política Externa, completamente disponível via
        WEB, no link redalint www.redalint.com
      </>
    ),
    pessoas: "Rafael de Almeida",
    instituicoesEnvolvidas: "Labri UNESP",
    financiamento: "Nenhum",
    link: "https://hemerotecapeb.lantri.org/recoll/#SobreEsteSite",
    tipo: "dados",
  },
  {
    title: "NewsCloud",
    objetivo:
      "Coletar informações de notícias de jornais e parseá-las de forma a mostrar somente as informações necessárias e organizar os dados coletados em um banco de ",
    imageUrl: "img/projetos/news_cloud.svg",
    description: (
      <>
        O intuito do projeto news cloud é usar os acessos que o LabRI possui de
        diversos jornais para coletar informações de notícias de jornais e
        parseá-las de forma a mostrar somente as informações necessárias para os
        pesquisadores e organizar os dados coletados em um banco de dados
      </>
    ),
    pessoas: "Rafael de Almeida",
    instituicoesEnvolvidas: "Labri UNESP",
    financiamento: "Nenhum",
    link: "https://hemerotecapeb.lantri.org/recoll/#SobreEsteSite",
    tipo: "dados",
  },
  {
    title: "Coleta e Disponibilização de Diários Oficiais",
    objetivo:
      "Disponibilizar de forma Gratuita e Digital os Diários Oficiais do Estado Brasileiro, do estado de São Paulo e do Município de Franca",
    imageUrl: "img/projetos/coleta_diarios.png",
    description: (
      <>
        Este projeto visa Disponibilizar de forma Gratuita e Digital os Diários
        Oficiais do Estado Brasileiro, do estado de São Paulo e do Município de
        Franca
      </>
    ),
    pessoas: "Rafael de Almeida",
    instituicoesEnvolvidas: "Labri UNESP",
    financiamento: "Nenhum",
    link: "#",
    tipo: "dados",
  },
  {
    title: "Curso Trilha de Dados",
    objetivo:
      "Disponibilizar um curso básico de Ciência de Dados acessível a todos os estudantes de todos os níveis de conhecimento de programação, focado em aplicações nas Relações Internacionais",
    imageUrl: "./img/projetos/trilha_de_dados.svg",
    description: (
      <>
        Curso de Ciência de Dados (Data Science) voltado para estudantes de
        ciências sociais, em especial Relações Internacionais. Não é necessária
        nenhuma experiência com programação, o curso abarcará aspectos básicos
        de lógica de programação e fundamentos de python
      </>
    ),
    pessoas: "Pedro Henrique Moura Rafael Almeida, Pedro Rocha",
    instituicoesEnvolvidas: "LabRI Unesp",
    financiamento: "Nenhum",
    link: "https://gitlab.com/unesp-labri/curso/trilhas-labri-ciencia-dados",
    tipo: "educacao",
  },
  {
    title: "Projeto Full Text - Tudo em Texto",
    objetivo:
      "Disponibilizar diversoss conteúdos de diversas mídias em formato texto para permitir o processamento computacional",
    imageUrl: "img/projetos/full_text.svg",
    description: (
      <>
        O projeto Full Text busca viabilizar o processamento de diversos
        conteúdos (em áudio, texto, vídeo, revistas, etc) em formato texto. Uma
        vez transformados, os textos permitem ao pesquisador realizar o
        processamento automático, baseado em softwares próprios ou de terceiros.
        Ajuda também a manter uma memória institucional/cultural dos conteúdos
        que estão sendo transformados em texto, pois haevrá um backup dessas
        mídias.
      </>
    ),
    pessoas: "Pedro Henrique Moura, Rafael de Almeida",
    instituicoesEnvolvidas: "LabRI Unesp",
    financiamento: "Nenhum",
    link: "/full-text",
    tipo: "dados",
  },
  {
    title: "Projeto As Relações Internacionais e o Novo Corona Vírus",
    objetivo:
      "Debater junto estudantes as transformações nas Relações Internacionais relacionadas com o novo Corona Vírus",
    imageUrl: "img/projetos/projeto_covid.png",
    description: (
      <>
        O projeto As Relações Internacionais e o Novo Corona vírus busca trazer
        as transformações nas Relações Internacionais para que alunos pensem,
        debatam e produzam sobre esse tema, de forma a aumentar a vivência com o
        aspectos da análise e prática das Relações Internacionais.
      </>
    ),
    pessoas: "Pedro Henrique Moura, Rafael de Almeida",
    instituicoesEnvolvidas: "LabRI Unesp",
    financiamento: "Nenhum",
    link: "covid19",
    tipo: "extensao",
  },
  {
    title: "Mercodocs",
    imageUrl: "img/projetos/mercodocs.svg",
    description: (
      <>
        Visando a criação de uma memória institucional mais sólida das
        Instituições Regionais latinoamericanas, este projeto busca criar uma
        biblioteca digital gratuita e pesquisável de todos os documentos do
        Mercosul.
      </>
    ),
    link: "/docs/projetos/dados/mercodocs/intro",
    tipo: "dados",
  },
  {
    title: "Tweepina",
    imageUrl: "img/projetos/tweepina.svg",
    description: (
      <>
        O Twitter consolidou-se como uma das redes sociais mais importantes da
        atualidade. Dessa forma, este projeto visa criar uma base de dados
        acessível a qualquer pesquisador com uma série de informações acerca dos
        perfils de pessoas e Instituições a fim de facilitar investigações e
        análises sobre esses dados.
      </>
    ),
    link: "/docs/projetos/dados/tweepina/intro",
    tipo: "dados",
  },
];

const sobre = [
  {
    title: "Projetos de Dados",
    imgLogo: "/img/projetos/dados/logo-projetos-dados.svg",
    description:
      "Os projetos de dados são iniciativas que visam coletar, tratar, analisar e, quando possível, disponibilizar dados relevantes para as pesquisas em Relações Internacionais. Deste modo, neste espaço é possível encontrar projetos voltados à incorporação de tecnologias digitais para melhorar o manuseio da crescente quantidade de dados disponíveis nas redes sociais, nos meios de comunicação, nas instâncias governamentais e organismos internacionais.",
    link: "/projetos/dados/",
  },
  {
    title: "Projetos de extensão",
    imgLogo: "/img/projetos/extensao/projetos-extensao.svg",
    description:
      "Os projetos de extensão são iniciativas voltadas para melhorar e incentivar a interação entre o meio acadêmico e a sociedade. Assim, neste espaço é possível encontrar projetos que buscam envolver docentes e discentes em atividades voltadas à divulgação científica.",
    link: "/projetos/extensao/",
  },
];

const sobrep = [
  {
    title: "Projetos de Ensino",
    imgLogo: "/img/projetos/ensino/logo-projetos-ensino.svg",
    description: (
      <>
        Os projetos de ensino são iniciativas voltadas à promoção de oficinas,
        material de apoio e cursos que incentivem e auxiliem uma melhor
        incorporação de ferramentas tecnológicas nas atividades de pesquisa.
        Sendo assim, neste espaço é possível encontrar projetos conectados para
        melhorar o intercâmbio entre as Relações Internacionais e a ciência de
        dados.
      </>
    ),
    link: "/projetos/ensino/",
  },
  {
    title: "Projetos de Sistema",
    imgLogo: "/img/projetos/sistemas/logo-projetos-sistemas.svg",
    description:
      "Os projetos de sistemas são iniciativas que visam promover uma melhor utilização  dos recursos computacionais, bem como a implementação e o desenvolvimento de software e serviços voltados para as pesquisas de Relações Internacionais. Com isso, neste espaço é possível encontrar projetos relacionados à construção e manutenção de pequenas e versáteis estações de trabalho multifuncionais que dão suporte às atividades de pesquisa, ensino e extensão. Ademais, também é possível encontrar alguns softwares e serviços utilizados e/ou mantidos pelo LabRI/UNESP.",
    link: "/projetos/sistemas",
  },
];

function Projeto({
  imageUrl,
  title,
  description,
  pessoas,
  instituicoesEnvolvidas,
  financiamento,
  link,
  objetivo,
}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>
        <h3>{title}</h3>
        <p>
          {" "}
          <b> Objetivo: </b> {objetivo}{" "}
        </p>
        <p>
          {" "}
          <b> Descrição: </b> {description}
        </p>
        <p>
          {" "}
          <b> Pessoas envolvidas: </b> {pessoas}
        </p>
        <p>
          {" "}
          <b> Instituições Envolvidas: </b> {instituicoesEnvolvidas}
        </p>
        <p>
          {" "}
          <b> Financiamento: </b> {financiamento}{" "}
        </p>
        <a className="button button--secondary" href={link}>
          Saiba Mais
        </a>
      </div>
    </div>
  );
}

function Sobre({
  title,
  logo,
  imgLogo,
  logo2,
  imgLogo2,
  imageUrl,
  description,
  description2,
  link,
}) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.Infos, "card col col--6")}>
      <div className="card-container">
        <div>
          <img src={imgLogo} alt={logo} width="180" height="110" />
        </div>
        <div className="card__body">
          <h4>{description}</h4>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>
            <b>Acesse aqui</b>
          </a>
        </div>
      </div>
    </div>
  );
}

function SobreP({ title, logo, imgLogo, imageUrl, description, link }) {
  const imagemInfo = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.InfosP, "card col col--6")}>
      <div className="card-container">
        <div>
          <img src={imgLogo} alt={logo} width="180" height="110" />
        </div>
        <div className="card__body">
          <h4>{description}</h4>
        </div>
        <div className="card__footer">
          <a className="button button--secondary" href={link}>
            <b>Acesse aqui</b>
          </a>
        </div>
      </div>
    </div>
  );
}

function Projetos() {
  return (
    <Layout title="Projetos">
      <header className={clsx("hero hero--primary", styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className="hero__title">Projetos</h1>
          <h2 className="hero__subtitle">
            Conheça e Contribua com os projetos do LabRI
          </h2>
        </div>
      </header>
      <main>
        <section className={styles.content}>
          <div className={clsx(styles.apresentacao, styles.container)}>
            <div className={clsx(styles.h1A)}> APRESENTAÇÃO </div>
            <p>
              Parte significativa das atividades do LabRI se relacionam a
              projetos que envolvem discentes, docentes e grupos de pesquisas
              vinculados ao curso de Relações Internacionais da UNESP e do
              Programa de Pós-Graduação em Relações Internacionais San Tiago
              Dantas (UNESP, UNICAMP, PUC-SP). Tais iniciativas não são projetos
              de pesquisa, mas sim projetos que visam fornecer suporte a
              pesquisa, por exemplo através do auxílio na construção e
              manutenção de bases de dados. Para deixar mais clara esta
              distinção os projetos mais diretamente relacionados à pesquisa são
              denominados como “projetos de dados”.
            </p>
            <p>
              Além deste tipo de projeto o LabRI/UNESP também conduz e/ou
              auxilia projetos de ensino, extensão e de sistemas. Tais projetos
              são quatro frentes de trabalho importantes dentre as atividades
              realizadas no LabRI/UNESP. A principal característica comum no
              desenvolvimento de tais projetos é a utilização de tecnologias
              digitais e a preferência pela utilização de software livre e/ou de
              código aberto.
            </p>
            <p>
              Tais projetos podem ser denominados, de maneira mais geral, como
              projetos integrados ou projetos guarda chuva, pois são grandes
              frentes de trabalho que servem para organizar e articular melhor
              as atividades em que o LabRI/UNESP está envolvido. Tais projetos
              se dividem em subprojetos desenvolvidos em parceria com discentes,
              docentes, grupos de pesquisa entre outros parceiros e
              colaboradores. Veja abaixo como os projetos integrados estão
              estruturados e quais são seus subprojetos.
            </p>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {sobre.map((props, idx) => (
                <Sobre key={idx} {...props} />
              ))}
            </div>
          </div>

          <div className={clsx(styles.container)}>
            <div className="row">
              {sobrep.map((props, idx) => (
                <SobreP key={idx} {...props} />
              ))}
            </div>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Projetos;
