import React from 'react';
import Layout from '@theme/Layout';
import styles from '../styles.module.css';
import clsx from 'clsx';
import useBaseUrl from '@docusaurus/useBaseUrl';


const projetos = [
  {
    title: 'Websites e Redes',
    imageUrl: '/img/projetos/sistemas/web-logo.svg',
    description: (
      <>
      </>
    ),
    link: '/docs/projetos/sistemas/web-redes/intro',
    tipo: 'sistemas',
  },
  {
    title: 'Utilização de Distribuição Linux',
    imageUrl: '/img/labriunesp-02.svg',
    description: (
      <>
      </>
    ),
    link: '/docs/projetos/sistemas/linux/intro',
    tipo: 'sistemas',
  },
  {
    title: 'Cadernos LabRI',
    imageUrl: '/img/labriunesp-02.svg',
    description: (
      <>
      </>
    ),
    link: '/docs/projetos/sistemas/dev/geral/cadernos-labri',
    tipo: 'sistemas',
  },
  {
    title: 'Sistema de Confecção de Certificados',
    imageUrl: '/img/labriunesp-02.svg',
    description: (
      <>
      </>
    ),
    link: '/docs/projetos/sistemas/dev/geral/certificados',
    tipo: 'sistemas',
  },
  {
    title: 'Sistema de OCR Automático',
    imageUrl: '/img/labriunesp-02.svg',
    description: (
      <>
      </>
    ),
    link: '/docs/projetos/sistemas/dev/geral/ocr',
    tipo: 'sistemas',
  },

];

function Projeto({imageUrl, title, description, pessoas, instituicoesEnvolvidas, financiamento, link, objetivo}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>

        <h3>{title}</h3>
        <p> <b></b> {description}</p>
        <a className="button button--secondary" href={link}>Saiba Mais</a>

      </div>

    </div>
  );
}

function Projetos_sistemas() {
  return (
    <Layout title="Projetos de Sistemas">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className="hero__title">Projetos</h1>
          <h2 className="hero__subtitle">Conheça e Contribua com os projetos do LabRI</h2>
        </div>
      </header>
      <div className={clsx(styles.apresentacao, styles.container)}>
        <h2> Apresentação </h2>
        <p>
          Abaixo estarão expostos os projetos nos quais o LabRI participa, assim como informações essenciais sobre esses projeto.
        </p>
        <p>Os projetos do LabRI são divididos conforme seus objetivos. são eles: </p>
        <div className="row tipos_projeto">
          <div className="col col--4">
            <h3><a href="/projetos/dados/">Projetos de Dados</a></h3>
          </div>
          <div className="col col--4">
            <h3><a href="/projetos/ensino/">Projetos de Ensino</a></h3>
          </div>
          <div className="col col--4">
            <h3><a href="/projetos/extensao/">Projetos de Extensão</a></h3>
          </div>
        </div>
      </div>

      <main className={styles.main}>
      <section className={styles.content}>
        {projetos && projetos.length > 0 && (
          <section className={styles.projetos}>
            <div className="container">
              {projetos.map((props, idx) => (
                <Projeto key={idx} {...props} />
              ))}
            </div>
          </section>
        )}
      </section>
    </main>
    </Layout>
  );
}

export default Projetos_sistemas;
