import React from 'react';
import Layout from '@theme/Layout';
import styles from '../styles.module.css';
import clsx from 'clsx';
import useBaseUrl from '@docusaurus/useBaseUrl';


const projetos = [
  
  {
    title: 'Pod-RI',
    imageUrl: '/img/projetos/extensao/pod-ri/pod-ri.jpg',
    description: (
      <>
        O Pod-RI é um projeto feito por estudantes, no formato de podcast, iniciado como uma ideia entre amigos. Gravamos episódios com diversas temáticas das Relações Internacionais, assim como conversas com especialistas e profissionais. Além disso, produzimos conteúdos para redes sociais, onde entramos em contato mais direto com a comunidade de Relações Internacionais do Brasil.
      </>
    ),
    link: '/docs/projetos/extensao/pod-ri/intro',
    tipo: 'extensao',
  },
  {
    title: 'Pandemia e as Relações Internacionais',
    imageUrl: '/img/projetos/extensao/labri-pandemia/labri-pandemia.svg',
    description: (
      <>
        'As Relações Internacionais e o Novo Coronavírus' surgiu a partir da iniciativa do Laboratório de Relações Internacionais (LabRI) da UNESP Franca. O projeto contou com a participação de vários docentes e discentes da universidade e tinha como principal objetivo produzir conteúdos sobre a pandemia de COVID-19 sob a perspectiva das Relações Internacionais.
      </>
    ),
    link: '/docs/projetos/extensao/covid19/intro',
    tipo: 'extensao',
  },

];

function Projeto({imageUrl, title, description, pessoas, instituicoesEnvolvidas, financiamento, link, objetivo}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={styles.Projeto}>
      <div className={styles.imagemProjeto}>
        {imgUrl && (
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        )}
      </div>
      <div className={styles.projetoTexto}>

        <h3>{title}</h3>
        <p> <b></b> {description}</p>
        <a className="button button--secondary" href={link}>Saiba Mais</a>

      </div>

    </div>
  );
}

function Projetos_extensao(){
  return (
    <Layout title="Projeto De Extensao">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className={styles.container}>
          <h1 className="hero__title">Projetos</h1>
          <h2 className="hero__subtitle">Conheça e Contribua com os projetos de Extensão do LabRI</h2>
        </div>
      </header>
      <div className={clsx(styles.apresentacao, styles.container)}>
        <h2> Apresentação </h2>
        <p>
          Abaixo estarão expostos os projetos nos quais o LabRI participa, assim como informações essenciais sobre esses projeto.
        </p>
        
        <div className="row tipos_projeto">
          <div className="col col--4">
            <h3><a href="/projetos/dados/">Projetos de Dados</a></h3>
          </div>
          <div className="col col--4">
            <h3><a href="/projetos/ensino/">Projetos de Ensino</a></h3>
          </div>
          <div className="col col--4">
            <h3><a href="/projetos/sistemas/">Projetos de Sistemas</a></h3>
          </div>
        </div>
        
      </div>

      <main className={styles.main}>
      <section className={styles.content}>
        {projetos && projetos.length > 0 && (
          <section className={styles.projetos}>
            <div className="container">
              {projetos.map((props, idx) => (
                <Projeto key={idx} {...props} />
              ))}
            </div>
          </section>
        )}
      </section>
    </main>
    </Layout>
  );
}

export default Projetos_extensao;
