---
id: mdx
title: Powered by MDX
---

You can write JSX and use React components within your Markdown thanks to [MDX](https://mdxjs.com/).

export const Highlight = ({children, color}) => ( <span style={{
      backgroundColor: color,
      borderRadius: '2px',
      color: '#fff',
      padding: '0.2rem',
    }} >{children}</span> );

export const Coluna1 = ({children}) => ( <span style={{
      display: flex,
      width: '45%',
      height: '10rem',
      backgroundColor: 'pink',
      borderRadius: '2px',
      padding: '0.2rem',
    }} ><div>{children}</div></span>);

export const Coluna2 = ({children}) => ( <span style={{
      display: flex,
      width: '45%',
      height: '10rem',
      backgroundColor: 'blue',
      borderRadius: '2px',
      padding: '0.2rem',
    }} ><div>{children}</div></span>);


<Highlight color="#25c2a0">Docusaurus green</Highlight> and <Highlight color="#1877F2">Facebook blue</Highlight> are my favorite colors.

<<<<<<< HEAD
I can write **Markdown** alongside my _JSX_!




<!-- 
<Coluna1> TEXTO TEXTO </Coluna1>
<Coluna2> TEXTO TEXTO </Coluna2> -->
=======
I can write **Markdown** alongside my _JSX_!
>>>>>>> 3c8c74eb785f0cf16a641aea5a61ee90a2029470
