---
id: series-labri
title: Séries LabRI
sidebar_label: Séries LabRI
slug: /cadernos/series-labri
---

### Séries LabRI

Os Cadernos LabRI estão organizados em uma numeração contínua e categorizadas por séries que podem ser temáticas ou vinculada a algum grupo específico

### Séries LabRI disponíveis

- [Covid-19](https://labriunesp.org/cadernos/tags/covid-19)
