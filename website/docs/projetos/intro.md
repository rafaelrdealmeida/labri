---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/intro
---

Parte significativa das atividades do LabRI se relacionam a projetos que envolvem discentes, docentes e grupos de pesquisas vinculados ao curso de Relações Internacionais da UNESP, Campus Franca, e do Programa de Pós-Graduação em Relações Internacionais San Tiago Dantas (UNESP, UNICAMP, PUC-SP). Dividimos tais projetos em quatro principais frentes: dados, extensão, ensino, sistemas.

- _**[Projetos de Dados](/docs/projetos/dados/intro)**_

- _**[Projetos de Ensino](/docs/projetos/ensino/intro)**_

- _**[Projetos de Extensão](/docs/projetos/extensao/intro)**_

- _**[Projetos de Sistemas](/docs/projetos/sistemas/intro)**_