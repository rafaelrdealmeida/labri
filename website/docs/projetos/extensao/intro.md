---
id: intro
title: Projetos LabRI/UNESP
sidebar_label: Projetos
slug: /projetos/extensao/intro
---

<center>
    <img src="/img/projetos/extensao/logo-projetos-extensao.svg" alt="centered image" width="300" height="180" />
</center>

Os projetos de extensão são iniciativas voltadas para melhorar e incentivar a interação entre o meio acadêmico e a sociedade. Assim, neste espaço é possível encontrar projetos que buscam envolver docentes e discentes em atividades voltadas à divulgação científica.

- [Projetos de Dados](/docs/projetos/dados/intro)

- [Projetos de Ensino](/docs/projetos/ensino/intro)

- Projetos de Entensão

- Projetos de Infraestutura computacional
