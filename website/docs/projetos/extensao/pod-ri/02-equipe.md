---
id: equipe
title: Equipe Pod-RI
sidebar_label: Equipe Pod-RI
slug: /projetos/extensao/pod-ri/equipe
---

Atualmente a equipe do Pod-RI é composta por onze graduandos de Relações Internacionais da UNESP/Franca.

- Ailton Salvadori

- Bianca Cintra da Costa Antunes

- Bruno Cesar David Ruy

- Danielle Elis Alves Valdivia 

- Enzo Mendes Golfetti 

- Gustavo Pasqueta

- Leonardo Pagano Landucci 

- Samuel Davis Domingues Lima

- Sofia Navarro Aguiar

- Stephany Anicezio de Souza Primo

- Victoria dos Anjos Gois
