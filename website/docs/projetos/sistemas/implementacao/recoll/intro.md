---
id: intro
title: Intro-Recoll
sidebar_label: Intro-Recoll
slug: /projetos/sistemas/implementacao/recoll/intro
---

|What|Examples|
|---|---|
|And|one two one AND two one&&two|
|Or|one OR two oneIItwo|
|Complex boolean. OR has priority, use parentheses where needed|(onde AND two) OR three|
|Not|-term|
|Phrase|"pride and prejudice"|
|Ordered proximity (slack=1)|"pride prejudice"o1|
|Unordered proximity (slack=1)|"prejudice pride"po1|
|Unordered prox. (default slack=10)|"prejudice pride"p|
|No stem expansion: capitalize|Floor|
|Field-specific|author:austen title:prejudice|
|AND inside field(no order)|author:jane,austen|
|OR inside field|author:austen/bronte|
|Field names|title/subject/caption author/from <br/> recipient/to filename ext|
|Directory path filter|dir:/home/me dir:doc|
|MIME type filter|mime:text/plain mime:video/*|
|Date intervals|date:2018-01-01/2018-31-12 <br/> date:2018 date: 2018-01-01/P12M|
|Size|size>100k size<1M|


