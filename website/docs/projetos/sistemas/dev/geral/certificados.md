---
id: certificados
title: Certificados
sidebar_label: Certificados
slug: /projetos/sistemas/dev/geral/certificados
---

## Como gerar certificados automaticamente

1. Use o [Google Apresentações](https://www.google.com/intl/pt-BR/slides/about/), [Google Formulários](https://www.google.com/intl/pt-BR/forms/about/) e [Google Planilhas](https://www.google.com/intl/pt-BR/sheets/about/)
1. O Apresentações será usado para criar o _template_ do certificado, onde você irá inserir os elementos gráficos e as informações gerais. 
1. Através do Google Formulários, você irá coletar as informações como e-mail, nome e etc., que irão diretamente para a planilha que servirá como base de dados para a geração de certificados
1. No Planilhas, é importante ressaltar que utilizaremos uma ferramenta (complemento) chamada [Autocrat](https://workspace.google.com/u/0/marketplace/app/autocrat/539341275670?hl=pt&pann=sheets_addon_widget), que fará a integração da planilha com o arquivo do Google Apresentações.


### Passo a passo

1. Abra o Google Apresentações e crie um template básico que servirá para o Certificado.
    1. Note que aqui colocamos algumas variáveis, como `<<nome>>`, `<<data>>`, `<<ch>>` e `<<responsavel>>`. É importante que essas variáveis estejam entre os sinais de `<<>>`, pois é assim que o Autocrat irá "ler" as informações da planilha e convertê-las para o certificado.
    1. Guarde o local onde você salvou esse certificado, recomenda-se que crie uma pasta no Google Drive exclusiva para o processo como um todo.

![Apresentacoes](https://i.imgur.com/O6ihju3.png)

2. Agora, usando o Formulários, crie um formulário padrão, onde você irá requisitar as informações dos participantes/de quem vai ter seus dados inseridos no certificado.
    1. O mais importante nesta parte é solicitar o e-mail, através das configurações → coletar e-mail.
    1. Na aba de respostas, selecione a opção para criar uma planilha, tentando coloca-la na mesma pasta do drive que você criou o formulário e a apresentação/certificado.

![Formularios](https://i.imgur.com/QiFMzft.png)

3. Em seguida, trabalharemos com a Planilha. Aqui, na primeira linha (row), você notará que ela estará pré-preenchida com alguns dos dados que você solicitou no formulário, porém aqui podemos adicionar mais colunas que entrarão diretamente nas variáveis estabelecidas no certificado
    1. Desta forma, podemos adicionar a Carga horária (ch), o Responsável (responsavel) e a data (data), além de qualquer outra variável que você julgar válida e tiver colocado no certificado.
    1. Com as variáveis estabelecidas, aguarde coletar as informações necessárias dos participantes. No caso, estou trabalhando com meus dois e-mails para fazer o teste, mas todos que preencheram o formulário devem aparecer nas linhas. 

![Planilha](https://i.imgur.com/lvjtRFP.png)

4. A seguir, utilizaremos o servico do Autocrat. Para isso, iremos na aba de complementos → autocrat → open
    1. Criaremos um novo _job_ e selecionaremos o template (certificado criado no apresentações)
    1. A ferramenta já reconhece a maioria das variáveis e as "linka" com as estabelecidas na planilha, mas é importante que você cheque e preencha com as correspondentes. É válido ressaltar que você deve atentar-se a qual aba da planilha está trabalhando.
    1. Seguindo nas opções, vamos deixar criar um nome para cada certificado criado. Para isso, utilizaremos alguma variável que estabelecemos, como a `<<nome>>`. Desta forma, o Drive reconhece cada documento como único e não criará conflitos na hora de enviar os certificados. Ainda nesta mesma página, devemos marcar a opção `PDF` na caixa, e marcar a primeira opção (Multiple output mode (classic mode)).
    1. Selecione uma pasta no seu drive para onde esses certificados serão armazenados e pule as duas próximas opções (não trabalharemos com elas por enquanto). A seguir, você deverá marcar a opção de compartilhar documento e abrirá uma caixa para criar um modelo de email. Aqui, o importante é que no campo **To** você coloque `<<email>>` (da mesma forma que estiver na coluna da planilha), para que o serviço envie de forma automática os emails para as contas registradas



5. A seguir, salve e crie no ícone de _play_, que fará o Autocrat rodar. Em seguida, note que a aplicação "costurou" os dados e enviou os e-mails para as contas desejadas. 
