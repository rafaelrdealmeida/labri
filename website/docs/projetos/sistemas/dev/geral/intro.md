---
id: intro
title: Intro-DevLabRI
sidebar_label: Intro-DevLabRI
slug: /projetos/sistemas/dev/geral/intro
---

- [Certificados](https://apoio.labriunesp.org/docs/projetos/sistemas/dev/certificados/)
- [Cadernos do LabRI](https://apoio.labriunesp.org/docs/projetos/sistemas/dev/geral/cadernos-labri)
- [OCR](https://apoio.labriunesp.org/docs/projetos/sistemas/dev/geral/ocr)
- [Cadastro de Usuários](https://apoio.labriunesp.org/docs/projetos/sistemas/dev/geral/cadastro-usuarios)

