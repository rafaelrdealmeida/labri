---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/sistemas/intro
---

<center>
    <img src="/img/projetos/sistemas/logo-projetos-sistemas.svg" alt="centered image" width="250" height="150" />
</center>

Os projetos de infraestrutura computacional são iniciativas que visam promover uma melhor utilização dos recursos computacionais nas pesquisas de Relações Internacionais. Com isso, neste espaço é possível encontrar projetos relacionados à construção e manutenção de pequenas e versáteis estações de trabalho multifuncionais que dão suporte às atividades de pesquisa, ensino e extensão.
