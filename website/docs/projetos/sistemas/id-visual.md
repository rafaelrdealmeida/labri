---
id: id-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/sistemas/id-visual
---

### Sobre o Logo: 

* Fonte: Bodoni FLF
* Cores: #0A0A44; #6F6F70; #999999; #D8D8D5
* [Link(Canva)](https://www.canva.com/design/DAEdhYi4uJ0/s3vDkL8vudvpqh_vMKw7yQ/view?utm_content=DAEdhYi4uJ0&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)