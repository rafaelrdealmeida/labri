---
id: iniciativas
title: Iniciativas
sidebar_label: Iniciativas
slug: /projetos/sistemas/iniciativas
---

<center>
    <img src="/img/projetos/sistemas/logo-projetos-sistemas.svg" alt="centered image" width="300" height="150" />
</center>

- [Utilização de Distribuição Linux](/docs/projetos/sistemas/infra/linux/intro)
- [ProxMox](/docs/projetos/sistemas/infra/proxmox/intro)
- [RecollWeb](/docs/projetos/sistemas/dev/recollweb/intro)
- [Recoll](/docs/projetos/sistemas/dev/recoll/intro)
- [Certificados](/docs/projetos/sistemas/dev/certificados/intro)
- [Cadernos LabRI](/docs/projetos/sistemas/dev/cadernos-labri/intro)
- [OCR](/docs/projetos/sistemas/dev/ocr/intro)