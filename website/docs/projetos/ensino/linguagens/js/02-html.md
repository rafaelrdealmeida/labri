---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/linguagens/js/intro/hmtl
---

### O que é HTML?

HyperText Markup Language (HTML) trata-se da fonte primária de inserção de textos e imagens em uma página web. De certa maneira, o HTML puro é apenas a maneira abstrata de gerar informações, não contando com estilizações avançadas. Por esse motivo, aprender HTML significa aprender a base de toda a construção de uma página da Web e, posteriormente, aprender a intercalar esses conhecimentos com o Cascading Style Sheets (CSS) e o JavaScript para que o resultado final conte com maior nível de personalização e interação, facilitando a visualização e administração de dados. 

### Ferramentas necessárias 

Para aprender e dominar os conhecimentos sobre HTML é necessário utilizar um editor de texto[^1] para a prática do que será aprendido. Além disso, saber manusear um editor de texto pode se tornar de grande importância para facilitar processos que podem ser automatizados. 

Além disso, será necessário utilizar um navegador (Chrome, Safari, etc...) para poder visualizar o que está sendo gerado no editor de código. 

**Dica:** procure não utilizar o _Internet Explorer_ devido a limitação do navegador em lidar com ferramentas mais avançadas. 

[^1]: Sugestões de editor de texto: [Atom](https://atom.io) ou [VSCode](https://code.visualstudio.com).

## Primeiros Passos 

Crie uma pasta pessoal para o projeto para que todos os arquivos de código sejam encontrados facilmente. 

### Criando um arquivo 

**Editor de texto utilizado**: _Vs Code_ 

1. Abra o VS Code; 

2. Clique em "Arquivo" e depois em "Abrir pasta" para abrir a pasta do projeto;

3. Clique em "Arquivo" e depois em "Novo arquivo";

4. Utilize o comando "Ctrl + S" para Salvar o arquivo; 

5. Salve o Arquivo na pasta do projeto como "nomedoarquivo.html" para que o arquivo seja entendido como um documento HTML. 

6. Para criar um novo arquivo utilize o comando "Ctrl + N" e depois use o comando "Ctrl + S" para salvá-lo. 

### Entendendo as estruturas básicas da web

O HTML é utilizado para definir o conteúdo de uma página web. Sendo assim, para criar qualquer página na web é necessário aprender a utilizar o HTML para criar as estruturas básicas para o funcionamento das páginas criadas. 

## Criando uma primeira página com HTML 

1. Crie uma pasta chamada "basic-web-pages" dentro da sua pasta pessoal de códigos; 

2. Crie um arquivo chamado "basics.html"; 

### Estrutura do documento HTML 

Todo documento HTML precisa de uma estrutura básica, sendo essa composta em primeiro momento por <!DOCTYPE html> para indicar para o navegador que o documento está escrito em  HTML. 

Além disso, é necessário que todo o código esteja dentro da tag <html>. Sendo assim, abra o documento com 

    <html> ("opening tag") 

e feche o documento com 

    </html> ("closing tag"). 

Todo documento HTML precisa de uma tag 

    <head> conteúdo </head>

A tag "head" comporta todo o metadata do site: título da página, código de CSS, etc... 

A próxima tag básica de todo código de HTML seria o "body"

    <body> conteúdo </body>

Sendo assim: as três tags necessárias em todos os códigos de html são "html", "head" e "body".

Para escrever páginas que comportam caracteres utilizados na língua portguesa, utilize o comando "meta charset" no início da página 

    <!DOCTYPE html>
    <meta charset="utf-8">

Essa tag serve para comportar toda a parte vísivel do código na página web. 

    <!DOCTYPE html>
    <meta charset="utf-8"> 
    <html>
    <head>
    </head>
    <body>
    </body>
    </html>

#### Fazer um comentário no código 

Para criar um comentário que não será lido pelo código, basta utilizar 

    <!-- texto -->

### Adicionando o título da página 

Para adicionar um título para sua pagína basta utilizar a tag "title" no "head" do código

    <!DOCTYPE html>
    <meta charset="utf-8"> 
    <html>
        <head>
            <title>Título</title>
        </head>
    <body>
    </body>
    </html>

Para visualizar o código basta salvar o arquivo e abrir e visualizá-lo através de um navegador. 

### Adicionando parágrafos 

Para adicionar parágrafos basta utilizar a tag "p" no "body" do código

    <!DOCTYPE html>
    <meta charset="utf-8"> 
    <html>
        <head>
            <title>Título</title>
        </head>
        <body>
            <p>Exemplo de parágrafo</p>
        </body>
    </html>

**Dica:** utilize o site [Lorem Ipsum](https://www.lipsum.com) para gerar textos autómaticos. A sua utilização pode facilitar a criação de páginas modelo. 

### Utilizando "headings" (tamanhos de textos)

Muitas vezes se faz necessário utilizar diferentes tamanhos de textos no corpo da página: diferenciar um título de um texto, etc. Para fazer isso, basta utilizar as diferentes tags "h": h1, h2, h3, ..., h6. Quanto menor for o número, maior será o tamanho da fonte. 

    <!DOCTYPE html>
    <meta charset="utf-8"> 
    <html>
        <head>
            <title>Título</title>
        </head>
        <body>
            <h1>Exemplo de título</h1>
            <h2>Exemplo de subtítulo</h2>
            <p>Exemplo de parágrafo</p>
        </body>
    </html>

### Criando listas não ordenadas 

Utilize a tag "ul" para que o navegador reconheça que tudo que está dentro dessa tag pode ser entendido como uma lista não ordenada. 

    <body>
    <ul>
    </ul>
    </body>

Utilize a tag "li" para indicar cada um dos tópicos da lista 

    <body>
    <ul>
    <li>Tópico um</li>
    <li>Tópico dois</li>
    <li>Tópico três</li>
    </ul>
    </body>

### Criando listas ordenadas 

Utilize a tag "ol" para que o navegador reconheça que tudo que está dentro dessa tag pode ser entendido como uma lista ordenada. 

    <body>
    <ol>
    </ol>
    </body>

Utilize a tag "li" para indicar cada um dos tópicos da lista de forma ordenada (1, 2, 3, ...)

    <body>
    <ol>
    <li>Tópico um</li>
    <li>Tópico dois</li>
    <li>Tópico três</li>
    </ol>
    </body>

### Elementos inline: ênfase (itálico)

Para gerar ênfase em uma palavra/frase dentro de um parágrafo basta utilizar a tag "em"

    <body>
    <p>
    <em>Lorem ipsum</em> dolor sit amet, consectetur adipiscing elit.
    </p>
    </body>

### Elementos inline: destacar (negrito)

Para destacar uma palavra/frase dentro de um parágrafo basta utilizar a tag "strong"

    <body>
    <p>
    <strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit.
    </p>
    </body>

É possível utilizar a tag "em" e "strong" em conjunto 

    <body>
    <p>
    <em><strong>Lorem ipsum</strong></em> dolor sit amet, consectetur adipiscing elit.
    </p>
    </body>

### Como quebrar um texto 

#### Quebra de linha

Para quebrar uma linha utilize a tag "br" de "break" 

    <body>
    <p>
    Lorem ipsum<br/>
    dolor sit amet, consectetur adipiscing elit.
    </p>
    </body>

#### Quebra de texto utilizando uma "linha" horizontal 

Para quebrar um texto utilizando uma "linha" horizontal utilize a tag "hr"

    <body>
    <p>
    Lorem ipsum<br/>
    dolor sit amet
    </p>
    <hr/>
    <p>consectetur adipiscing elit.</p>
    </body>
