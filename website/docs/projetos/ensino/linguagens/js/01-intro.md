---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/linguagens/js/intro
---

### Introdução 

Guia de estudos de HTML, CSS, JavaScript, React e D3 com o foco na visualização de dados. 

Conteúdo: 

1. Estudo de "HTML"

    * Material apoio: [Site](https://calma-senhora.netlify.app)

2. Estudo de "CSS"

    * Material de apoio: [Site](https://calma-senhora.netlify.app) 
    
    * Sugestão: [GitHub](https://github.com/polyanetuag/30DiasDeCSS)

3. Estudo de "JavaScript" 

    * Material de apoio: [Site](https://calma-senhora.netlify.app)

    * Sugestão: [Youtube](https://www.youtube.com/playlist?list=PLirko8T4cEmzWZVn_ZKQbfDOuCnSZJ4va)

4. Estudo de "React"

    * Material de apoio: [Site](https://calma-senhora.netlify.app)

    * Sugestão: [Playlist 1](https://youtube.com/playlist?list=PLnDvRpP8BneyVA0SZ2okm-QBojomniQVO) e [Playlist 2](https://www.youtube.com/playlist?list=PLirko8T4cEmzWZVn_ZKQbfDOuCnSZJ4va)

5. Estudo de "D3" 

    * Material de apoio: [Curso pt. 1](https://youtu.be/2LhoCfjm8R4) e [Curso pt. 2](https://youtu.be/H2qPeJx1RDI)
