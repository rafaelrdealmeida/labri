---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/linguagens/python/intro
---




### Material de apoio

- [How to Use Generators and yield in Python](https://realpython.com/introduction-to-python-generators/)
- [Code Style](https://docs.python-guide.org/writing/style/)
- [Python zip()](https://www.programiz.com/python-programming/methods/built-in/zip)
- [How To Pretty Print in Python](https://betterprogramming.pub/how-to-pretty-print-in-python-9b1d8764d151)
- [Working With Files in Python](https://realpython.com/working-with-files-in-python/)
- [Working With Files in Python](https://realpython.com/python-csv/)
- [Why (and how) to put notebooks in production](https://towardsdatascience.com/why-and-how-to-put-notebooks-in-production-667fc3979dca)
- [Python Generators](https://www.programiz.com/python-programming/generator)
- [Welcome to Lazy Predict’s documentation!](https://lazypredict.readthedocs.io/en/latest/)
- [Algoritmo Prophet - Amazon Forecast](https://docs.aws.amazon.com/pt_br/forecast/latest/dg/aws-forecast-recipe-prophet.html)
