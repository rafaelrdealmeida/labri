---
id: p01-03-conversao-tipos
title: Conversão de Tipos
sidebar_label: Conversão de tipos
slug: /projetos/ensino/linguagens/python/p01-03-conversao-tipos
---

- [Link](https://replit.com/@fparon/conversao-tipos) do Replit dessa parte

```py
'''
Conversão de tipos de dados simples
'''
a = 'Brasil'
b = 10
c = 15.5
print(type(a))
print(type(b))
print(type(c))
print(int(c))

b = str(b)
print(type(b))
```
















