---
id: p00-01-ambiente
title: Preparação de ambiente
sidebar_label: Preparar Ambiente
slug: /projetos/ensino/linguagens/python/p00-01-ambiente
---

 - Editor de codigo e REPLIT
 - Ambiente virtual 
 - Versionamento (GIT)
 - Zen do Python


 ### Zen do Python

- [O Zen do Python](https://archive.is/wip/mkN75) é uma coleção de 19 princípios de software escritos em um poema que influencia o design da linguagem de programação Python. Foi publicado na lista de discussão do Python em junho de 1999 por Tim Peters

### Conda - ambiente virtual

```
conda init

```

#### Criar e ativar ambiente conda

```
conda config --set auto_activate_base false # desativar startup conda terminal
conda init # para inicializar o conda

which python

conda list

conda create --name env_projeto_A

conda activate env_projeto_A

```


#### 


```
conda install python
conda install -n env_projeto_A scipy

conda create --name env_projeto_A python=3.7 scipy pandas=0.25.3

conda info --envs # verificar ambiente existentes

conda env export > environment.yml # exportar as configs

conda env create -f environment.yml # criar um ambiente a partir de um arquivo de ambiente previamente criado

conda deactivate # desativer ambiente

 conda remove --name your_env_name --all # Remover ambiente conda


```

