---
id: iniciativas
title: Iniciativas
sidebar_label: Iniciativas
slug: /projetos/ensino/linguagens/intro
---

- [Python](/docs/projetos/ensino/linguagens/python)
- [Linguagem R](/docs/projetos/ensino/linguagens/r-lang)
- [Versionamento](/docs/projetos/ensino/linguagens/versionamento)