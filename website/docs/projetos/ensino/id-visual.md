---
id: id-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/ensino/id-visual
---

### Sobre o Logo

* Fontes: Glacial Indifference
* Cores: #D9D9D9; #AB6767
* [Link (Canva)](https://www.canva.com/design/DAEdhYi4uJ0/s3vDkL8vudvpqh_vMKw7yQ/view?utm_content=DAEdhYi4uJ0&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)
* Desenho: [Design Thinking](https://www.flaticon.com/free-icon/creating_4358454)