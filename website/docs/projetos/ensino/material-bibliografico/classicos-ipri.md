---
id: classicos-ipri
title: Clássicos IPRI
sidebar_label: Clássicos IPRI
slug: /projetos/ensino/material-bibliografico/classicos-ipri
---

O Acervo da FUNAG disponibiliza vários livros de forma gratuita através de sua biblioteca digital. Abaixo são listados os denominados "Clássicos IPRI":


|Livro|Link|
|---|---|
|Relectiones sobre os índios e sobre o poder civil|[Download](http://funag.gov.br/biblioteca-nova/produto/1-67-relectiones_sobre_os_indios_e_sobre_o_poder_civil)|
|História da guerra do Peloponeso|[Download](http://funag.gov.br/biblioteca-nova/produto/1-827-historia_da_guerra_do_peloponeso)|
|Vinte anos de crise: 1919-1939|[Download](http://funag.gov.br/biblioteca-nova/produto/1-1005-vinte_anos_de_crise_1919_1939)|
|A Sociedade Anárquica|[Download](http://funag.gov.br/biblioteca-nova/produto/1-599-sociedade_anarquica_a)|
|As consequências econômicas da paz|[Download](http://funag.gov.br/biblioteca-nova/produto/1-1059)|
|Paz e guerra entre as nações|[Download](http://funag.gov.br/biblioteca-nova/produto/1-556)|
|A grande ilusão|[Download](http://funag.gov.br/biblioteca-nova/produto/1-985-grande_ilusao_a)|
|A política do poder|[Download](http://funag.gov.br/biblioteca-nova/produto/1-1055-politica_do_poder_a)|
|A política entre as nações|[Download](http://funag.gov.br/biblioteca-nova/produto/1-986-politica_entre_as_nacoes_a)|
|Projeto para tornar perpétua a paz na Europa|[Download](http://funag.gov.br/biblioteca-nova/produto/1-999-projeto_para_tornar_perpetua_a_paz_na_europa)|
|Rousseau e as relações internacionais|[Download](http://funag.gov.br/biblioteca-nova/produto/1-988-rousseau_e_as_relacoes_internacionais)|
|O direito das gentes|[Download](http://funag.gov.br/biblioteca-nova/produto/1-685-direito_das_gentes_o)|
|Utopia|[Download](http://funag.gov.br/biblioteca-nova/produto/1-983-utopia)|
 
 
 




