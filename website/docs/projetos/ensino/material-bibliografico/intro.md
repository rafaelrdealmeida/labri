---
id: intro
title: Repositórios Digitais
sidebar_label: Repositórios Digitais
slug: /projetos/ensino/material-bibliografico/intro
---

Abaixo são indicados bases de dados e repositórios digitais que reúnem importantes produções acadêmicas/científicas


|Nome da Base|Descrição|Link de Acesso|
|---|---|---|
|SciELO|Reúne revistas acadêmicas de acesso aberto|[Site Oficial](https://scielo.org)|
|BDTD|Reúne teses e dissertações existentes nas instituições de ensino e pesquisa do Brasil|[Site Oficial](http://bdtd.ibict.br/vufind/)|
|Biblioteca Digital|Reúne alguns acervos digitais da UNESP|[Site Oficial](https://bibdig.biblioteca.unesp.br)|
|Parthenon/Athena|Reúne material bibliográfico da UNESP|[Site Oficial](https://unesp.primo.exlibrisgroup.com/discovery/search?vid=55UNESP_INST:UNESP)|
|JSTOR[^1]|Reúne revistas e livros acadêmicos|[Site Oficial](https://www.jstor.org)|
|Periódicos - CAPES[^1]|Reúne revistas acadêmicas assinadas pela CAPES|[Site Oficial](http://www-periodicos-capes-gov-br.ezl.periodicos.capes.gov.br)|
|ProQuest[^1]|Reúne material de ensino e pesquisa|[Site Oficial](https://www.proquest.com/hnpnewyorktimes/news/fromDatabasesLayer)|
|Scopus[^1]|Banco de dados de literatura acadêmica|[Site Oficial](https://www.scopus.com/)|
|SciMago[^1]|Reúne métricas sobre produção acadêmica|[Site Oficial](https://www.scimagoir.com/)|
|Web of Science[^1]|Reúne banco de dados de literatura acadêmica|[Site Oficial](https://clarivate.com/webofsciencegroup/solutions/web-of-science/)|


[^1]: Estes repositórios estão disponíveis apenas através do [VPN da UNESP](https://www2.unesp.br/portal#!/vpn-unesp)





