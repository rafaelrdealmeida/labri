---
id: intro
title: Apresentação Trilha de Dados
sidebar_label: Trilha de Dados
slug: /projetos/ensino/trilha-dados/intro
---

<center>
    <img src="/img/projetos/ensino/trilha-dados/trilha-dados.svg" alt="centered image" width="200" height="300" />
</center>

O projeto Trilha de Dados trata-se da disponibilização de curso básico de Ciência de Dados acessível a todos os estudantes de todos os níveis de conhecimento de programação, focado em aplicações nas Relações Internacionais. 