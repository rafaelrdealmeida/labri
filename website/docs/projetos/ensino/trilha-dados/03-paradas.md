---
id: paradas
title: Paradas da Trilha de Dados
sidebar_label: Paradas
slug: /projetos/ensino/trilha-dados/paradas
---


### Fundamentos de algoritmos e programação
- __*[Versionamento - GIT](/docs/projetos/ensino/versionamento/intro)*__

- __*[Linguagem de Programação - Python](/docs/projetos/ensino/linguagens/python/intro)*__
- __*[Linguagem de Programação - R](/docs/projetos/ensino/linguagens/r-lang/intro)*__

### Coleta de dados

- __*[WebScraping](/docs/projetos/ensino/trilha-dados/webscraping/intro)*__


### Métodos de análise

- __*[Métodos Quantitativos](/docs/projetos/ensino/trilha-dados/metodos-quanti/intro)*__

### Visualização de dados

- __*[Linguagem JS](/docs/projetos/ensino/linguagens/js/intro)*__

### Banco de dados

- __*[Banco de dados](/docs/projetos/ensino/linguagens/banco-dados/intro)*__
