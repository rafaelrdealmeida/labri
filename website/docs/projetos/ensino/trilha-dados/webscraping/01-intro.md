---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/ensino/trilha-dados/webscraping/intro
---















### Material de apoio

- [How to parse XML sitemaps using Python](https://practicaldatascience.co.uk/data-science/how-to-parse-xml-sitemaps-using-python)
- [Intro to Beautiful Soup](https://programminghistorian.org/en/lessons/intro-to-beautiful-soup)
- [Beautiful Soup: Build a Web Scraper With Python](https://realpython.com/beautiful-soup-web-scraper-python/)
- [How to scrape websites with Python and BeautifulSoup](https://www.freecodecamp.org/news/how-to-scrape-websites-with-python-and-beautifulsoup-5946935d93fe/)
- [Parsing tables and XML with Beautiful Soup 4](https://pythonprogramming.net/tables-xml-scraping-parsing-beautiful-soup-tutorial/)
- [Find all URLs of a website in a few seconds - Python](https://primates.dev/find-all-urls-of-a-website-in-a-few-seconds-python/)



