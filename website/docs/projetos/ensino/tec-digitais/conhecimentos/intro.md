---
id: intro
title: Conhecimentos Básicos
sidebar_label: Conhecimentos Básicos
slug: /projetos/ensino/tec-digitais/conhecimentos/intro
---

- Tutorial sobre **"Bits, bytes, kilos..."**

[![T1](https://i.imgur.com/jgkr1xw.png)](https://youtu.be/u4P0LOofEFs "T1")

- Tutorial sobre **"Velocidade de transferência de dados: unidades de medida"**

[![T2](https://i.imgur.com/clVuVqM.png)](https://youtu.be/X2adch0_WBY "T2")

- Tutorial sobre **"Dados pessoais: cópias de segurança"**

[![T3](https://i.imgur.com/9O9SwYC.png)](https://youtu.be/OK2u0WEKJCg "T3")

- Tutorial sobre **"Imagens e vídeos: pixel, resolução e proporção de imagens"**

[![T4](https://i.imgur.com/8IvWeQ5.png)](https://youtu.be/KJ7whRUOvNQM "T4")
