---
id: intro
title: Tecnologias para Anotações
sidebar_label: Tecnologias para Anotações
slug: /projetos/ensino/tec-digitais/anotacoes/intro
---

### Sugestões

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/gn9Vth3r5h4" frameborder="0" allowfullscreen="true"></iframe>
</figure>

### Documentação acadêmica: explicação sobre os apontamentos e o uso de softwares e serviços

[![Imgur](https://i.imgur.com/9gSE3rO.png)](https://youtu.be/gn9Vth3r5h4 "Teste")


### Mapas Mentais 

- **Freeplane**: Software de Mapa Mental baseado no Freemind

    - **Sistema**: Linux / Windows / Mac OS

    - **Custo**: Gratuito

    - **[Site oficial](https://www.freeplane.org/wiki/index.php/Home)**

- **MindMup**: Mapa Mental Online

    - **Sistema**: Linux / Windows / Mac OS

    - **Custo**: Gratuitos e Planos Pagos

    - **Exporta e importa arquivos .mm (Freemind)**

    - **Salva arquivos no Google Drive**

    - **[Site oficial](https://www.mindmup.com/)**

[![MindMup](https://i.imgur.com/35hN335.png)](https://youtu.be/gtSe4U58FJs "MindMup")

###  Exemplos de anotações realizadas 

- Anotações combinadas com um sistema de cores 

    - **Observação**: Anotação feita por uma estudante em arquivo de texto (PDF pesquisável) combinando marcas coloridas, elementos gráficos e apontamentos próprios nas últimas páginas do arquivo. As cores utilizadas seguem um padrão que foi definido pela estudante e atende suas necessidades.

    - **Software**: Leitor de PDF como o Adobe Reader, PDFxchange, Okular, etc.

[![Exemplo](https://i.imgur.com/BumXUDa.png)](https://youtu.be/TtW0m1Ie5ls "Exemplo")
