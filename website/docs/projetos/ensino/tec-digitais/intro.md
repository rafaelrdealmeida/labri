---
id: intro
title: Tecnologias Digitais
sidebar_label: Tecnologias Digitais
slug: /projetos/ensino/tec-digitais/intro
---

<center>
    <img src="/img/projetos/ensino/tec-digitais/logo-tec-digitais.svg" alt="centered image" width="250" height="200" />
</center>

Nesse espaço você encontrará tutoriais entre outros materiais didáticos que tem por objetivo disseminar a utilização de tecnologias digitais.

|Tecnologia|Link|
|---|---|
|Google Docs|[Playlist](https://www.youtube.com/playlist?list=PLJrDt7I-i2bykl-p0fn9WCd8JDSEsJuuW)|
|Mapas Mentais|[Playlist](https://www.youtube.com/playlist?list=PLJrDt7I-i2bxz0GtI3IA3WBI62a3jCOD0)|

Encontre tutoriais específicos em outras páginas do site LabRI

|Tecnologia|Link da Página|
|---|---|
|Conhecimentos Básicos|[Acesse aqui](/docs/projetos/ensino/tec-digitais/conhecimentos/intro)|
|Tecnologias para Anotações|[Acesse aqui](/docs/projetos/ensino/tec-digitais/anotacoes/intro)|
|Tecnologias para Edição de texto|[Acesse aqui](/docs/projetos/ensino/tec-digitais/edicao-texto/intro)|
|Pesquisa em Buscadores|[Acesse aqui](/docs/projetos/ensino/tec-digitais/buscadores/intro)|
|Áudio|[Acesse aqui](/docs/projetos/ensino/tec-digitais/audio/intro)|

