---
id: id-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/ensino/tec-digitais/id-visual
---

### Sobre o logo 

- **Fonte**: Better Together Spaced 
- **Cores**: #224C8D, #A6A6A6
- **_[Link (Canva)](https://www.canva.com/design/DAEfxiPDBOY/T2c2taR9E7kpjnIMK_xZ0A/view?utm_content=DAEfxiPDBOY&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton))_**