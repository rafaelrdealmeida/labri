---
id: iniciativas
title: Iniciativas
sidebar_label: Iniciativas
slug: /projetos/ensino/iniciativas
---

<center>
    <img src="/img/projetos/ensino/logo-projetos-ensino.svg" alt="centered image" width="300" height="150" />
</center>

- [Python](/docs/projetos/ensino/linguagens/python/intro)
- [Linguagem R](/docs/projetos/ensino/linguagens/r-lang/intro)
- [Versionamento - GIT](/docs/projetos/ensino/versionamento/intro)
- [Trilha de Dados](/docs/projetos/ensino/trilha-dados/intro)
- [Tecnologias Digitais](/docs/projetos/ensino/tec-digitais/intro)