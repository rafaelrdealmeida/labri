---
id: intro
title: Versionamento (GIT)
sidebar_label: Versionamento (GIT)
slug: /projetos/ensino/versionamento/intro
---


## O que é Git?

### Qual a diferença entre Git, Github e Gitlab?

### Versionamento e reprodutibilidade cientifica 

## Git: comandos básicos

| Comando | Funcionalidade | Descrição/Forma de usar |
| :-----: | :------------: | :----------------------:| 
| git config | definir usuário e email | necessário para submeter commits |
| git help | ajuda a entender comandos | git help ou git `comando` --help |
| git init | adicionar um projeto existente | - |
| git clone | clonar um repositório do gitlab/github | git clone `url` |
| git add | comando para iniciar submissão de commit | - |
| git commit | descreve o que será enviado | "git commit -m `descrição` |
| git pull | adiciona ao projeto novas modificações externas | "git pull origin" |
| git push | enviar o commit | "git push origin main" |
| git status | analisar status do arquivo | - |

 - Basico
   - config, init, add ., commit, pull, push
   - repositório na web (gitlab/github)
   - .gitignore
     - ambiente virtual (venv)



- Verificar estado atual dos arquivos monitorados pelo git

```
git status 

```

- Adicinar todos os arquivos no monitoramento do git

```

git add . 

```

- Salvando versionamento com comentário

```

git commit -m "inserir mensagem sobre o commit realizado" 

```

- Sincronizar arquivos remotos localizados no repositorio do gitlab com seus arquivos locais


```
git pull origin main

```

- Sincronizar arquivos locais com arquivos localizados no repositorio remoto

```

git push origin main

```

#### Git global steup

```
  git config --global user.name "Júlia Silveira'"
  git config --global user.email "7125949-rikamishiro@users.noreply.gitlab.com"
```

#### Create a new repository 

```
git clone git@gitlab.com:unesp-labri/sys-admin/gestao-dados-labri.git
cd gestao-dados-labri
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

#### Push an existing folder

```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:unesp-labri/sys-admin/gestao-dados-labri.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

#### Push an existing repository 

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:unesp-labri/sys-admin/gestao-dados-labri.git
git push -u origin --all
git push -u origin --tags
``` 

### Material de apoio

- [Oh Shit, Git!?!](https://ohshitgit.com/pt_BR)
- [Flight rules for Git](https://github.com/k88hudson/git-flight-rules)
- [Git Command Explorer](https://gitexplorer.com/)





