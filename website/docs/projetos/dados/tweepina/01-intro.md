---
id: intro
title: Apresentação TweePInA
sidebar_label: Apresentação
slug: /projetos/dados/tweepina/intro
---

<center>
    <img src="/img/projetos/dados/tweepina/tweepina.svg" alt="centered image" width="200" height="300" />
</center>


O Twitter é uma das principais redes sociais da atualidade, sendo muito utilizado por autoridades e instituições públicas que são objetos de estudo de várias pesquisas acadêmicas. Porém, ter acesso a série histórica de tweets dessas autoridades e instituições muitas vezes é difícil. Além disso, muitos tweets acabam sendo deletados, não estando disponíveis em arquivos e/ou repositórios públicos. Devido a isso, o objetivo geral desse projeto é reunir tweets de autoridades e instituições públicas com especial destaque ao Brasil e organismos internacionais. 

Mais especificamente, este projeto visa: 

- (1) auxiliar a construção de uma memória de informações de autoridades e instituições públicas divulgadas através do Twitter; 

- (2) subsidiar pesquisas acadêmicas que utilizam o Twitter como fonte de informação de seus objetos de estudo através da disponibilização das variáveis disponibilizadas pelo Twitter; 

- (3) indicar possibilidades e instrumentos que auxiliem uma análise mais detalhada dos tweets; 

- (4) fornecer um instrumento básico de análise de tweets

## Atividades Realizadas

|Data|Atividades Realizadas|
|---|---|
|13/08/2021|Migração do ambiente virtual `PyEnv` para o `Conda` e início do ajuste da função de update-profile|
|06/08/2021|Ajuste no timezone para UTC e início da construção da funcionalidade de atualização de informações de perfis já coletados|
|30/07/2021|Viabilizar adaptação de perfis já atualizados e habilitar coleta tanto pelo id quando pelo username|
|23/07/2021|Término do tratamento de erros; <br/> Início da estruturação da coleta de perfis que já se encontram na base de dados|
|16/07/2021|Tratamento de erro quando a API não retorna o autor_ID ou username|


