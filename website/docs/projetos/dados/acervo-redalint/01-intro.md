---
id: intro
title: Apresentação Acervo Redalint
sidebar_label: Apresentação
slug: /projetos/dados/acervo-redalint/intro
---

<center>
    <img src="/img/projetos/dados/acervo-redalint/redalint.svg" alt="centered image" />
</center>

A produção científica de acesso aberto sobre a internacionalização da educação superior na América Latina se encontra dispersa em variados portais, há certa inconsistência nos metadados destas publicações e a indexação integral deste material é rara. A partir deste diagnóstico, o Acervo REDALINT surgiu com o objetivo de reunir em uma plataforma a produção científica sobre esta temática fornecendo metadados consistentes e a indexação integral do conteúdo disponível.

|Data|Atividades Realizadas|
|---|---|
| - | - |
