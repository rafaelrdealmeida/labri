---
id: id-visual
title: Identidade Visual
sidebar_label: Identidade Visual
slug: /projetos/dados/newscloud/id-visual
---

### Sobre o Logo: 

* Fonte: Bantayog Regular
* Cores: #004455; #006680; #00AA88; #FFFFFF
* __*[Link (Canva)](https://www.canva.com/design/DAEOn9JDEko/UkG36icGx3N2m-czVdYiRA/view?utm_content=DAEOn9JDEko&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)*__