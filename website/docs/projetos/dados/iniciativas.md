---
id: iniciativas
title: Iniciativas
sidebar_label: Iniciativas
slug: /projetos/dados/iniciativas
---

<center>
    <img src="/img/projetos/dados/logo-projetos-dados.svg" alt="centered image" width="210" height="150" />
</center>

- [DiáriosBr](/docs/projetos/dados/diariosbr/intro)
- [NewsCloud](/docs/projetos/dados/newscloud/intro)
- [GovLatinAmerica](/docs/projetos/dados/gov-latin-america/intro)
- [TweePInA](/docs/projetos/dados/tweepina/intro)
- [Acervo Redalint](/docs/projetos/dados/acervo-redalint/intro)
- [Hemeroteca PEB](/docs/projetos/dados/hemeroteca-peb/intro)
- [IRJournalsBR](/docs/projetos/dados/irjournalsbr/intro)
- [MercoDocs](/docs/projetos/dados/mercodocs/intro)
- [Full Text](/docs/projetos/dados/full-text/intro)