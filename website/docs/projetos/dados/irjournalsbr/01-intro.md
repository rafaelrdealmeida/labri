---
id: intro
title: Apresentação IRJournalsBR
sidebar_label: Apresentação
slug: /projetos/dados/irjournalsbr/intro
---

<center>
    <img src="/img/projetos/dados/irjournalsbr/irjournalsbr.svg" alt="centered image" width="200" height="300" />
</center>


As revistas acadêmicas brasileiras de Relações Internacionais seguem as políticas de acesso aberto. Com isso, todo o conhecimento divulgado por estas revistas podem ser acessados gratuitamente por qualquer pessoa interessada e podem ser reutilizados sem prévia autorização dos editores e autores, desde que seja respeitada a licença de uso do Creative Commons adotado pelos respectivos periódicos. Apesar da adoção do acesso aberto ser um aspecto importante para garantir acesso universal ao conhecimento científico, parte dos metadados destas revistas apresentam inconsistências. Ademais, a indexação integral do conteúdo dessas revistas não é disponibilizada publicamente, dificultando a busca por palavras chaves na integralidade dos arquivos. A partir do exposto acima, o projeto IRjornalsBR objetiva formar uma base de dados que aglutine tanto metadados mais consistentes como também forneça uma indexação integral destas revistas.

## ATIVIDADES REALIZADAS

|DATA| Atividade realizada|
|---|--|
|09/08/2021| coleta metadados contexto internacional; coleta pdf RBPI (falta metadados); <br/> resolvendo nomeação arquivos pdf de publicações continuas |
