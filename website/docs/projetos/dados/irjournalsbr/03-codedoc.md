---
id: codedoc
title: Documentação IRJournalsBR
sidebar_label: Documentação
slug: /projetos/dados/irjournalsbr/codedoc
---


### Repositórios

- https://github.com/pedrodrocha/irjournalsbr
   - metadados
- https://github.com/pedrodrocha/irarticlesbr
  - pdf

- https://github.com/pedrodrocha/irindexbr




### Caminho relativo das revistas

```
/007/002/002/

```


### Codigo de cada revista


|Código           | Nome da Revista               |
|-----------------|-------------------------------|
| 007/002/002/001 | Boletim EPI Unicamp
| 007/002/002/002 | Revista Cena Internacional
| 007/002/002/003 |	Revista Contexto Internacional
| 007/002/002/004 |	Revista Meridiano 47
| 007/002/002/005 |	RBCE
| 007/002/002/006 |	RBCS
| 007/002/002/007 |	RBPI
| 007/002/002/008 | Revista de la Cepal
| 007/002/002/009 |	Revista DEP
| 007/002/002/010 |	Revista Política Externa
| 007/002/002/011 |	Revista Via Mundi
| 007/002/002/012	| Revista Estudos Internacionais
| 007/002/002/013	| Revista Carta Internacional
| 007/002/002/014	| Revista Monções - Revista de Relações Internacionais da UFGD
| 007/002/002/015	| Revista Mural Internacional
| 007/002/002/016	| Revista Austral
| 007/002/002/017	| Revista Brazilian Journal of International Relations
| 007/002/002/018	| Revista Conjuntura Global
| 007/002/002/019	| Revista Conjuntura Internacional
| 007/002/002/020	| Revista Conjuntura Austral
| 007/002/002/021	| Revista OIkos
| 007/002/002/022	| Revista Brasileira de Estudos de Defesa
| 007/002/002/023	| Brazilian Journal of Latin American Studies


### Pastas com dados e metadados

|Pasta        |conteúdo                                           |Estrutura| Exemplo |
|-------------|---------------------------------------------------|---------|---------|
|pdf          | arquivos PDF de todos os artigos                  |`pdf/ano/nome_arquivo` | `pdf/2003/2003-v.2-n.2-02.pdf`|
|xml          | xml gerados, via grobid, a partir dos pdfs        |`xml/ano/nome_arquivo` |`xml/2003/2003-v.2-n.2-02.tei.xml`|
|metadados    | cada linha contem metadados de um artigo da edição|`metadados/nome_arquivo` |`metadados/2013v01n01.csv`|
|csv          | cada arquivo csv contem dados das referências citadas em cada artigo das revistas|`csv/ano/nome_arquivo`| `/2003/2003-v.2-n.2-02.csv`|
|logs         | info de controle da coleta dos pdfs               |`logs/.logs.csv` , `logs/acervo.csv` , `info_data`|
|logs_metadata | info de controle de coleta dos metadados         |`logs_metadata/.logs.csv`, `acervo.csv` |



### Arquivos do controle


|nome        |função                             |    variáveis                           |
|------------|-----------------------------------|----------------------------------------|
|`.logs.csv` |armanezar histórico da coleta      |`date`, `houve_coleta`(`TRUE`), `coleta`|
|`acervo.csv`|refere-se cada edição              |`url` de determinada edição, `editions`, `vol`, `n`, `ano`, `revista`|
|`info_data` |tamanho e localização de cada pdf  |`loc_arquivo` local do pdf, `pdf_url` localização web, `size` tamanho|



###  Metadados das referencias citadas em cada artigo das revistas


|variável               | significado|
|-----------------------|------------|
|`cited_in_journal`     |Revista em que a referencia foi citada|
|`cited_in_journal_year`|ano em que a referencia foi citada na revista|
|`references_raw`       |referência completa(crua)|
|`references_date`      |data da referência|
|`references_author`    |autores da referencias|
|`title_monogr`         |titulo|
|`title_analytic`       |titulo|
|`path_pdf`             |local do pdf|
|`path_xml`             |local do xml|


### Metadados de cada artigo das revistas

A definição dos nomes das variáveis dos metadados segue os padrões estabelecidos pelo "Rótulos de campo da Principal Coleção do Web of Science" [clique aqui](https://archive.is/5stfq) para mais detalhes

|variável| significado|
|--------|------------|
|AU|autor|
|OG|instituição|
|TI|titulo|
|AB|resumo|
|DE|palavras chaves|
|CR|referencias citadas|
|BP_EP|pagina inicial e pagina final|
|PY|ano|
|IS|issue|
|LA|lingua|
|DI|Identificador de Objeto Digital (DOI)|
|SO|nome revista|
|SN|ISSN da revista|
|URL|endereço web da pagina html|
|PDFURL|endereço web do pdf|


:::info

O `info_data` contém informações que possibilitam vincular os arquivos pdf, csv (com referencias) , csv (com metadados)

:::


- `loc_arquivo` (de `logs/info_data.csv`) e `path_pdf` (de `csv/ano/nome_arquivo.csv` ) vinculam os arquivos pdf ao csv das referencias

- `pdf_url` (de `logs/info_data.csv`) e `PDFURL` (de `metadados/nome_arquivo.csv`) vinculam o metadados com os  arquivos pdf





