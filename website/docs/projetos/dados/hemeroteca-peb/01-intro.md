---
id: intro
title: Apresentação Hemeroteca PEB
sidebar_label: Apresentação
slug: /projetos/dados/hemeroteca-peb/intro
---

A Hemeroteca de Política Externa Brasileira contém uma seleção de matérias publicadas, no período de 1972 a 2010, por alguns jornais brasileiros dentre os quais se destacam: O Estado de S. Paulo, Folha de S. Paulo e Gazeta Mercantil. O objetivo desta Hemeroteca é permitir aos pesquisadores interessados o acesso a notícias que foram selecionadas e classificadas ao longo dos anos, de 1972 até 2010, sobre importantes acontecimentos atinentes às relações internacionais do Brasil.


|Data|Atividades Realizadas|
|---|---|
| - | - |
