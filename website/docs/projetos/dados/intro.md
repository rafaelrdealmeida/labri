---
id: intro
title: Apresentação
sidebar_label: Apresentação
slug: /projetos/dados/intro
---

<center>
    <img src="/img/projetos/dados/logo-projetos-dados.svg" alt="centered image" width="210" height="150" />
</center>

Os projetos de dados são iniciativas que visam coletar, tratar, analisar e, quando possível, disponibilizar dados relevantes para as pesquisas em Relações Internacionais. Deste modo, neste espaço é possível encontrar projetos voltados à incorporação de tecnologias digitais para melhorar o manuseio da crescente quantidade de dados disponíveis nas redes sociais, nos meios de comunicação, nas instâncias governamentais e organismos internacionais.