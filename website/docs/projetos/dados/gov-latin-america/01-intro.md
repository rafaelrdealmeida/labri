---
id: intro
title: Apresentação Governo LatinAmerica
sidebar_label: GovLatinAmerica
slug: /projetos/dados/gov-latin-america/intro
---

<center>
    <img src="/img/projetos/dados/gov-latam/gov-latam.svg" alt="centered image" width="200" height="300" />
</center>

Os dados públicos dos órgãos governamentais latino americanos disponíveis via web com frequência são retirados dos sites oficiais, especialmente, após a passagem de um mandato presidencial para outro. A partir deste diagnóstico, o objetivo do projeto GovLatinAmerica é coletar tais dados para que possam ser utilizados em pesquisas acadêmicas diversas.

|Data|Atividades Realizadas|
|---|---|
|12/08/2021|Término do mapeamento e início da coleta no site do Planalto|
|05/08/2021|Estruturação do mapeamento e início da estruturação do fluxograma da coleta e tratamento dos dados.|
|29/07/2021|Ajustes BeautifulSoup e extração das informações do xml do sidemap|
|22/07/2021|Configuração do ambiente virtual Conda e do VSCode|
