---
id: intro
title: Apresentação DiáriosBR
sidebar_label: Apresentação
slug: /projetos/dados/diariosbr/intro
---

<center>
    <img src="/img/projetos/dados/diariosbr/diariosbr.svg" alt="centered image" width="200" height="300" />
</center>

Os Diários Oficiais são onde toda a movimentação legal dos governos federal, estadual e municipal são publicadas, sendo esse fator o que os tornam uma fonte de pesquisa importante para o acompanhamento da destinação de recursos, transferência de cargos e o embasamento legal das atividades da administração pública. 

## Atividades Realizadas

|Data|Atividades Realizadas|Presentes|
|----------|---------------|---------|
|27/10/2021|Orlândia - Coleta dos dados resolvida; <br>lidando com a falta de padronização dos nomes dos arquivos|Vitório, Rafael|
|20/10/2021|Continuidade da coleta de São Joaquim da Barra|Júlia, Rafael|
|06/10/2021|Início da coleta DO Legislativo do Estado de São Paulo (anterior à 06/2017)|
|29/09/2021|Início da coleta de Orlândia|
|22/09/2021|Início da coleta de Ituverava|
|15/09/2021|Revisão do banco de termos|
|08/09/2021|Início da coleta de São Joaquim da Barra|
|25/08/2021|Instalação das bibliotecas de Python para a realização da coleta e instruções iniciais|
|18/08/2021|criação do documento de "como utilizar" no site aberto e criação do ambiente virtual conda|
|28/07/2021|entendimento da indexação do recoll|
|21/07/2021|organização da documentação e ínicio do entendimento de indexação do recoll|

