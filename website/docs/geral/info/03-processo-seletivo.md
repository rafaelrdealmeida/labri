---
id: processo-seletivo
title: Informações sobre o processo seletivo
sidebar_label: Processo Seletivo
slug: /geral/info/processo-seletivo
---

## Processo Seletivo 2021

**Inscrições abertas para estágio no LabRI**

:::tip EDITAL DA CHAMADA ESTÁGIO REMUNERADO LabRI/UNESP - 2021
[CLIQUE NESTE LINK PARA LER O EDITAL DA CHAMADA](https://docs.google.com/document/d/1zyil-GuzABcZwkEaDRYwKjxk6QFZzwDcqHu_b-TEGUY/edit?usp=sharing)
:::

Data de inscrição: **26/05/2021** até **07/06/2021**

Carga Horária: **30h/semanais**

Remuneração mensal: **R$550,00**

Período do estágio: **validade inicial de um (1) ano a partir da data de contratação**

Formulário de inscrição: **[CLIQUE AQUI](https://forms.gle/QsjgSMAsttB9mirf7)**

Tendo em vista que um dos principais objetivos do Laboratório de Relações Internacionais é o aprimoramento das atividades desenvolvidas no curso de Relações Internacionais através da incorporação do uso de novas tecnologias da informação e comunicação nas atividades de pesquisa, extensão e docência, as atividades dos interessados em estagiar no Laboratório estarão relacionadas ao aprendizado, operação, auxílio, suporte e disseminação destes diversos conhecimentos, que poderão ser aplicados na área de atuação profissional nas Relações Internacionais a que o estagiário se dedique futuramente. 

### Resultado do Processo Seletivo 2021

A partir dos critérios estabelecidos no edital da chamada, segue abaixo a classificação do processo seletivo do estágio remunerado 2021 do LabRI/UNESP.


| Classificação | Participantes	| Experiência/ Relato formulário | Participação LabRI | Atividade | TOTAL
| --- | --- | --- | --- | --- | --- |
| 1 | Fábio de Oliveira Paron | 10 | 0 | 50 | 60 |
| 2 | Cíntia Paulena Di Iório | 20 | 0 | 35 | 55 |


Preencha o Cadastro de novo integrante do LabRI/UNESP
O 1º classificado deverá manifestar interesse em assumir a função até as **15 horas do dia 10/06/2021**, entrando em contato com o LabRI/DERI via e-mail: *unesplabri@gmail.com*  - conforme edital.
A não manifestação de interesse implicará na convocação do próximo classificado.
Deverá  também enviar, para os e-mails *unesplabri@gmail.com* e *deri.franca@unesp.br* as seguintes informações: Nome completo, endereço completo, número do PIS (se possuir), RG, CPF, e-mail e telefone.
Deverá entrar em contato com a Seção Técnica de Saúde - STS, da FCHS através do e-mail *sts.franca@unesp.br* para informações sobre exame admissional.
Deverá abrir conta no Banco do Brasil (se não possuir) e informar o número da conta corrente, agência e cidade
Deverá assinar termo de estágio junto ao CIEE (via e-mail). Para isso é necessário enviar RG, CPF, comprovante de residência e declaração escolar para o email: atende.*franca@ciee.ong.br* 
