---
id: atendimento
title: Atendimento e Suporte aos docentes e discentes
sidebar_label: Atendimento
slug: /atendimento
---

#### Durante a pandemia o Labri realiza suas atividades de forma remota. Veja abaixo as maneiras de entrar em contato com a equipe do LabRI/UNESP:  

 - Preferencialmente pedimos que o contato seja realizado: 
    - através do e-mail (unesplabri@gmail.com) ou  
    - entrando em nossa [sala virtual do Discord](https://discord.com/channels/832235168689029180/832235168689029183)

 - Também é possível entrar em contato através das Redes Sociais do LabRI/UNESP (pode ser que a resposta demore mais para ser feita)
    - [Facebook](https://www.facebook.com/labriunesp)
    - [Twitter](https://twitter.com/labriunesp)
    - [Instagram](https://www.instagram.com/unesplabri/)
    - [LinkedIn](https://www.linkedin.com/company/labri-unesp-franca/)
    - [Youtube](https://www.youtube.com/channel/UCHx_m-4Cv7_ZLEDUe_wYATA/featured)
  
 - Marcar um horário para atendimento na [sala virtual](https://meet.google.com/poe-qsad-zvs) do Google Meet

:::info

Nos horários indicados abaixo, os colaboradores estão disponíveis na sala virtual do Discord.

link da sala no discord: [CLIQUE AQUI](https://discord.com/channels/832235168689029180/832235168689029183)

:::

|                                    | Segunda       | Terça         | Quarta        | Quinta        | Sexta
| ---------------------------------- | ------------  | ------------- | ------------  | ------------- | -------
| Funcionamento                      | 09h00-21h00\* | 09h00-21h00\* | 09h00-21h00\* | 09h00-21h00\* | 09h00-21h00\*
| Atendimento (JULIA/RAFAEL/FÁBIO)   | 08h30-12h30   | 08h30-12h30   | 08h30-12h30   | 08h30-12h30   | 08h30-12h30 
| Atendimento (JULIA/RAFAEL/FÁBIO)   | 14h-16h       | 14h-16h       | 14h-16h       | 14h-16h       | 14h-16h 

 \* Horário disponível para aqueles que possuem acesso direto ao laboratório. Aqueles que não possuem acesso direto ao laboratório devem seguir o horário de atendimento dos estagiários (Funcionamento suspenso devido a pandemia).

### Reuniões de Trabalho em Torno de Projetos Integrados

- Para saber mais sobre os Projetos Integrados, clique [Aqui](https://labriunesp.org/projetos/)

| atividade/Projetos            | Segunda       | Terça       | Quarta      | Quinta      | Sexta
| ----------------------------- | ------------  | ----------- | ----------  | ----------- | -------
| MercoDocs(Projeto)            |       -       | 10h30-12h30 |      -      |      -      |      -     
| NewsCloud (Projeto)           |  10h30-12h30  |      -      |      -      |      -      |      -     
| GovLatinAmerica (Projeto)     |       -       |      -      |      -      | 10h30-12h30 |      -
| Tweepina (Projeto)            |       -       |      -      |      -      |      -      | 10h30-12h30 

### Tipos de Reuniões 

- Reuniões de planejamento
    - São encontros em que são repassados informes gerais, são definidas as próximas tarefas de cada colaborador, e ocorre uma revisão da dinâmica do LabRI/UNESP. Normalmente estas Reuniões contam com a presença do coordenador do LabRI/UNESP. 

- Reuniões de trabalho 
    - São encontros em que desenvolvemos alguma atividade específica. Tal atividade pode ser relacionada a algum projeto integrado específico, ao apoio a algum grupo de pesquisa, docente ou discente, ou ao auxílio na manutenção da infraestrutura disponível no LabRI/UNESP.
    - Em grande medida, tais reuniões têm um forte vínculo com algum subprojeto dentre os projetos integrados.

- Reuniões de treinamento 
    - São encontros em que os colaboradores são introduzidos ao funcionamento geral do LabRI/UNESP, à dinâmica de algum projeto integrado específico ou é realizado alguma oficina voltada ao aprendizado de alguma tecnologia digital.






