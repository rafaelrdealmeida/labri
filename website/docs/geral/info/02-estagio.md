---
id: estagio
title: Oportunidade de Estágio
sidebar_label: Estágio
slug: /geral/info/estagio
---

### Oportunidades de Estágio

:::danger ESTAMOS COM PROCESSO SELETIVO PARA ESTAGIO REMUNERADO ABERTO
[PARA MAIS DETALHES CLIQUE NESTE LINK](/docs/geral/info/processo-seletivo)
:::

O Laboratório de Relações Internacionais oferece um Estágio Remunerado anualmente via edital, o qual será divulgado em nosso site e também no site do [Departamento de Relações Internacionais (DERI)](https://www.franca.unesp.br/#!/selecaoestagiariolabri) da Unesp-Franca.

Também incentivamos o Estágio Voluntário. Acreditamos que essa oportunidade tem grandes qualidades de agregar conhecimento pessoal e profissional para quem está em busca de novos competências e habilidades. Esta forma de estágio poderá ser desenvolvida de duas maneiras: a primeira nos moldes das atividades desenvolvidas pelo estagiário remunerado; a segunda é o desenvolvimento de um conjunto de atividades visando um projeto específico.

Os estagiários voluntários que quiserem concorrer ao Estágio Remunerado terão um bônus de pontuação ao concorrerem ao edital (os detalhes serão apresentados no mesmo).

Mais informações sobre o Estágio no LabRI, entre em contato conosco pelo nosso e-mail: unesplabri@gmail.com

### Editais Anteriores

No link a seguir, você pode consultar os editais encerrados para oportunidades de estágio no LabRI:

#### Seleção 2018:
[Edital 2018](https://docs.google.com/document/d/1UyHQ99sERyZghOUYx7J044-JGpanOZigBkRgytu0g3I/edit)

[Resultado final - 2018](https://docs.google.com/document/d/1xYcD2d6Q6JWuTiUlDeCLxjzC0Aufw63Tu9hNA32Mo54/edit)

#### Seleção 2016:
[Edital 2016](https://docs.google.com/document/d/1xJBvAqOrvjzNR18bonDaMbY2K0GG5N7tLiawg2tdjSI/edit)

[Resultado final - 2016](https://docs.google.com/document/d/1X33x9Go0ZDUp6fvV8kpeO3erNNEnJ-fgJeNAR4VVTmg/edit)
