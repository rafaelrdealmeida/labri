const isBootstrapPreset = process.env.DOCUSAURUS_PRESET === 'bootstrap'; //add
module.exports = {
  title: 'LabRI-UNESP',
  tagline: 'Laboratório de Relações Internacionais - UNESP',
  url: 'https://labriunesp.org',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  favicon: './img/icone-favicon.ico',
  organizationName: 'labriunesp', // Usually your GitHub org/user name.
  projectName: 'site', // Usually your repo name.
  themeConfig: {
    hideableSidebar: true, // add
    colorMode: { // add
      defaultMode: 'light',
      disableSwitch: false,
      respectPrefersColorScheme: true,
    },
    navbar: {
      hideOnScroll: true, // add
      title: 'Home',
      logo: {
        alt: ' Logo LabRI UNESP',
        src: 'img/labriunesp-12.svg',
        srcDark: 'img/labriunesp-12.svg',
      },
      items: [
        {
          to: 'projetos/',
          activeBasePath: 'projetos',
          label: 'Projetos',
          position: 'right',
        },
        //{to: 'blog', label: 'Blog', position: 'right'}, //nadd
        {to: 'cadernos/', label: 'Publicações', position: 'right', id: 'second-blog'},
        {to: 'noticias/', label: 'Noticias', position: 'right', id: 'third-blog'},
        {
          to: 'docs/atendimento/',
          activeBasePath: 'atendimento',
          label: 'Atendimento',
          position: 'right',
        },
        //{
          //to: 'docs/',
          //activeBasePath: 'docs',
          //label: 'Docs',
          //position: 'right',
        //},
        {
          href: 'https://gitlab.com/unesp-labri',
          label: 'Gitlab',
          position: 'right',
          className: 'header-gitlab-link',
          'aria-label': 'GitLab repository'
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        //{
        //  title: 'Docs',
          //items: [
            //{
            //  label: 'Style Guide',
            //  to: 'docs/',
          //  },
          //  {
            //  label: 'Second Doc',
            //  to: 'docs/doc2/',
            //},
          //],
        //},
        {
          title: 'Redes Sociais',
          items: [
            {
              label: 'Youtube',
              href: 'https://www.youtube.com/channel/UCHx_m-4Cv7_ZLEDUe_wYATA/featured',
            },
            {
              label: 'Facebook',
              href: 'https://www.facebook.com/labriunesp',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/labriunesp',
            },
            {
              label: 'Instagram',
              href: 'https://www.instagram.com/unesplabri/',
            },
            {
              label: 'Linkedin',
              href: 'https://www.linkedin.com/company/labri-unesp-franca/',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Notícias',to: 'noticias',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/unesp-labri',
            },
          ],
        },
      ],
      //copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
      copyright: `${new Date().getFullYear()} Site feito e mantido pelos membros do LabRI/UNESP. Construído com Docusaurus.`,
    },
  },
  presets: [
    [
      isBootstrapPreset // add
         ? '@docusaurus/preset-bootstrap' // add
         : '@docusaurus/preset-classic', // add
      {
        debug: true, // force debug plugin usage // add
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website/',
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,

        },
        blog: {
          showReadingTime: true,
          //postsPerPage: 3,
          blogSidebarCount: 2,
          blogSidebarTitle: 'Posts recentes',
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website/blog',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    //['docusaurus-plugin-sass'],
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'second-blog',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: 'cadernos',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: 'cadernos',
        showReadingTime: true,
        //postsPerPage: 3,
        blogSidebarCount: 0,
        //blogSidebarTitle: 'Posts recentes',
        // Please change this to your repo.
        editUrl:
          'https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website',
      },
    ],
    [
      '@docusaurus/plugin-content-blog',
      {
        /**
         * Required for any multi-instance plugin
         */
        id: 'third-blog',
        /**
         * URL route for the blog section of your site.
         * *DO NOT* include a trailing slash.
         */
        routeBasePath: 'noticias',
        /**
         * Path to data on filesystem relative to site dir.
         */
        path: 'noticias',
        showReadingTime: true,
        //postsPerPage: 3,
        //blogSidebarCount: 1,
        //blogSidebarTitle: 'Posts recentes',
        // Please change this to your repo.
        editUrl:
          'https://gitlab.com/unesp-labri/sites/labri/-/tree/main/website',
      },
    ],
    // easyops-cn/docusaurus-search-local
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        // For Docs using Chinese, The `language` is recommended to set to:

        language: ["en", "pt"],
        indexPages: true,
        searchResultLimits: 15,
        searchResultContextMaxLength: 250,
        // When applying `zh` in language, please install `nodejieba` in your project.
      },
    ],
    [
      '@docusaurus/plugin-ideal-image',
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
      },
    ],
  ],
  //themes: ['@docusaurus/theme-bootstrap'],
};
