---
slug: labri-n0014
tags: [COVID-19, CADERNOS]
title: "O que é o “passaporte de imunidade”?"
author: Isabel do Nascimento Gonçalves e Matheus Valencia
author_title: Graduandos em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 14
image_url: https://i.imgur.com/2Hu99Q7.png
descricao: A crise promovida pelo novo coronavírus prevê para os próximos anos intensas mudanças no que estudamos e compreendemos por economia. Novas regras nas relações comerciais entre países deverão ser instauradas...
download_pdf: /arquivos/2020-05-19-labri-014.pdf
serie: COVID-19

---



A crise promovida pelo novo coronavírus prevê para os próximos anos intensas mudanças no que estudamos e compreendemos por economia. Novas regras nas relações comerciais entre países deverão ser instauradas, hábitos de consumo da população sofrerão alterações irreversíveis e o peso do Estado frente ao mercado colocará em exercício a força da máquina estatal ao responder o suplício de empresas flertando com a falência. Grandes choques econômicos vivenciados pela humanidade ao decorrer da história resultaram, não só em uma herança de mágoas, mas também de mudanças, e empresários descrentes procuram hoje amenizar a iminente degeneração de suas riquezas indo de encontro a soluções duvidosas que transparecem a irresponsabilidade humana.

![](https://i.imgur.com/2Hu99Q7.png)
<figcaption>Fonte: https://passageirodeprimeira.com/passaporte-da-imunidade-sera-que-precisaremos-de-um-novo-documento-para-viajar/_</figcaption>

Inviabilizado pela Organização Mundial da Saúde (OMS) , o “passaporte de imunidade” é um conceito historicamente propagado que, em solo brasileiro, instalou-se durante os surtos de Febre Amarela ocorridos no século XIX, de acordo com o doutor em virologia, Atila Iamarino. Escravos e imigrantes infectados recebiam status de imunidade assim que se recuperavam da doença, e seus donos, afetados pelo mau andamento do engenho, obrigava-os a retornarem às condições insalubres do trabalho forçado. A mesma falta de conhecimento que, há duzentos anos, provocou o surgimento de inúmeros surtos por toda a costa brasileira e a morte de mais de 10 mil indivíduos, encaminha hoje a humanidade para um encontro com o passado.

Numa tentativa inerente às políticas de isolamento, o Centro Alemão de Pesquisa de Infecções realizou um estudo com mais de 100 mil voluntários com o intuito de registrar a recuperação de pacientes já infectados. A ideia focava em projetar cenários em que voluntários analisados, caso recuperados, receberiam a permissão de retornar a rotina de trabalho, realizando funções cotidianas em suas respectivas empresas. Jonathan Ashworth, membro do parlamento inglês, ao apoiar a intenção alemã disse estar disposto a aprender com a abordagem tomada pelo país, ressaltando a importância da realização de testes e do rastreamento dos infectados. Todavia, ressurgindo como uma alternativa promissora, o passaporte de imunidade encontra obstáculos científicos e, principalmente, éticos em sua execução.



![](https://i.imgur.com/9wvBut9.jpg)

<figcaption>Fonte: https://www.nsctotal.com.br/colunistas/estela-benetti/coronavirus-passaporte-de-imunidade-testes-em-massa-mascaras-planos-para_</figcaption>



A corrida iniciada por insumos médicos em todo mundo revelou não só a falta de leitos hospitalares como também a escassez de testes rápidos para detecção do vírus. O Ministério da Saúde estimou em março que os casos confirmados em nosso país equivalem a menos de 15% da existência verídica de pessoas contaminadas no país, portanto, os dados hoje revelam um cenário falso e muito distante da realidade. Tamanha sub notificação clama pela necessidade de testagens recorrentes na população, o exercício de gerenciar o aumento de casos permite ao Governo Federal instaurar novas medidas públicas de contenção e impedir o surgimento de novos surtos remotos. Porém, esse parece ser um cenário distante no contexto brasileiro atual, visto que as incertezas sobre os dados disponibilizados acarretam em dúvidas a respeito da permanência do isolamento e o afastamento dos postos de trabalho.

O dilema do passaporte enfrenta, dessa forma, sua inviabilidade a partir da carência de recursos hospitalares. Unido a isso, a concepção de que os contaminados por uma doença, como a COVID-19, uma vez curados, estariam imunes à enfermidade podendo circular livremente é tão atraente como inventiva. Um estudo conduzido pelo Centro de Controle e Preservação de Doenças (CDC) apontou que indivíduos contaminados pelo Sars-CoV-1, um vírus análogo ao causador atual da pandemia, revelaram uma imunidade estimada em apenas três anos após recuperação efetiva. Atualmente, cientistas de todo o mundo conduzem estudos para entender como nosso sistema imunológico responde à doença e, utilizando o princípio da precaução, afirmam que a incerteza não deve promover ações precipitadas que colocariam em risco a vida dos trabalhadores.

Conforme dito anteriormente, o conceito de passaporte de imunidade é cercado de diversos problemas que vão além de sua efetividade. Uma das questões que deve ser levada em consideração é a precisão dos testes que dizem se uma pessoa produz anticorpos suficientes. Além disso, um artigo publicado por dois cientistas americanos demonstra os riscos enfrentados pelo sistema de saúde do país que adotar essa medida, já que não só há a possibilidade de pessoas adquirirem esses passaportes sem possuírem a imunidade necessária, como também pode encorajar indivíduos a se contaminarem para adquirirem o mesmo, ambos os fatores podem acarretar no colapso do sistema de saúde das nações emissoras e a consecutiva falta de testes.





![](https://i.imgur.com/SkSEEfR.jpg)

<figcaption>Fonte: https://www.jb.com.br/ciencia_e_tec/2020/03/1022799-medicos-descrevem-graves-consequencias-do-coronavirus-em-pacientes-curados.html_</figcaption>



Mesmo com diversas evidências contra o conceito de passaporte de imunidade, diversos países o veem como a salvação de sua economia. Um exemplo foi o de Jaime Mañalich, Ministro da Saúde do Chile, que afirmou no dia 9 de abril que pacientes recuperados receberiam um cartão com permissão para circulação em áreas que estão em quarentena obrigatória. Porém, no dia 29 de abril recuou na ideia afirmando que pessoas recuperadas não irão receber, por enquanto, nenhum documento que permita a livre circulação, demonstrando uma importante consciência frente a esses estudos

No Brasil, a instável situação do passaporte também é alvo de discussões. Paulo Guedes, Ministro da Economia, afirmou em março que o país passaria por duas fases da crise decorrentes do novo Coronavírus, uma que afetaria a saúde brasileira e outra que impactaria a economia. Dizendo acreditar que o passaporte da imunidade é uma resolução para essa segunda fase, Guedes sugeriu efetuar testes em massa para identificar quem havia desenvolvido imunidade a doença.

Uma startup norte-americana, chamada  _Privacy Tools_, anunciou o lançamento de uma plataforma a qual permite que pessoas recuperadas candidatem-se à entrega dos passaportes. A ferramenta, planejada e construída dentro do sistema  _blockchain_, facilitaria a fiscalização e a concessão de permissões. Empresas de tecnologia por todo mundo buscam conciliar suas ferramentas à necessidade de multinacionais que no cenário atual enfrentam uma profunda queda em suas produções e serviços. Tanto a  _Privacy Tools_  quanto a  _Data Machina_  e a  _Cappta_, outras empresas do ramo, representam o interesse privado no combate ao recesso mundial promovido pelo novo coronavírus.



![](https://i.imgur.com/5F7zOqr.jpg)

<figcaption>Fonte:https://cointelegraph.com.br/news/south-korean-hospital-to-create-blockchain-medical-data-management-platform_</figcaption>



O futuro, entretanto, é incerto. Sem projeções concretas para os próximos meses líderes políticos, multinacionais e investidores por todo o globo encontram-se vendados em um terreno hostil. Iniciativas, como a do passaporte de imunidade, nascem com a intenção de trazer a esses agentes a capacidade de enxergar. Porém, não obstante, divergindo dos parâmetros de saúde e encontrando objeções morais tais iniciativas assemelham-se à uma narrativa incisiva na história: encontrar saídas lucrativas onde, eticamente, não existe proveito à ser extraído.



<article></article>
### Referências Bibliográficas

ALFANO, Bruno. **O "passaporte da imunidade" vale para o novo coronavírus?**. Época. 2020. Disponível em: https://epoca.globo.com/sociedade/o-passaporte-da-imunidade-vale-para-novo-coronavirus-24376948. Acesso em: 30 abr. 2020.

BOSCOLI, Claudia Zucare. **A esperança do "passaporte da imunidade"**. Eu Quero Investir. 2020. Disponível em: https://www.euqueroinvestir.com/a-esperanca-do-passaporte-da-imunidade/. Acesso em: 30 abr. 2020.

GUSSON, Cássio. **Startup brasileira quer usar blockchain para criar 'Passaporte da Imunidade' e acabar com o #fiqueemcasa**. Telegraph Coin. 2020. Disponível em: https://cointelegraph.com.br/news/brazilian-startup-wants-to-use-blockchain-to-create-immunity-passport-and-put-an-end-to-stayhome. Acesso em: 02 maio 2020.

IAMARINO, Atila. **Mais um passaporte de imunidade.** Folha de S. Paulo. 2020. Disponível em: https://www 1.folha.uol.com.br/colunas/atila-iamarino/2020/04/mais-um-passaporte-de-imunidade.shtml. Acesso em: 29 abr. 2020.

RESENDE, Thiago; MACHADO, Renato. **Contra coronavírus, Guedes defende 'passaporte de imunidade' para retomada da economia**. Folha de S. Paulo. 2020. Disponível em: https://www1.folha.uol.com.br/mercado/2020/04/contra-coronavirus-guedes-defende-passaporte-de-imunidade-para-retomada-da-economia.shtml. Acesso em: 01 maio 2020.

VEGA, Miguel Ángel GarcÍa. **Como será a economia após o coronavírus**. El País. 2020. Disponível em: https://brasil.elpais.com/economia/2020-04-13/como-sera-a-economia-apos-o-coronavirus.html. Acesso em: 29 abr. 2020.
