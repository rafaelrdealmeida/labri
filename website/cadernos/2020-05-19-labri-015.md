---
slug: labri-n0015
tags: [COVID-19, CADERNOS]
title: "Os Fatores Determinantes para o Sucesso da Coreia do Sul no Combate ao COVID- 19"
author: Bianca Morales, Gabriela Alencar, Rodrigo Toloi e Pedro Henrique Campagna
author_title: Graduandos em Relações Internacionais
tipo_publicacao: Cadernos LabRI/UNESP - Núm. 15
image_url: https://i.imgur.com/WBQqErL.png
descricao: O novo coronavírus surpreendeu todo o mundo com seu advento repentino. Nenhuma nação pôde se preparar para enfrentar suas consequências, bem como as crises de saúde e econômica generalizadas. Entretanto, em meio a um cenário caótico...
download_pdf: /arquivos/2020-05-19-labri-015.pdf
serie: COVID-19
---



O novo coronavírus surpreendeu todo o mundo com seu advento repentino. Nenhuma nação pôde se preparar para enfrentar suas consequências, bem como as crises de saúde e econômica generalizadas. Entretanto, em meio a um cenário caótico, onde líderes mundiais encontram-se enfrentando dificuldades intensas, um Estado em especial tornou-se um exemplo de sucesso no combate à COVID-19 por seus eficientes métodos, fortemente associados às tecnologias: a Coreia do Sul. Segundo matéria publicada pelo Jornal Nexo, o país asiático obteve uma taxa de mortalidade da população pelo vírus de 0,7%, muito inferior à média mundial, de 3,4%.

![](https://i.imgur.com/WBQqErL.png)

Um dos métodos utilizados pelo governo sul-coreano tem sido o uso de um aplicativo desenvolvido pelo Ministério do Interior da Coreia do Sul, capaz de monitorar o deslocamento da população via Sistema de Posicionamento Global  (GPS, sigla em inglês) ou, até mesmo, transações de cartões de crédito. Dessa forma, quando um usuário deixa sua casa, o aplicativo emite um alerta via SMS para ele e para o governo, iniciando o rastreamento e possibilitando a identificação do ponto inicial de uma contaminação em massa, além de detectar a trajetória de transmissão.

Ademais, se um caso for dado como positivo, o contaminado deve responder a um questionário informando onde esteve e com quem entrou em contato. A partir disso, todos os indivíduos que estiveram próximos a ele ou visitaram o mesmo local recebem um novo alerta no celular; incluindo os desconhecidos, não pertencentes ao mesmo círculo social, mas que da mesma forma, entraram em contato. Da mesma forma, o comportamento dos usuários em quarentena restrita - ou seja, aqueles com risco de estarem carregando o vírus também serão monitorados. Isso é feito por meio de novas perguntas sobre a saúde do confinado em questão, sem a necessidade de haver consulta médica presencial. Do mesmo modo, é disponibilizado no aplicativo uma forma de contato rápido com o serviço de ambulâncias para casos de emergência.

A problemática dessa solução de monitoramento, porém, reside na proteção de dados e na esfera privada, que não foram alvo de grandes preocupações por parte do governo, devido à formação cultural do povo asiático, pautada na doutrina filosófica confucionista e, por isso, menos propenso a se importar com liberdades individuais.

É válido ressaltar, ainda, que a Coreia do Sul já havia passado por uma crise de saúde anos atrás devido a um outro tipo de coronavírus, causador da Síndrome Respiratória do Oriente Médio (MERS), além de possuir uma cultura muito divergente da ocidental, na qual a população é mais propícia à obedecer ordens e cooperar entre si. O gráfico abaixo mostra como todos esses elementos em conjunto foram fatores determinantes para que a Coreia do Sul pudesse controlar o nível de infectados pela COVID-19.



![](https://i.imgur.com/QN282tv.png)
### O sucesso do combate ao COVID-19 baseado em questões culturais sul coreanas

Estados Unidos e Coreia do Sul anunciaram o primeiro caso de coronavírus no mesmo dia, 20 de janeiro de 2020. Há pouco tempo, os americanos haviam testado 4,3 mil pessoas em seu território. Já o país asiático chegou a 196 mil testes no mesmo período. O que está por trás do sucesso da Coreia do Sul?

O país se tornou um exemplo mundial quando se trata do controle sobre a pandemia atual do novo coronavírus, e muito disso se deve não só à tecnologia de monitoramento e às experiências passadas com outras crises de saúde, mas também à sua formação cultural.

A tradição Ocidental difere da Oriental em muitos pontos. Essencialmente, o Ocidente é muito adepto dos ideais Iluministas. Os filósofos desse período, tais como Thomas Hobbes, Jean-Jacques Rousseau e John Locke, defendiam as liberdades de cada indivíduo na sociedade, preocupando-se em definir os limites da atuação do Estado a fim de que ele pudesse auxiliar o povo assegurando seus direitos chamados de fundamentais e naturais, sem interferir na “autonomia” supracitada. É a partir dessa formação cultural pautada no movimento Iluminista, no progresso material e a expansão europeia no século XIX com o Neocolonialismo, que a Ásia passou a ser vista - de forma errônea e preconceituosa - como “inferior” pelos ocidentais, educados por filósofos como Karl Marx, Adam Smith, Friedrich Hegel, entre outros, que posicionaram a Ásia como um ambiente não desenvolvido, atrasado (GONÇALVES, 2005).

Os Estados asiáticos, por sua vez, compartilham de uma mentalidade autoritária, que vem de sua tradição cultural, conhecida como confucionismo. Originado no século VI a.C pelo filósofo chinês Kung-Fu-Tzu, foi uma doutrina filosófica que influenciou na formação cultural da Coreia e de outros países asiáticos. O confucionismo estabeleceu sistemas educacionais, de moral, social, político e religiosos buscando conciliar a natureza humana com teorias políticas e sociais, o que o torna uma doutrina prescritiva do bem viver. Diferente de outros países ocidentais, a população sul-coreana é mais obediente e confia mais no Estado, influência que prevalece até a sua atual conjuntura (SIMÕES, GOMES, GNERRE, 2020).

Em oposição à mentalidade europeia de inferioridade asiática, intelectuais e líderes do continente passaram a fortalecer seus valores próprios embasados na doutrina confucionista, como harmonia social, devoção à comunidade e à família, submissão, recusa do dissenso, e preferência por um governo “forte” (GONÇALVES, 2005).

O escritor e político japonês Shintaro Ishihara manifesta que os valores condutores da política são muito diferente na Europa Ocidental e no Sudeste Asiático, pois é a política que representa a forma como um governo conduz seu povo. Na visão oriental, é incoerente que os europeus tenham sido capazes de “tolerar” grupos neonazistas em prol da liberdade de pensamento. Segundo Shintaro, “os asiáticos não apreciam uma democracia que tende para ser permissiva quanto a uma liberdade excessiva e sem inibições, não importa o muito respeito pelo indivíduo que seja enfatizado. É o estoicismo asiático que rejeita este individualismo” (GONÇALVES, 2005).

Dessa forma, é possível compreender porque a formação cultural do povo oriental é tão diversa à formação cultural europeia ou americana. E, a partir disso, pode-se dizer que o comportamento de coletividade dos cidadãos sul-coreanos combinado ao alto uso e acesso à tecnologia, à ampla quantidade de testes disponíveis, ao monitoramento da população e à colaboração da população, foi, portanto, essencial para tornar a Coreia do Sul, e outros países asiáticos com medidas similares, como Taiwan, exemplos para o restante do mundo no combate da COVID-19.

### A relação entre o surto anterior de MERS e o atual combate efetivo à COVID-19 na Coreia do Sul

No presente cenário mundial de pandemia, no qual diversos países estão sucumbindo ao novo coronavírus, a Coreia do Sul tem exibido escolhas de medidas profiláticas e conduta exemplar no combate à enfermidade. Tal precisão e velocidade atuais na tomada de decisões se deve, em grande parte, a outro coronavírus que já causou danos ao país, a Síndrome Respiratória do Oriente Médio (MERS).

Em maio de 2015, um homem retornando do Oriente Médio trouxe a síndrome para a Coreia do Sul. Após apresentar sintomas similares aos da gripe e passar por diversos hospitais sem ter um diagnóstico, foi internado com pneumonia. Quando finalmente diagnosticado com MERS, após 16 dias desde seu retorno, já era tarde demais para evitar o surto da doença. Desse modo, um único caso de MERS provocou 186 infecções, das quais 36 foram fatais. Nesse momento, o governo sul-coreano foi duramente criticado por sua população devido a lentidão e despreparo para responder ao caso.

Diante de tais fatos, o país asiático aprendeu duras e importantes lições sobre o combate a enfermidades de fácil proliferação. Primeiramente, a necessidade do aviso precoce e diagnóstico preciso de casos suspeitos de doenças infecciosas, medidas que poderiam ter evitado a disseminação da MERS por diferentes hospitais do país antes de sua identificação. Em segundo lugar, a importância da testagem em massa e velocidade dos resultados, ao passo que, em 2015, eram necessários dias para a confirmação de algum caso e não se possuíam testes o bastante, fato que dificultou enormemente o combate à doença.

Portanto, graças ao aprendizado com a crise passada, a Coreia do Sul encontrava-se muito mais preparada do que grande parte do globo para um novo surto como a COVID-19. Assim, foi capaz de mobilizar-se rapidamente e tomar medidas precisas para conter os quadros exponenciais de mortes e contaminação causados pela doença. O país mostrou, assim, um caminho a ser seguido por países democráticos e mais abertos, em contraste às medidas aparentemente efetivas, porém pesadas como as da China.



### Medidas de vigilância e monitoramento e o autoritarismo digital estatal

A crise em saúde pública global criada pelo novo coronavírus impõe aos países uma urgente necessidade de gerar esforços no sentido de tentar controlar o crescimento do número de casos e suas consequências, sejam elas humanitárias, sanitárias ou econômicas. Segundo o guia de combate à doença realizado pela Organização Mundial da Saúde (OMS), grande parte das medidas de combate à essa doença orientam-se na “prevenção de surtos, atrasar a disseminação e parar o contágio” (WORLD HEALTH ORGANIZATION, 2020, p. 1, tradução nossa), o que só é possível a partir da testagem de pessoas e do isolamento social.

Países que lidaram mais prematuramente com a doença criaram diferentes formas de combate à essa crise e, em especial, China e Coreia do Sul possuem como um dos pontos principais de seu plano de ação o extensivo monitoramento e vigilância de pessoas contaminadas. Isso só é possível pelo amplo acesso à celulares que possuem acesso a internet e à localização por satélite.

O foco desse tipo de ação é tornar mais eficiente o que é chamado de  _contact tracing_, ou rastreamento de contato, que busca verificar baseado nos lugares aos quais a pessoa frequentou quais as possibilidades que o paciente se contagiou e também qual risco ele ofereceu para outras pessoas. (EAMES, KEELING, 2020).

Tais medidas trazem à tona discussões acerca de como essas ferramentas foram utilizadas e quais as consequências práticas e éticas, uma vez que tornam-se uma forma de controle em massa da população, e situam-se no limiar das liberdades individuais.

Acerca disso, uma das questões que podem ser levantadas são as críticas aos modelos chinês e sul-coreano e à aproximação destes à gestos autoritários. Na Coreia do Sul, com a preocupação na possibilidade de contágio por proximidade de uma pessoa contaminada, como reporta Jaime Santirso (2020), que apresenta a estratégia sul-coreana como efetiva.

Ainda sim, críticas foram feitas ao mesmo método em matéria da BBC, colocando como principais pontos negativos o medo social de pessoas infectadas e a divulgação compulsória de dados pessoais pelo governo. (HYUNG, 2020). De forma a levantar questões importantes acerca do tema de privacidade pessoal e monitoramento governamental e como essas ações tomadas por países podem se transformar em ferramentas para governos autoritários.

No Brasil, “O governo brasileiro vai passar a ter acesso a dados das operadoras de celulares para identificar aglomerações de pessoas em todo o país.” (MAGENTA, 2020). Seguindo um modelo mais próximo ao utilizado na China e que é considerado “um dos mais controversos acerca do direito à privacidade.” (MAGENTA, 2020). Desse modo, deve ser monitorado como o Estado fará o uso dessas informações a fim de única e exclusivamente lidar de forma mais eficiente com a  [pandemia](https://www.google.com/url?q=https%3A%2F%2Fwww.bloomberg.com%2Fgraphics%2F2020-coronavirus-cases-world-map%2F%3Fitm_source%3Dinline&sa=D&sntz=1&usg=AFQjCNGf3VxsCGk-OyQdZRIjpKnqjzNc0Q).

<article></article>
### Referências Bibliográficas

ALESSI, Gil. As lições contra o coronavírus que Coreia do Sul e China podem dar ao mundo, incluindo o Brasil. **El País**, 30 mar. 2020. Disponível em: https://brasil.elpais.com/internacional/2020-03-30/as-licoes-contra-o-coronavirus-que-coreia-do-sul-e-china-podem-dar-ao-mundo-incluindo-o-brasil.html. Acesso em: 4 maio 2020.

CHARLEAUX, João Paulo. O uso do celular contra o coronavírus. E os limites da vigilância. **Nexo Jornal**, 3 abr. 2020. Disponível em: https://www.nexojornal.com.br/expresso/2020/04/03/O-uso-do-celular-contra-o-coronav%C3%ADrus.-E-os-limites-da-vigil%C3%A2ncia. Acesso em: 4 maio 2020.

CHO, Hyunghoon; IPPOLITO, Daphne; YU, Yun William. **Contact tracing mobile apps for COVID-19: Privacy considerations and related trade-offs**, 2020. Disponível em https://arxiv.org/abs/2003.11511.

EAMES, Ken TD; KEELING, Matt J. **Contact tracing and disease control**. Proceedings of the Royal Society of London. Series B: Biological Sciences, v. 270, n. 1533, p. 2565-2571, 2003. Disponível em: https://royalsocietypublishing.org/doi/abs/10.1098/rspb.2003.2554

FOX, Justin. What Prepares a Country for a Pandemic? An Epidemic Helps. **Bloomberg Opinion**, 18 mar. 2020. Disponível em: https://www.bloomberg.com/opinion/articles/2020-03-18/covid-19-response-better-in-countries-with-sars-mers-coronavirus. Acesso em: 6 maio 2020.

GONÇALVES, Arnaldo. **Os Valores asiáticos e os Direitos Humanos**. Brasil: POLÍTICA INTERNACIONAL_,_ nº27, 2005. Disponível em: <http://www.ipris.org/files/27/7PI_ArnGonc.pdf>

HAN, Byung-Chul. O coronavírus de hoje e o mundo de amanhã segundo o filósofo Byung-Chul Han. **El País**, 22 mar. 2020. Disponível em: https://brasil.elpais.com/ideas/2020-03-22/o-coronavirus-de-hoje-e-o-mundo-de-amanha-segundo-o-filosofo-byung-chul-han.html.Acesso em: 6 maio 2020.

HYUNG, Eun Kim .Coronavírus: o que está por trás do sucesso da Coreia do Sul para salvar vidas em meio à pandemia**.** 19 de Março de 2020. **BBC**. Disponível em: https://www.bbc.com/portuguese/internacional-51877262

JO, Eun A. A Democratic Response to Coronavirus: Lessons From South Korea. **The Diplomat**, 30 mar. 2020. Disponível em: https://thediplomat.com/2020/03/a-democratic-response-to-coronavirus-lessons-from-south-korea/. Acesso em: 4 maio 2020.

KIM, HyunJung. South Korea learned its successful Covid-19 strategy from a previous coronavirus outbreak: MERS. **Bulletin of the Atomic Scientists**, 20 mar. 2020. Disponível em: https://thebulletin.org/2020/03/south-korea-learned-its-successful-covid-19-strategy-from-a-previous-coronavirus-outbreak-mers/. Acesso em : 6 maio 2020.

MAGENTA, Matheus. Coronavírus: governo brasileiro vai monitorar celulares para conter pandemia. **BBC**, 03 de Abril de 2020. Disponível em: https://www.bbc.com/portuguese/brasil-52154128.

NEVES, Emanuel; LACERDA, Ricardo; GARATTONI, Bruno. O mundo pós-coronavírus. Superinteressante, 7 maio 2020. Disponível em: https://super.abril.com.br/especiais/o-mundo-pos-coronavirus/. Acesso em: 7 maio 2020.

RASKAR, Ramesh et al. **Apps gone rogue: Maintaining personal privacy in an epidemic.**, 2020. Disponível em [https://arxiv.org/abs/2003.08567](https://www.google.com/url?q=https%3A%2F%2Farxiv.org%2Fabs%2F2003.08567&sa=D&sntz=1&usg=AFQjCNG6-H7Mlr9Jyo9EGfzhzm_I2rBImA).

ROCHA, Camilo. Coronavírus: As lições da Coreia do Sul para o mundo. **Nexo Jornal**, 13 mar. 2020. Disponível em: https://www.nexojornal.com.br/expresso/2020/03/13/Coronav%C3%ADrus-as-a%C3%A7%C3%B5es-da-Coreia-do-Sul-e-as-li%C3%A7%C3%B5es-para-o-Brasil. Acesso em: 4 maio 2020.

SANTIRSO, Jaime. Coreia do Sul: Contra o coronavírus, tecnologia. **El País**, 15 mar. 2020. Disponível em: https://brasil.elpais.com/internacional/2020-03-15/coreia-do-sul-contra-o-coronavirus-tecnologia.htm. Acesso em: 4 maio 2020.

SIMÕES, Ana Soré Araújo; GOMES, I. F.; GNERRE, M. L. A. **CONFUCIONISMO E ÉTICA: UMA PRÁTICA INTEGRADA À VIDA**. 2011. Disponível em: <https://pdfs.semanticscholar.org/90f1/7a95bf367f1ec21806251e07e154e0b5be2f.pdf>

WORLD HEALTH ORGANIZATION et al. **Operational considerations for case management of COVID-19 in health facility and community: interim guidance**, 19 March 2020. World Health Organization, 2020. Disponível em https://apps.who.int/iris/bitstream/handle/10665/331492/WHO-2019-nCoV-HCF_operations-2020.1-eng.pdf
