module.exports = {
  info: [ //informações gerais sobre funcionamento e estágio labri
     {
       type: 'category',
       label: 'Informações',
       items: ['geral/info/atendimento',
       'geral/info/estagio', 
       'geral/info/processo-seletivo', 
       'geral/info/colaborador',
       'geral/info/emprestimos', 
      {
          type: 'link',
          href: '/docs/geral/equipe/intro',
          label: 'Equipe',
        },
      ],
     },
   ],
  cadernos: [ // publicação LabRI/UNESP
    {
     type: 'category',
     label: 'Cadernos LabRI/UNESP',
     items: [
     'cadernos/intro', 
     'cadernos/expediente', 
     {
          type: 'link',
          href: '/cadernos',
          label: 'Edição Atual/Anteriores',
        },
     'cadernos/series-labri', 
     'cadernos/como-publicar'],
    },
  ],

  tecDigitais: [ // material de apoio
    {
      type: 'category',
      label: 'Tecnologias Digitais',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/ensino/intro',
          label: 'Projetos de Ensino',
        },
        'projetos/ensino/tec-digitais/intro',
        'projetos/ensino/tec-digitais/id-visual',
        'projetos/ensino/tec-digitais/conhecimentos/intro',
        'projetos/ensino/tec-digitais/anotacoes/intro',
        'projetos/ensino/tec-digitais/edicao-texto/intro',
        'projetos/ensino/tec-digitais/buscadores/intro',
        'projetos/ensino/tec-digitais/audio/intro'
        ],
    },
  ],

  materialBibliografico: [ // material bibliográfico
    {
      type: 'category',
      label: 'Material Bibliográfico',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/ensino/intro',
          label: 'Projetos de Ensino',
        },
        'projetos/ensino/material-bibliografico/intro',
        'projetos/ensino/material-bibliografico/classicos-ipri',
        ],
    },
  ],



  projetos: [ 
      {
       type: 'category',
       label: 'Projetos',
       items: ['projetos/intro'],
      },
    ],
    
    projetosDados: [
      {
        type: 'category',
        label: 'Projetos de Dados',
        items: [
      'projetos/dados/intro',
      'projetos/dados/iniciativas', 
      'projetos/dados/id-visual'      
      ],
    },
  ],
  
  projetosDadosAcervoRedalint: [
    {
      type: 'category',
      label: 'Acervo REDALINT',
      items: [
        {
        type: 'link',
        href: '/docs/projetos/dados/intro',
        label: 'Projetos de Dados',
      },
        'projetos/dados/acervo-redalint/intro', 
        'projetos/dados/acervo-redalint/howto', 
        'projetos/dados/acervo-redalint/codedoc'],
      },
    ],
  projetosDadosDiariosbr: [
    {
      type: 'category',
      label: 'DiáriosBR',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/dados/intro',
          label: 'Projetos de Dados',
        },
        'projetos/dados/diariosbr/intro',
        'projetos/dados/diariosbr/id-visual'],
      },
    ],
  projetosDadosFullText: [
    {
      type: 'category',
      label: 'Full Text',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/dados/intro',
          label: 'Projetos de Dados',
        },
        'projetos/dados/full-text/intro',
        'projetos/dados/full-text/howto',
        'projetos/dados/full-text/codedoc',
        'projetos/dados/full-text/id-visual'],
      },
    ],
  
  projetosDadosHemerotecaPEB: [
    {
      type: 'category',
      label: 'Hemeroteca PEB',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/dados/intro',
          label: 'Projetos de Dados',
        },
        'projetos/dados/hemeroteca-peb/intro', 
        'projetos/dados/hemeroteca-peb/howto', 
        'projetos/dados/hemeroteca-peb/codedoc'],
      },
    ],
  
  projetosDadosIRJournalsbr: [
    {
      type: 'category',
      label: 'IRJournalsBR',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/dados/intro',
          label: 'Projetos de Dados',
        },
        'projetos/dados/irjournalsbr/intro', 
        'projetos/dados/irjournalsbr/howto', 
        'projetos/dados/irjournalsbr/codedoc',
        'projetos/dados/irjournalsbr/id-visual'],
      },
    ],

  projetosDadosMercoDocs: [
    {
      type: 'category',
      label: 'MercoDocs',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/dados/intro',
          label: 'Projetos de Dados',
        },
        'projetos/dados/mercodocs/intro', 
        'projetos/dados/mercodocs/howto', 
        'projetos/dados/mercodocs/codedoc'],
      },
    ],

  projetosDadosNewsCloud: [
    {
      type: 'category',
      label: 'NewsCloud',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/dados/intro',
          label: 'Projetos de Dados',
        },
        'projetos/dados/newscloud/intro', 
        'projetos/dados/newscloud/howto', 
        'projetos/dados/newscloud/codedoc',
        'projetos/dados/newscloud/id-visual'],
      },
    ],

  projetosDadosTweepina: [
    {
      type: 'category',
      label: 'TweePiNa',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/dados/intro',
          label: 'Projetos de Dados',
        },
        'projetos/dados/tweepina/intro', 
        'projetos/dados/tweepina/howto', 
        'projetos/dados/tweepina/codedoc',
        'projetos/dados/tweepina/id-visual'],
      },
    ],

  projetosDadosGovLatinAmerica: [
    {
      type: 'category',
      label: 'GovLatinAmerica',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/dados/intro',
          label: 'Projetos de Dados',
        },
        'projetos/dados/gov-latin-america/intro', 
        'projetos/dados/gov-latin-america/howto', 
        'projetos/dados/gov-latin-america/codedoc',
        'projetos/dados/gov-latin-america/id-visual'],       
      },
    ],

    projetosExtensao: [
      {
        type: 'category',
        label: 'Projetos de Extensão',
        items: [
      'projetos/extensao/intro',
      'projetos/extensao/iniciativas',
      'projetos/extensao/id-visual'
      ],
    },
  ],

    projetosExtensaoPodRI: [
      {
        type: 'category',
        label: 'Pod-RI',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/extensao/intro',
            label: 'Projetos de Extensão',
          },
          'projetos/extensao/pod-ri/intro',
          'projetos/extensao/pod-ri/equipe',
          'projetos/extensao/pod-ri/episodios',
          'projetos/extensao/pod-ri/id-visual'],
        },
      ],

     projetosExtensaoPandemiaRI: [
      {
        type: 'category',
        label: 'Pandemia e as Relações Internacionais',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/extensao/intro',
            label: 'Projetos de Extensão',
          },
          'projetos/extensao/covid19/intro',
          'projetos/extensao/covid19/grupos-trabalho',
          'projetos/extensao/covid19/postagens',
          'projetos/extensao/covid19/id-visual'],
      },
    ],

    projetosEnsino: [
      {
        type: 'category',
        label: 'Projetos de Ensino',
        items: ['projetos/ensino/intro','projetos/ensino/iniciativas', 'projetos/ensino/id-visual'],
      },
  ],
 
       projetosEnsinoBancoDados: [
        {
          type: 'category',
          label: 'Banco de dados',
          items: [
            {
              type: 'link',
              href: '/docs/projetos/ensino/intro',
              label: 'Projetos de Ensino',
            },
            'projetos/ensino/linguagens/banco-dados/intro'],
          },
        ],
      projetosEnsinoJs: [
        {
          type: 'category',
          label: 'Linguagem Js',
          items: [
            {
              type: 'link',
              href: '/docs/projetos/ensino/intro',
              label: 'Projetos de Ensino',
            },
            'projetos/ensino/linguagens/js/intro'],
          },
        ],
        
      projetosEnsinoMetodosQuanti: [
        {
          type: 'category',
          label: 'Métodos Quantitativos',
          items: [
            {
              type: 'link',
              href: '/docs/projetos/ensino/intro',
              label: 'Projetos de Ensino',
            },
            'projetos/ensino/trilha-dados/metodos-quanti/intro'],
          },
        ],
      
      projetosEnsinoPython: [
      {
        type: 'category',
        label: 'Linguagem Python',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/ensino/intro',
            label: 'Projetos de Ensino'},
          'projetos/ensino/linguagens/python/intro',
          {
            type: 'category',
            label: 'Básico',
            items: [
              'projetos/ensino/linguagens/python/p00-01-ambiente',
              'projetos/ensino/linguagens/python/p00-02-zen-python',
              {
                type: 'category',
                label: 'Tipos Básicos',
                items: [
              'projetos/ensino/linguagens/python/p01-01-variaveis-e-numeros',
              'projetos/ensino/linguagens/python/p01-02-strings',
              'projetos/ensino/linguagens/python/p01-02b-operadores',
              'projetos/ensino/linguagens/python/p01-03-conversao-tipos',],
             },
              'projetos/ensino/linguagens/python/p02-01-funcoes',
               {
                 type: 'category',
                 label: 'Módulos',
                  items: [
              'projetos/ensino/linguagens/python/p05-01-arquivos',
              'projetos/ensino/linguagens/python/p05-02-diretorios',
              'projetos/ensino/linguagens/python/p05-03-bibliotecas'],
             },
              'projetos/ensino/linguagens/python/p03-05-mutabilidade',
               {
                 type: 'category',
                 label: 'Tipos Avançados',
                  items: [
              'projetos/ensino/linguagens/python/p03-01-listas',
              'projetos/ensino/linguagens/python/p03-02-tuplas',
              'projetos/ensino/linguagens/python/p03-03-conjuntos',
              'projetos/ensino/linguagens/python/p03-04-dicionarios'],
             },

              {
                 type: 'category',
                 label: 'Controle de Fluxo',
                  items: [
              'projetos/ensino/linguagens/python/p04-01-booleanos',
              'projetos/ensino/linguagens/python/p04-02-loops'],
             },
             ],},
             {
                 type: 'category',
                 label: 'Intermediário',
                  items: [
              'projetos/ensino/linguagens/python/p06-01-compreensao',
              'projetos/ensino/linguagens/python/p06-02-itertools',
              'projetos/ensino/linguagens/python/p06-03-encode',
              'projetos/ensino/linguagens/python/p07-01-objetos',

             ],
             }, 
              {
                 type: 'category',
                 label: 'Quiz',
                  items: [
              'projetos/ensino/linguagens/python/quiz/q-01',

             ],
             }, 
          ],
        },
      ],
      projetosEnsinoRlang: [
        {
          type: 'category',
          label: 'Linguagem R',
          items: [
            {
              type: 'link',
              href: '/docs/projetos/ensino/intro',
              label: 'Projetos de Ensino',
            },
            'projetos/ensino/linguagens/r-lang/intro'],
          },
        ],
      
      projetosEnsinoTrilhaDados: [
    {
      type: 'category',
      label: 'Trilha de Dados',
      items: [
        {
          type: 'link',
          href: '/docs/projetos/ensino/intro',
          label: 'Projetos de Ensino',
        },
        'projetos/ensino/trilha-dados/intro', 
        'projetos/ensino/trilha-dados/comece-aqui', 
        'projetos/ensino/trilha-dados/paradas',
        'projetos/ensino/trilha-dados/id-visual'],
      },
    ],

      projetosEnsinoVersionamento: [
      {
        type: 'category',
        label: 'Versionamento',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/ensino/intro',
            label: 'Projetos de Ensino',
          },
          'projetos/ensino/versionamento/intro',
          'projetos/ensino/versionamento/integrar-branches'],
        },
      ],
    
    projetosEnsinoWebScraping: [
      {
        type: 'category',
        label: 'Web Scraping',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/ensino/intro',
            label: 'Projetos de Ensino',
          },
          'projetos/ensino/trilha-dados/webscraping/intro'],
        },
      ],

  projetossistemas: [
      {
        type: 'category',
        label: 'Projetos de sistemas',
        items: [
          {
            type: 'link',
            href: '/docs/projetos/intro',
            label: 'Projetos',
          },
            'projetos/sistemas/intro',
            'projetos/sistemas/iniciativas',
          {
            type: 'link',
            href: '/docs/projetos/sistemas/dev/intro',
            label: 'Dev-LabRI',
          },

              {
            type: 'link',
            href: '/docs/projetos/sistemas/implementacao/intro',
            label: 'Implementação',
          },
             {
            type: 'link',
            href: '/docs/projetos/sistemas/linux/intro',
            label: 'Linux',
          },
             {
            type: 'link',
            href: '/docs/projetos/sistemas/monitoramento/intro',
            label: 'Monitoramento',
          },
             {
            type: 'link',
            href: '/docs/projetos/sistemas/proxmox/intro',
            label: 'Proxmox',
          },
                       {
            type: 'link',
            href: '/docs/projetos/sistemas/container/intro',
            label: 'Container',
          },
            'projetos/sistemas/id-visual'],
          },
        ],

    projetossistemasLinux: [
      {
        type: 'category',
        label: 'Linux',
        items: [
          'projetos/sistemas/linux/intro', 
          'projetos/sistemas/linux/backup',
          'projetos/sistemas/linux/storage', // NFS, ZFS, CEPH
          'projetos/sistemas/linux/ldap',
          'projetos/sistemas/linux/cron'],
        },
      ],

    projetosSistemasDevLabri: [
      {
        type: 'category',
        label: 'Dev-LabRI',
        items: [
          'projetos/sistemas/dev/intro',
          'projetos/sistemas/dev/geral/cadernos-labri',
          'projetos/sistemas/dev/geral/certificados',
          'projetos/sistemas/dev/geral/ocr'],

        },
      ],
      projetosSistemasImplementacao: [
      {
        type: 'category',
        label: 'Implementação',
        items: [
         'projetos/sistemas/implementacao/intro',
          'projetos/sistemas/implementacao/recoll/intro',
],
        },
      ],
      projetossistemasProxmox: [
        {
          type: 'category',
          label: 'Proxmox',
          items: [
            'projetos/sistemas/proxmox/intro'],
          },
        ],
      projetossistemasContainer: [
          {
            type: 'category',
            label: 'Container',
            items: [
              'projetos/sistemas/container/intro'],
            },
          ],
      projetossistemasMonitoramento: [
            {
              type: 'category',
              label: 'Monitoramento',
              items: [
                'projetos/sistemas/monitoramento/intro'],
              },
            ],

    equipe: [
    {
     type: 'category',
     label: 'Equipe',
     items: [
        'geral/equipe/intro',
        {
        'Equipe Atual': [
            {
              type: 'link',
              href: '/docs/geral/equipe/atual/fabio-paron/intro',
              label: 'Fabio Paron',
            }, 
             {
              type: 'link',
              href: '/docs/geral/equipe/atual/julia-silveira/intro',
              label: 'Julia Silveira',
            }, 
             {
              type: 'link',
              href: '/docs/geral/equipe/atual/rafael-almeida/intro',
              label: 'Rafael Almeida',
            }, 
            {
              type: 'link',
              href: '/docs/geral/equipe/atual/fulano-teste/intro',
              label: 'Fulano Teste',
            }, 
          ],
        },
        {
        'Ex-membros': [
            {
              type: 'link',
              href: '/docs/geral/equipe/ex-membro/pedro-campagna/intro',
              label: 'Pedro Campagna',
            }, 
          ],
        },
        ],
      },
    ],

    equipeFabioParon: [ // projetos de dados LabRI/UNESP
        {
          type: 'category',
          label: 'Fabio Paron',
          items: ['geral/equipe/atual/fabio-paron/intro'],
        },
      ],
     equipeJuliaSilveira: [ // projetos de dados LabRI/UNESP
        {
          type: 'category',
          label: 'Julia Silveira',
          items: ['geral/equipe/atual/julia-silveira/intro'],
        },
      ],
       equipeRafaelAlmeida: [ // projetos de dados LabRI/UNESP
        {
          type: 'category',
          label: 'Rafael Almeida',
          items: ['geral/equipe/atual/rafael-almeida/intro'],
        },
      ],
      equipePedroCampagna: [ // projetos de dados LabRI/UNESP
        {
          type: 'category',
          label: 'Pedro Campagna',
          items: ['geral/equipe/ex-membro/pedro-campagna/intro'],
        },
      ],
    
};
