def get_template(dict, data, content):
    ano, mes, dia = data.split("-")
    dict["title"] = dict["title"].replace('"', "")
    dict["subtitulo"] = dict["subtitulo"].replace('"', "")
    primeiros_nomes = []
    ultimos_nomes = []
    abnt_nomes = []
    for autor in dict["author"]:
        ultimos_nomes.append(autor.split()[-1])
        primeiros_nomes.append(autor.split()[0])
    ultimos_nomes_cabecalho = ", ".join(ultimos_nomes)
    for i in range(len(primeiros_nomes)):
        abnt_nome = f"{ultimos_nomes[i].upper()}, {primeiros_nomes[i]}"
        abnt_nomes.append(abnt_nome)
    abnt_nomes.sort()
    if dict["subtitulo"] != "":
        dict["subtitulo"] = f": {dict['subtitulo']}"
    autores_para_primeira_pagina = []
    for i, autor in enumerate(dict["author"], 1):
        autores_para_primeira_pagina.append(
            f"{autor} <sup id='author'><a class='footnote-ref' href='#author_ft'>{'*'*i }</a></sup>"
        )

    TEMPLATE = f"""
    <html lang="en" dir="ltr">
      <head>
        <link rel="stylesheet" href="/home/labri_pedro/labri/python/style.css">
        <meta charset="utf-8">
        <meta name="slug" content="{dict['slug']}">
        <meta name="tags" content="{dict['tags']}">
        <meta name="title" content="{dict['title']}">
        <meta name="subtitle" content="{dict['subtitulo']}">
        <meta name="autor:" content="{dict['author']}">
        <meta name="author_title" content="{dict['author_title']}">
        <meta name="tipo_publicacao" content="{dict['tipo_publicacao']}">
        <meta name="image" content="{dict['image_url']}">
        <meta name="descricao" content="{dict['descricao']}">
        <meta name="data" content="{data}">
        <meta id="ncaderno" name="ncaderno" content="Cadernos LabRI - N. {dict['n_caderno']}, {dia}/{mes}/{ano}">
        <title>{dict['title']}</title>

      </head>
      <body>
        <article class="cover">
            <div class="cabecalho_capa">
                <address>
                    Cadernos LabRI - N. {dict['n_caderno']}, {dia}/{mes}/{ano}
                </address>
            </div>
            <div class="titulo-publicacao">
                <h1 class="cadernos_labri"> CADERNOS <mark>LABRI</mark> </h1>
                <h1 class="titulo"> {dict['title']}{dict['subtitulo']} </h1>
            </div>
            <div class="serie">
               <img class="serie" src="/home/labri_pedro/labri/python/style/serie.png" alt="serie">
            </div>
            <div class="creative_commons">
              <div class="column_cc">
                <img class="cc_img" src="/home/labri_pedro/labri/python/style/cc.png" alt="creative_commons">
              </div>
              <div class="column_cc">
                <img class="cc_img" src="/home/labri_pedro/labri/python/style/by.png", alt="logo-labri">
              </div>
              <div class="column_cc">
                <p>BY</p>
              </div>

            </div>
            <div class="logos">
              <div class="column">
                <img class="logo_img logo_unesp" src="/home/labri_pedro/labri/python/style/unesp.png" alt="Unesp">
              </div>
              <div class="column">
                <img class="logo_img" src="/home/labri_pedro/labri/python/style/logo.png", alt="logo-labri">
              </div>
            </div>
        </article>
        <article class="contracapa">
          <h6 class="contracapa_logo"><img class="logo_img" src="/home/labri_pedro/labri/python/style/logo.png", alt="logo-labri"><img class="logo_img logo_unesp" src="/home/labri_pedro/labri/python/style/unesp.png" alt="Unesp"></h6>
          <div class="row">
            <div class="column_contracapa">
              <h3>Sobre os Cadernos LabRI</h3>
              <p>Os Cadernos LabRI são uma publicação circunscrita a divulgar os trabalhos de grupos e indivíduos vinculados ao LabRI/UNESP. Os principais objetivos desta publicação são:</p>
              <ul class="objetivos_labri">
                <li>Fornecer um espaço aos grupos para divulgação de seus trabalhos</li>
                <li>Auxiliar uma melhor comunicação dos projetos e dos produtos derivados</li>
                <li>Estímulo à utilização de tecnologias digitais no cotidiano acadêmico</li>
              </ul>
              <p>Devido aos aspectos apontados acima, os Cadernos LabRI não estão abertos a submissão de trabalhos externos para a publicação. Apesar disso, eventualmente, convidados também poderão ter seus trabalhos divulgados nos Cadernos LabRI.</p>
              <h3> Sobre as Séries LabRI </h3>
              <p>Os Cadernos LabRI estão organizados em uma numeração contínua e categorizadas por séries que podem ser temáticas ou vinculada a algum grupo específico</p>

              <div class="sobre_o_labri">
              <h3>Sobre o LabRI</h3>
              <p>O LabRI é um espaço do Departamento de Relações Internacionais (DERI) da UNESP de Franca para o desenvolvimento e experimentação de atividades de pesquisa e extensão intensivas no uso de novas tecnologias de informação e comunicação, assim como de novas metodologias de trabalho</p>
              </div>
              <div class="cabecalho">
                <p><br>Cadernos LabRI - N. {dict['n_caderno']}. {dia}/{mes}/{ano}. {ultimos_nomes_cabecalho}</p>
              </div>
            </div>
            <div class="column_contracapa">
              <div class="corpo_editorial">
                <h3><center>Informações editoriais<center></h3>
                  <h4>Equipe editorial</h4>
                  <p>Marcelo Mariano (Editor)</p>
                  <h4>Assistentes Editoriais</h4>
                  <p>Bárbara Carvalho Neves </p>
                  <p>Jaqueline Trevisan Pigatto</p>
                  <p>Rafael Augusto Ribeiro de Almeida</p>
                  <h4>Diagramação e Design</h4>
                  <p>Júlia dos Santos Silveira </p>
                  <p>Pedro Henrique Campagna Moura da Silva </p>
                  <h4> Periodicidade </h4>
                  <p>Irregular</p>
                  <p><bold>This work is licensed under a Creative Commons Attribution 4.0 International License.</bold></p>
                  <h4> Página Oficial </h4>
                  <p>https://novo.labriunesp.org/</p>
                  <h4> Endereço </h4>
                  <p>Universidade Estadual Paulista "Julio de Mesquita Filho" Campus Franca <br>
                  Av. Eufrásia Monteiro Petráglia, 900 <br>
                  Jd. Dr. Antonio Petráglia -14409-160 - Franca, SP, Brasil.
                  </p>​
              </div>
            </div>
          </div>
          <div class="logos_contracapa">
            <div class="column_contracapa">
              <img src="/home/labri_pedro/labri/python/style/cnpq.jpg" alt="CNPQ">
            </div>
            <div class="column_contracapa">
              <img src="/home/labri_pedro/labri/python/style/fapesp.png" alt="Fapesp">
            </div>
          </div>
        </article>
        <article class="apresentacao">
          <h1>Apresentação</h1>
          <div>Este artigo faz parte do projeto LabRI Pandemia, Realizado Pelo Laboratório de Relações Internacionais da UNESP Franca no período de pandemia a fim de estimular os alunos a relfetir sobre as condições internacionais geradas pela pandemia e suas diversas consequências </div>
        </article>

        <article id="sumario">
          <h2>Sumário</h2>

        </article>

        <article class="content">
          <div class="main">
            <div class="titulo_conteudo">
              <h2>{dict['title']}{dict['subtitulo']}</h2>
            </div>
            <div class="autores">
              <h6>{'<br>'.join(autores_para_primeira_pagina)}</h6>
            </div>
            <div class="content">
              {content}
            </div>
          </div>
          <hr>
          <div class="notas_autores">
            <ul id="author_ft">
              <li>
                <p>{dict['author_title'].replace('"','')}
                  <a class="footnote-backref" href="#author" title="Jump back to footnote 1 in the text">↩</a>
                </p>
              </li>
            </ul>

          <div>
        </article>
        <article class="endpage">
          <div class="row_endpage">
            <div class="column_endpage">
              <img class="logo_fim" src="/home/labri_pedro/labri/python/style/logo-labri.svg" alt="logo LabRIi">
            </div>
            <div class="column_endpage">
              <p>Laboratório  de Relações Internacionais <br> da UNESP</p>
            </div>
          </div>
          <div class="abnt">
            <p>Como Citar este artigo:</p>
            <p>
                {", ".join(abnt_nomes)}. {dict["title"]}. <b> Cadernos LabRI N. {dict["n_caderno"]}</b>. Franca, SP. {ano}.
            </p>
          </div>
          <div class="infos_endereco">
            <address class="endereco">
              Av. Eufrásia Monteiro Petráglia, 900 - Jd. Dr. Antonio Petráglia <br>
              Franca/SP - CEP 14409-160 <br>
              unesplabri@gmail.com - labriunesp.org
            </address>
            <img class="logo_img logo_unesp" src="/home/labri_pedro/labri/python/style/unesp.png" alt="Unesp">
          </div>
        </article>



      </body>

      <footer>

      </footer>
    </html>
    """

    return TEMPLATE
