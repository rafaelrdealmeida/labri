from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration
import os


def main():
    font_config = FontConfiguration()
    caminho_html = '/home/labri_pedro/labri/python/html'
    caminho_pdf = '/home/labri_pedro/labri/python/pdf'
    arquivos = os.listdir(caminho_html)
    arquivos.sort()

    for arquivo in arquivos:
        print(arquivo)
        arquivo_sem_extensao = arquivo.split('.')[0]
        css = CSS('/home/labri_pedro/labri/python/style.css')
        HTML(f'{caminho_html}/{arquivo}').write_pdf(
            f'{caminho_pdf}/{arquivo_sem_extensao}.pdf', stylesheets=[css], font_config=font_config
        )


if __name__ == '__main__':
    main()
