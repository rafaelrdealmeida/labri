from template import get_template
import markdown
import markdown.extensions.footnotes
import markdown.extensions.toc
import os
import pprint
import re
import tira_toc

def main():
    caminho = "//home//labri_pedro//labri//website//cadernos"
    arquivos = os.listdir(caminho.replace("//",  "/"))
    arquivos.sort()
    for arquivo in arquivos:
        tira_toc.coloca_toc(f"{caminho}/{arquivo}")
        n_caderno = arquivo.split("-")[-1]
        conteudo_arquivo = open(f"{caminho}//{arquivo}").read()
        pp = pprint.PrettyPrinter(indent=4)
        print(arquivo)
        # Parseando arquivo md
        data = get_data(arquivo)
        dict_meta = get_metadata(conteudo_arquivo)
        dict_meta["n_caderno"] = n_caderno.split(".")[0]        
        content = get_content(conteudo_arquivo)
        template = get_template(dict_meta, data, content)
        arquivo_sem_extensao = arquivo.split(".")[0]
        with open(
            f"//home//labri_pedro//labri//python//html//{arquivo_sem_extensao}.html",
            "w",
        ) as file:
            file.write(template)


def get_content(conteudo_arquivo):
    content = conteudo_arquivo.split("---")[-1]
    content = markdown.markdown(content, extensions=["footnotes", "toc"])
    return content


def get_data(arquivo):
    data_re = "[0-9]{4}-[0-1][0-9]-[0-3][0-9]"
    data = re.search(data_re, arquivo)
    data = data.group()
    return data


def get_metadata(conteudo_arquivo):
    dict_meta = {}
    metadata = conteudo_arquivo.split("---")[1]
    lista_meta = metadata.split("\n")
    for info in lista_meta:
        if info == "":
            continue
        else:
            lista_para_dict = info.split(":")
            if lista_para_dict[0] == "author":
                autores = lista_para_dict[1].replace(" e ", ", ")
                lista_autores = autores.split(",")
                dict_meta[lista_para_dict[0]] = lista_autores
            elif lista_para_dict[0] == "tags":
                tags = lista_para_dict[1].strip("]")
                tags = tags.strip(" [")
                tags = tags.split(",")
                dict_meta[lista_para_dict[0]] = tags

            else:
                dict_meta[lista_para_dict[0]] = lista_para_dict[1]
    if "subtitulo" not in dict_meta.keys():
        dict_meta["subtitulo"] = ""
    if "author_title" not in dict_meta.keys():
        dict_meta["author_title"] = ""
    if "author" not in dict_meta.keys():
        dict_meta["author"] = ["Laboratório de Relações internacional - LabRI UNESP"]
    return dict_meta


if __name__ == "__main__":
    main()
